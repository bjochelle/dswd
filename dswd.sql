-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 08, 2020 at 01:07 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 5.6.39

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dswd`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_child`
--

CREATE TABLE `tbl_child` (
  `child_id` int(11) NOT NULL,
  `fname` varchar(225) NOT NULL,
  `mname` varchar(225) NOT NULL,
  `lname` varchar(225) NOT NULL,
  `address` text NOT NULL,
  `religion` varchar(225) NOT NULL,
  `bday` date NOT NULL,
  `gender` varchar(225) NOT NULL,
  `livingwt` text NOT NULL,
  `a_int` text NOT NULL,
  `prob` text NOT NULL,
  `socio` text NOT NULL,
  `health` text NOT NULL,
  `deworming_fir` date NOT NULL,
  `deworming_sec` date NOT NULL,
  `wt_lgth_age` decimal(12,2) NOT NULL,
  `wt_age` decimal(12,2) NOT NULL,
  `weigh_hght_lg` decimal(12,2) NOT NULL,
  `specific_needs` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `archive_status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_child`
--

INSERT INTO `tbl_child` (`child_id`, `fname`, `mname`, `lname`, `address`, `religion`, `bday`, `gender`, `livingwt`, `a_int`, `prob`, `socio`, `health`, `deworming_fir`, `deworming_sec`, `wt_lgth_age`, `wt_age`, `weigh_hght_lg`, `specific_needs`, `user_id`, `archive_status`) VALUES
(1, 'XIAN', 'apostolero', 'Canlas', 'q', 'q', '2017-09-14', 'Female', 'others,', 'Medical,Teaching,Legal,Dental,Counseling,Evangelization,others,qweqwewqe', 'Lack of income,Skills,Livelihood,others ,wqeqr', 'Feeling of neglect,Feeling of helplessness,Feeling of loneliness,Inadequate leisure,Senior,others,wqrqr', 'Condition,With maintenance,High cost medicine,Lack of medical professionals,Lack/No access to sanitation,Health,No health insurance,medical facilities,others,rwqrq', '2020-09-01', '2020-09-15', '0.00', '0.00', '0.00', 'asdsadasd', 1, 0),
(3, 'jochelle ', 'esponilla', 'bravo', '16', 'baptisi', '2018-09-06', 'Female', 'Alone', 'Teaching,Legal,others,ddfdshjhjsdf', 'Lack of income, others, asdjghjhsads', 'Feeling of neglect,others,sadsadas dsadas ', 'With maintenance, others, sadhg gh ', '2020-03-01', '2020-04-01', '1.00', '2.00', '3.00', 'No one ', 1, 0),
(7, 'test first name', 'test Middle name', 'test Last name', 'test Address', 'test Religion', '2018-01-01', 'Female', 'others,test others', 'Medical,Teaching,Legal,Dental,Counseling,Evangelization,others,test others', 'Lack of income,Skills,Livelihood,others,test others', 'Feeling of neglect,Feeling of helplessness,Feeling of loneliness,Inadequate leisure,Senior,others,test others', 'Condition,With maintenance,High cost medicine,Lack of medical professionals,Lack/No access to sanitation,Health,No health insurance,medical facilities,others,test others', '2020-08-30', '2020-09-22', '1.00', '2.00', '3.00', 'test others', 1, 0),
(8, 'John', 'sy', 'Smith', '14', 'Catholic', '2020-09-01', 'Female', 'Parents', 'Teaching,Legal,Dental', 'Livelihood', 'Senior', 'Lack/No access to sanitation', '2020-09-01', '2020-09-24', '4.00', '5.00', '2.00', 'None', 1, 1),
(9, 'john', 'cruz', 'alipala', 'Blumentritt', 'Roman Catholic', '2015-11-26', 'Male', 'Parents', 'Teaching', 'Skills', 'Feeling of helplessness', 'With maintenance', '2020-10-21', '2020-10-30', '3.00', '5.00', '2.00', 'none', 28, 0),
(10, 'vins', 'Alimania', 'Delos reyes', 'Blumentritt', 'Roman Catholic', '2014-05-24', 'Male', 'Parents', 'Medical', '', '', '', '2020-10-20', '2020-10-14', '6.00', '5.00', '2.00', 'none', 28, 0),
(11, 'john', 'pamatian', 'montinola', 'Blumentritt', 'Roman Catholic', '2012-11-27', 'Male', 'Parents', 'Dental', 'Livelihood', '', 'Lack/No access to sanitation', '2020-09-29', '2020-10-07', '4.00', '3.00', '4.00', 'None', 28, 0),
(12, 'John Rey', 'Bayona', 'Nadales', 'Barangay Abo-abo', 'Roman Catholic', '2017-06-21', 'Male', 'Parents', 'Teaching', 'Skills', 'Feeling of helplessness', 'Health', '2021-02-28', '2021-08-29', '5.00', '5.00', '5.00', '', 29, 1),
(13, 'John', 'Smith', 'Doe', 'Brgy.Igluan, Murcia', 'Catholic', '2018-10-29', 'Male', 'Parents', 'Medical', 'Livelihood', '', 'Health', '2019-01-01', '2019-06-01', '3.50', '5.00', '6.00', 'None', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_eligibility`
--

CREATE TABLE `tbl_eligibility` (
  `eligibility_id` int(11) NOT NULL,
  `name` varchar(225) NOT NULL,
  `age` int(11) NOT NULL,
  `address` text NOT NULL,
  `patient_name` varchar(225) NOT NULL,
  `patient_age` int(11) NOT NULL,
  `patient_rel` varchar(50) NOT NULL,
  `amount` decimal(12,2) NOT NULL,
  `inv` varchar(50) NOT NULL,
  `date_added` datetime NOT NULL,
  `pharmacy_number` text NOT NULL,
  `rcpt_no` text NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_eligibility`
--

INSERT INTO `tbl_eligibility` (`eligibility_id`, `name`, `age`, `address`, `patient_name`, `patient_age`, `patient_rel`, `amount`, `inv`, `date_added`, `pharmacy_number`, `rcpt_no`, `user_id`) VALUES
(1, 'Macy Esponilla', 28, 'purok tabok laot, brgy.Murica', 'Xian Esponilla', 10, 'Son', '300.00', 'medicine', '2020-08-31 06:47:16', 'mercury ', '2854763', 1),
(2, 'dasd', 21, 'dsad', 'qqqq', 32, 'wq', '213.00', 'weqe', '2020-08-31 06:53:38', '', '', 1),
(3, 'sda', 132, 'dsad', 'qqqq', 23, 'widdd', '12333.00', 'school', '2020-08-31 06:58:24', '', '', 1),
(4, 'wqer', 321, 'qqq', 'qqqq', 32, 'wq', '12333.00', 'school', '2020-08-31 07:05:32', '', '', 1),
(5, 'wqer', 321, 'qqq', 'qqqq', 32, 'wq', '12333.00', 'school', '2020-08-31 07:05:38', '', '', 1),
(6, 'wqer', 23, 'qqq', 'qqqq', 23, 'wq', '23.00', 'school', '2020-08-31 07:06:30', '', '', 1),
(7, 'wqer', 23, 'qqq', 'qqqq', 23, 'widdd', '12333.00', 'weqe', '2020-08-31 07:07:01', '', '', 1),
(8, 'Marlyn Lomeran', 20, 'Brgy. 35', 'Bless Lomeran', 18, 'sister', '20000.00', 'Education', '2020-08-31 08:19:40', '', '', 1),
(9, 'John Doe', 19, 'tangub', 'Bless Lomeran', 12, 'sister', '15000.00', 'Education', '2020-09-24 05:14:28', '', '', 2),
(10, 'John Doe', 19, 'tangub', 'Bless Lomeran', 9, 'Son', '15000.00', 'Education', '2020-09-24 05:50:22', 'Grace', '2012', 33),
(11, 'Richelle Bravo', 35, 'bgry. iglou-an, Murcia City', 'Marlyn Bravo', 18, 'Sister', '300.00', 'medicine', '2020-10-22 09:11:02', 'mercury ', '098421', 2),
(12, 'joshua tan', 30, 'brgy. 2', 'alysa tan', 4, 'daughter', '300.00', 'medicine', '2020-10-22 11:14:21', 'grace', '098421', 41),
(13, 'ann delarosa', 39, 'mambucal', 'mary rose delarosa', 5, 'daughter', '300.00', 'medicine', '2020-10-22 11:54:06', 'grace', '098421', 45),
(14, 'rene rose antipona', 40, 'Blumentritt', 'ann antiposa', 12, 'daughter', '300.00', 'medicine', '2020-10-23 12:28:31', 'grace', '098421', 46),
(15, 'nestor dacles', 33, 'brgy. 2', 'daisy dacles', 3, 'daughter', '300.00', ' medicine', '2020-10-23 12:49:37', 'mercury', '09282512', 47),
(16, 'nestor dacles', 33, 'brgy. 2', 'daisy dacles', 3, 'daughter', '300.00', ' medicine', '2020-10-23 12:49:37', 'grace', '01398421', 47),
(17, 'joy villanueva', 20, 'Blumentritt', 'joy pama', 13, 'sister', '400.00', 'medicine', '2020-10-23 01:03:05', 'grace', '01398421', 48),
(18, 'anna montinola', 25, 'zone IV', 'jake santos', 19, 'brother', '300.00', 'medicine', '2020-10-23 01:22:19', 'grace', '01398421', 49),
(19, 'anna montinola', 25, 'zone IV', 'jake santos', 19, 'brother', '300.00', 'medicine', '2020-10-23 01:22:19', 'grace', '01398421', 49),
(20, 'maria delacruz', 25, 'zone V', 'ann ciento', 21, 'sister', '300.00', 'medicine', '2020-10-23 02:19:16', 'grace', '01398421', 50),
(21, 'alnora walda', 30, 'Blumentritt', 'jose gabora', 11, 'son', '300.00', 'medicine', '2020-10-23 02:37:54', 'grace', '01398421', 51);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_event`
--

CREATE TABLE `tbl_event` (
  `event_id` int(11) NOT NULL,
  `event_name` text NOT NULL,
  `event_when` datetime NOT NULL,
  `event_where` text NOT NULL,
  `event_description` text NOT NULL,
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_family`
--

CREATE TABLE `tbl_family` (
  `family_id` int(11) NOT NULL,
  `fam_name` varchar(225) NOT NULL,
  `fam_rel` varchar(225) NOT NULL,
  `fam_age` int(2) NOT NULL,
  `fam_civil_status` varchar(225) NOT NULL,
  `fam_occupation` varchar(225) NOT NULL,
  `fam_income` decimal(12,2) NOT NULL,
  `child_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_family`
--

INSERT INTO `tbl_family` (`family_id`, `fam_name`, `fam_rel`, `fam_age`, `fam_civil_status`, `fam_occupation`, `fam_income`, `child_id`) VALUES
(4, 'Jorenzo bravo', 'father', 58, 'Married', 'None', '0.00', 3),
(5, 'Michel Bravo', 'mother', 44, 'Married', 'Janitress', '9000.00', 3),
(6, 'Richelle Bravo', 'father', 23, 'Single', 'Accounting Staff', '9000.00', 3),
(26, 'Jorenzo bravo', 'father', 58, 'Married', 'Occupation2', '12000.00', 8),
(27, 'michel', 'mother', 40, 'Married', 'Occupation2', '15000.00', 8),
(28, 'Jorenzo bravo', 'father', 12, 'Married', 'Occupation2', '0.00', 1),
(29, 'Jorenzo bravo', 'father', 58, 'Married', 'Occupation2', '12.00', 1),
(30, 'renato', 'father', 54, 'Married', 'electrician', '5000.00', 9),
(31, 'rosario', 'mother', 52, 'Married', 'house wife', '0.00', 9),
(38, 'Jomar', 'Father', 54, 'Married', 'farmer', '5000.00', 10),
(39, 'lesley', 'Mother', 46, 'Married', 'farmer', '5000.00', 10),
(40, 'rene rose', 'Mother', 43, 'Married', 'farmer', '5000.00', 11),
(41, 'christian', 'faher', 46, 'Married', 'electrician', '5000.00', 11),
(42, 'Mario Nadales', 'Faher', 25, 'Married', 'farmer', '5000.00', 12),
(43, 'Meriam Nadales', 'Mother', 23, 'Married', 'House Wfe', '0.00', 12),
(44, 'Johnny Smith', 'Father', 35, 'Married', 'IT Staff', '25000.00', 13);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_feeding_details`
--

CREATE TABLE `tbl_feeding_details` (
  `feeding_id` int(11) NOT NULL,
  `cycle_id` int(11) NOT NULL,
  `child_id` int(11) NOT NULL,
  `weight` decimal(4,2) NOT NULL,
  `height` decimal(12,2) NOT NULL,
  `bmi` decimal(12,2) NOT NULL,
  `date_added` datetime NOT NULL,
  `age_taken` int(11) NOT NULL,
  `gender` varchar(1) NOT NULL,
  `isfeedingremarks` int(11) NOT NULL,
  `standard_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_feeding_details`
--

INSERT INTO `tbl_feeding_details` (`feeding_id`, `cycle_id`, `child_id`, `weight`, `height`, `bmi`, `date_added`, `age_taken`, `gender`, `isfeedingremarks`, `standard_id`, `user_id`) VALUES
(44, 6, 10, '30.00', '1.50', '13.33', '2020-10-29 00:00:00', 6, 'M', 1, 2, 28),
(45, 1, 9, '30.00', '1.50', '13.33', '2020-10-29 00:00:00', 4, 'M', 1, 2, 28),
(46, 1, 11, '30.00', '1.50', '13.33', '2020-10-29 00:00:00', 7, 'M', 1, 2, 28),
(47, 6, 12, '29.00', '2.00', '7.25', '2020-10-29 00:00:00', 3, 'M', 1, 1, 29),
(48, 1, 1, '35.00', '1.30', '20.71', '2020-10-29 00:00:00', 3, 'F', 1, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_feeding_header`
--

CREATE TABLE `tbl_feeding_header` (
  `cycle_id` int(11) NOT NULL,
  `cycle` int(11) NOT NULL,
  `date_added` date NOT NULL,
  `before_feeding` int(1) NOT NULL,
  `60_days` int(1) NOT NULL,
  `120_days` int(1) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_feeding_header`
--

INSERT INTO `tbl_feeding_header` (`cycle_id`, `cycle`, `date_added`, `before_feeding`, `60_days`, `120_days`, `user_id`, `status`) VALUES
(1, 2, '2020-09-06', 0, 0, 0, 2, 0),
(2, 4, '2020-08-30', 0, 0, 0, 24, 1),
(4, 3, '2020-09-06', 0, 0, 0, 2, 1),
(5, 5, '2020-10-01', 0, 0, 0, 2, 1),
(6, 6, '2020-10-23', 0, 0, 0, 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_logs`
--

CREATE TABLE `tbl_logs` (
  `logs_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `remarks` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_logs`
--

INSERT INTO `tbl_logs` (`logs_id`, `user_id`, `remarks`, `date_added`) VALUES
(1, 1, 'Update Account Information', '2020-08-30 09:14:25'),
(2, 1, 'Update Personal Information', '2020-08-30 09:27:09'),
(3, 1, 'Update Personal Information', '2020-08-30 09:27:35'),
(4, 1, 'Update Personal Information', '2020-08-30 09:29:19'),
(5, 1, 'Create new eligibility Record to wqeqwe', '2020-08-31 06:47:16'),
(6, 1, 'Create new eligibility Record to dasd', '2020-08-31 06:53:38'),
(7, 1, 'Create new eligibility Record to sda', '2020-08-31 06:58:24'),
(8, 1, 'Create new eligibility Record to wqer', '2020-08-31 07:05:33'),
(9, 1, 'Create new eligibility Record to wqer', '2020-08-31 07:05:38'),
(10, 1, 'Create new eligibility Record to wqer', '2020-08-31 07:06:30'),
(11, 1, 'Create new eligibility Record to wqer', '2020-08-31 07:07:01'),
(12, 1, 'Updated eligibility Record to qqqq', '2020-08-31 07:25:01'),
(13, 1, 'Updated eligibility Record to qqqq', '2020-08-31 07:25:53'),
(14, 1, 'Updated eligibility Record to qqqq', '2020-08-31 07:26:09'),
(15, 1, 'Updated eligibility Record to Macy Esponilla', '2020-08-31 08:17:58'),
(16, 1, 'Created new eligibility Record to Marlyn Lomeran', '2020-08-31 08:19:40'),
(17, 1, 'Created new Petty Cash Voucher to dsad', '2020-08-31 09:18:05'),
(18, 1, 'Created new Petty Cash Voucher to dsad', '2020-08-31 09:20:34'),
(19, 1, 'Created new Petty Cash Voucher to sdad', '2020-08-31 09:20:50'),
(20, 1, 'Updated Petty Cash Voucher to q', '2020-08-31 09:23:55'),
(21, 1, 'Updated Petty Cash Voucher to q', '2020-08-31 09:24:00'),
(22, 1, 'Updated Petty Cash Voucher to q', '2020-08-31 02:46:09'),
(23, 1, 'Updated Petty Cash Voucher to q', '2020-08-31 02:47:27'),
(24, 1, 'Updated Petty Cash Voucher to q', '2020-08-31 02:48:25'),
(25, 1, 'Updated Petty Cash Voucher to q', '2020-08-31 02:48:33'),
(26, 0, 'Create new child information', '2020-09-06 05:44:52'),
(27, 1, 'Create new child information', '2020-09-06 05:45:24'),
(28, 1, 'Create new cycle', '2020-09-06 06:52:25'),
(29, 1, 'Create new cycle', '2020-09-06 06:56:49'),
(30, 1, 'Create new cycle', '2020-09-06 07:01:48'),
(31, 1, 'Create new cycle', '2020-09-06 07:02:06'),
(32, 0, 'Updated cycle', '2020-09-06 07:08:08'),
(33, 1, 'Updated cycle', '2020-09-06 07:08:24'),
(34, 1, 'Updated cycle', '2020-09-06 07:08:36'),
(35, 1, 'Added new feeding to jochelle  bravo', '2020-09-07 07:11:27'),
(36, 1, 'Added new feeding to cian lim', '2020-09-07 07:13:27'),
(37, 1, 'Added new feeding to cian lim', '2020-09-07 07:22:40'),
(38, 1, 'Added new feeding to jochelle  bravo', '2020-09-07 07:23:26'),
(39, 1, 'Added new feeding to cian lim', '2020-09-07 07:27:50'),
(40, 1, 'Added new feeding to cian lim', '2020-09-07 07:29:13'),
(41, 1, 'Added new feeding to jochelle  bravo', '2020-09-07 07:30:42'),
(42, 1, 'Deleted Feeding', '2020-09-07 07:33:32'),
(43, 1, 'Deleted Feeding', '2020-09-07 07:33:45'),
(44, 1, 'Deleted Feeding', '2020-09-07 07:33:58'),
(45, 1, 'Deleted Feeding', '2020-09-07 07:34:07'),
(46, 1, 'Deleted Feeding', '2020-09-07 07:35:02'),
(47, 1, 'Deleted Feeding', '2020-09-07 07:35:19'),
(48, 1, 'Deleted Feeding', '2020-09-07 08:04:58'),
(49, 1, 'Added new feeding to cian lim', '2020-09-07 08:05:22'),
(50, 1, 'Added new feeding to jochelle  bravo', '2020-09-07 08:05:29'),
(51, 1, 'Deleted Feeding', '2020-09-07 08:05:37'),
(52, 1, 'Update Nutrional Standard', '2020-09-13 09:32:38'),
(53, 1, 'Update Nutrional Standard', '2020-09-13 09:32:38'),
(54, 1, 'Update Nutrional Standard', '2020-09-13 09:32:38'),
(55, 1, 'Update Nutrional Standard', '2020-09-13 09:32:38'),
(56, 1, 'Update Nutrional Standard', '2020-09-13 09:32:56'),
(57, 1, 'Update Nutrional Standard', '2020-09-13 09:32:56'),
(58, 1, 'Update Nutrional Standard', '2020-09-13 09:32:56'),
(59, 1, 'Update Nutrional Standard', '2020-09-13 09:32:56'),
(60, 1, 'Update Nutrional Standard', '2020-09-13 09:33:21'),
(61, 1, 'Update Nutrional Standard', '2020-09-13 09:33:21'),
(62, 1, 'Update Nutrional Standard', '2020-09-13 09:33:21'),
(63, 1, 'Update Nutrional Standard', '2020-09-13 09:33:21'),
(64, 1, 'Update Nutrional Standard', '2020-09-13 09:33:43'),
(65, 1, 'Update Nutrional Standard', '2020-09-13 09:33:43'),
(66, 1, 'Update Nutrional Standard', '2020-09-13 09:33:43'),
(67, 1, 'Update Nutrional Standard', '2020-09-13 09:33:43'),
(68, 1, 'Update Nutrional Standard', '2020-09-13 09:34:24'),
(69, 1, 'Update Nutrional Standard', '2020-09-13 09:34:24'),
(70, 1, 'Update Nutrional Standard', '2020-09-13 09:34:24'),
(71, 1, 'Update Nutrional Standard', '2020-09-13 09:34:24'),
(72, 1, 'Update Nutrional Standard', '2020-09-13 09:34:34'),
(73, 1, 'Update Nutrional Standard', '2020-09-13 09:34:34'),
(74, 1, 'Update Nutrional Standard', '2020-09-13 09:34:34'),
(75, 1, 'Update Nutrional Standard', '2020-09-13 09:34:34'),
(76, 1, 'Update Nutrional Standard', '2020-09-13 09:43:27'),
(77, 1, 'Update Nutrional Standard', '2020-09-13 09:43:27'),
(78, 1, 'Update Nutrional Standard', '2020-09-13 09:43:27'),
(79, 1, 'Update Nutrional Standard', '2020-09-13 09:47:04'),
(80, 1, 'Update Nutrional Standard', '2020-09-13 09:47:04'),
(81, 1, 'Update Nutrional Standard', '2020-09-13 09:47:04'),
(82, 1, 'Update Nutrional Standard', '2020-09-13 09:47:37'),
(83, 1, 'Update Nutrional Standard', '2020-09-13 09:47:37'),
(84, 1, 'Update Nutrional Standard', '2020-09-13 09:47:37'),
(85, 1, 'Update Nutrional Standard', '2020-09-13 09:47:51'),
(86, 1, 'Update Nutrional Standard', '2020-09-13 09:47:51'),
(87, 1, 'Update Nutrional Standard', '2020-09-13 09:47:51'),
(88, 1, 'Update Nutrional Standard', '2020-09-13 09:48:11'),
(89, 1, 'Update Nutrional Standard', '2020-09-13 09:48:11'),
(90, 1, 'Update Nutrional Standard', '2020-09-13 09:48:11'),
(91, 1, 'Update Nutrional Standard', '2020-09-13 09:48:37'),
(92, 1, 'Update Nutrional Standard', '2020-09-13 09:48:37'),
(93, 1, 'Update Nutrional Standard', '2020-09-13 09:48:37'),
(94, 1, 'Update Nutrional Standard', '2020-09-13 09:49:02'),
(95, 1, 'Update Nutrional Standard', '2020-09-13 09:49:02'),
(96, 1, 'Update Nutrional Standard', '2020-09-13 09:49:02'),
(97, 1, 'Update Nutrional Standard', '2020-09-13 09:49:17'),
(98, 1, 'Update Nutrional Standard', '2020-09-13 09:49:17'),
(99, 1, 'Update Nutrional Standard', '2020-09-13 09:49:17'),
(100, 1, 'Update Nutrional Standard', '2020-09-13 09:49:17'),
(101, 1, 'Update Nutrional Standard', '2020-09-13 09:49:24'),
(102, 1, 'Update Nutrional Standard', '2020-09-13 09:49:24'),
(103, 1, 'Update Nutrional Standard', '2020-09-13 09:49:24'),
(104, 1, 'Update Nutrional Standard', '2020-09-13 09:49:45'),
(105, 1, 'Update Nutrional Standard', '2020-09-13 09:49:45'),
(106, 1, 'Update Nutrional Standard', '2020-09-13 09:49:45'),
(107, 1, 'Update Nutrional Standard', '2020-09-13 09:52:18'),
(108, 1, 'Update Nutrional Standard', '2020-09-13 09:52:18'),
(109, 1, 'Update Nutrional Standard', '2020-09-13 09:52:18'),
(110, 1, 'Update Nutrional Standard', '2020-09-13 09:52:40'),
(111, 1, 'Update Nutrional Standard', '2020-09-13 09:52:40'),
(112, 1, 'Update Nutrional Standard', '2020-09-13 09:52:41'),
(113, 1, 'Update Nutrional Standard', '2020-09-13 09:53:52'),
(114, 1, 'Update Nutrional Standard', '2020-09-13 09:53:52'),
(115, 1, 'Update Nutrional Standard', '2020-09-13 09:53:52'),
(116, 1, 'Update Nutrional Standard', '2020-09-13 09:54:43'),
(117, 1, 'Update Nutrional Standard', '2020-09-13 09:54:43'),
(118, 1, 'Update Nutrional Standard', '2020-09-13 09:54:43'),
(119, 1, 'Update Nutrional Standard', '2020-09-13 09:55:00'),
(120, 1, 'Update Nutrional Standard', '2020-09-13 09:55:00'),
(121, 1, 'Update Nutrional Standard', '2020-09-13 09:55:00'),
(122, 1, 'Update Nutrional Standard', '2020-09-13 09:55:15'),
(123, 1, 'Update Nutrional Standard', '2020-09-13 09:55:15'),
(124, 1, 'Update Nutrional Standard', '2020-09-13 09:55:15'),
(125, 1, 'Update Nutrional Standard', '2020-09-13 09:56:00'),
(126, 1, 'Update Nutrional Standard', '2020-09-13 09:56:00'),
(127, 1, 'Update Nutrional Standard', '2020-09-13 09:56:00'),
(128, 1, 'Update Nutrional Standard', '2020-09-13 09:56:19'),
(129, 1, 'Update Nutrional Standard', '2020-09-13 09:56:19'),
(130, 1, 'Update Nutrional Standard', '2020-09-13 09:56:19'),
(131, 1, 'Update Nutrional Standard', '2020-09-13 09:56:43'),
(132, 1, 'Update Nutrional Standard', '2020-09-13 09:56:43'),
(133, 1, 'Update Nutrional Standard', '2020-09-13 09:56:43'),
(134, 1, 'Update Nutrional Standard', '2020-09-13 09:56:45'),
(135, 1, 'Update Nutrional Standard', '2020-09-13 09:56:45'),
(136, 1, 'Update Nutrional Standard', '2020-09-13 09:56:45'),
(137, 1, 'Update Nutrional Standard', '2020-09-13 10:08:22'),
(138, 1, 'Update Nutrional Standard', '2020-09-13 10:08:22'),
(139, 1, 'Update Nutrional Standard', '2020-09-13 10:08:53'),
(140, 1, 'Update Nutrional Standard', '2020-09-13 10:08:53'),
(141, 1, 'Update Nutrional Standard', '2020-09-13 10:08:53'),
(142, 1, 'Update Nutrional Standard', '2020-09-13 10:08:59'),
(143, 1, 'Update Nutrional Standard', '2020-09-13 10:08:59'),
(144, 1, 'Update Nutrional Standard', '2020-09-13 10:08:59'),
(145, 1, 'Update Nutrional Standard', '2020-09-13 10:08:59'),
(146, 1, 'Added new feeding to cian lim', '2020-09-13 10:16:23'),
(147, 1, 'Deleted Feeding', '2020-09-13 10:16:47'),
(148, 1, 'Deleted Feeding', '2020-09-13 10:16:50'),
(149, 1, 'Added new feeding to cian lim', '2020-09-13 10:16:57'),
(150, 1, 'Deleted Feeding', '2020-09-13 10:17:54'),
(151, 1, 'Added new feeding to cian lim', '2020-09-13 10:18:17'),
(152, 1, 'Deleted Feeding', '2020-09-13 10:20:20'),
(153, 1, 'Added new feeding to cian lim', '2020-09-13 10:20:31'),
(154, 1, 'Deleted Feeding', '2020-09-13 10:21:22'),
(155, 1, 'Added new feeding to cian lim', '2020-09-13 10:21:29'),
(156, 1, 'Deleted Feeding', '2020-09-13 10:30:25'),
(157, 1, 'Added new feeding to cian lim', '2020-09-13 10:30:35'),
(158, 1, 'Deleted Feeding', '2020-09-13 10:31:05'),
(159, 1, 'Added new feeding to cian lim', '2020-09-13 10:31:11'),
(160, 1, 'Deleted Feeding', '2020-09-13 10:32:48'),
(161, 1, 'Added new feeding to cian lim', '2020-09-13 10:32:53'),
(162, 1, 'Added new feeding to cian lim', '2020-09-14 10:13:43'),
(163, 1, 'Added new feeding to jochelle  bravo', '2020-09-14 10:24:22'),
(164, 0, 'Updated child informationcianq syq limq', '2020-09-15 09:31:04'),
(165, 1, 'Updated child information cianq syq limq', '2020-09-15 09:31:33'),
(166, 1, 'Updated child information cianq syq limq', '2020-09-15 09:42:41'),
(167, 1, 'Updated child information cianq syq limq', '2020-09-15 09:42:57'),
(168, 1, 'Updated child information cianq syq limq', '2020-09-15 09:43:29'),
(169, 1, 'Updated child information cianq syq limq', '2020-09-15 09:43:44'),
(170, 1, 'Updated child information cianq syq limq', '2020-09-15 09:46:15'),
(171, 1, 'Updated child information cianq syq limq', '2020-09-15 09:51:16'),
(172, 1, 'Updated child information cianq syq limq', '2020-09-15 09:53:30'),
(173, 1, 'Updated child information cianq syq limq', '2020-09-15 10:03:04'),
(174, 1, 'Updated child information cianq syq limq', '2020-09-15 10:03:48'),
(175, 1, 'Updated child information cianq syq limq', '2020-09-15 10:07:56'),
(176, 1, 'Updated child information cianq syq limq', '2020-09-15 10:08:14'),
(177, 1, 'Updated child information cianq syq limq', '2020-09-15 10:08:36'),
(178, 1, 'Updated child information cianq syq limq', '2020-09-15 10:09:00'),
(179, 1, 'Updated child information cianq syq limq', '2020-09-15 10:09:48'),
(180, 1, 'Updated child information cianq syq limq', '2020-09-15 10:12:34'),
(181, 1, 'Updated child information cianq syq limq', '2020-09-15 10:13:35'),
(182, 1, 'Updated child information cianq syq limq', '2020-09-15 10:14:03'),
(183, 1, 'Updated child information cianq syq limq', '2020-09-15 10:16:01'),
(184, 1, 'Updated child information cianq syq limq', '2020-09-15 10:16:38'),
(185, 1, 'Updated child information cianq syq limq', '2020-09-15 10:16:45'),
(186, 1, 'Create new child information', '2020-09-15 10:25:44'),
(187, 1, 'Create new child information', '2020-09-15 10:28:37'),
(188, 1, 'Create new child information', '2020-09-15 10:29:05'),
(189, 1, 'Create new child information', '2020-09-15 10:31:36'),
(190, 1, 'Create new child information', '2020-09-15 10:35:19'),
(191, 1, 'Create new child information', '2020-09-15 10:35:53'),
(192, 2, 'Closed feeding', '2020-09-23 09:55:59'),
(193, 2, 'Closed feeding', '2020-09-23 10:00:54'),
(194, 0, 'Create new staff account', '2020-09-24 05:10:40'),
(195, 0, 'Create new staff account', '2020-09-24 05:11:25'),
(196, 2, 'Create new staff account', '2020-09-24 05:12:16'),
(197, 2, 'Created new eligibility Record to John Doe', '2020-09-24 05:14:28'),
(198, 2, 'Updated eligibility Record to John Doe', '2020-09-24 05:16:37'),
(199, 2, 'Created new Petty Cash Voucher to John Doe', '2020-09-24 05:17:20'),
(200, 2, 'Created new cycle', '2020-09-24 05:20:49'),
(201, 2, 'Closed feeding', '2020-09-24 05:21:22'),
(202, 1, 'Added new feeding to jochelle  bravo', '2020-09-24 05:28:02'),
(203, 1, 'Deleted Feeding', '2020-09-24 05:30:51'),
(204, 1, 'Added new feeding to jochelle  bravo', '2020-09-24 05:31:13'),
(205, 1, 'Added new feeding to test first name test Last name', '2020-09-24 05:31:30'),
(206, 2, 'Closed feeding', '2020-09-24 05:34:50'),
(207, 1, 'Deleted Feeding', '2020-09-24 05:36:13'),
(208, 1, 'Added new feeding to cianq limq', '2020-09-24 05:40:16'),
(209, 1, 'Deleted Feeding', '2020-09-24 05:43:06'),
(210, 1, 'Added new feeding to cianq limq', '2020-09-24 05:43:16'),
(211, 1, 'Create new child information', '2020-09-24 05:46:33'),
(212, 33, 'Created new eligibility Record to John Doe', '2020-09-24 05:50:22'),
(213, 2, 'Update Nutrional Standard', '2020-09-24 06:25:53'),
(214, 2, 'Update Nutrional Standard', '2020-09-24 06:25:53'),
(215, 2, 'Update Nutrional Standard', '2020-09-24 06:25:53'),
(216, 2, 'Update Nutrional Standard', '2020-09-24 06:25:53'),
(217, 2, 'Update Nutrional Standard', '2020-09-24 06:26:21'),
(218, 2, 'Update Nutrional Standard', '2020-09-24 06:26:21'),
(219, 2, 'Update Nutrional Standard', '2020-09-24 06:26:21'),
(220, 2, 'Update Nutrional Standard', '2020-09-24 06:26:21'),
(221, 1, 'Added new feeding to John Smith', '2020-10-22 09:02:43'),
(222, 1, 'Deleted Feeding', '2020-10-22 09:03:07'),
(223, 1, 'Added new feeding to jochelle  bravo', '2020-10-22 09:04:00'),
(224, 2, 'Updated eligibility Record to John Doe', '2020-10-22 09:09:47'),
(225, 2, 'Created new eligibility Record to Richelle Bravo', '2020-10-22 09:11:02'),
(226, 1, 'Updated child information XIAN apostolero Canlas', '2020-10-22 09:13:05'),
(227, 1, 'Deleted Feeding', '2020-10-22 09:15:22'),
(228, 1, 'Deleted Feeding', '2020-10-22 09:31:55'),
(229, 1, 'Added new feeding to XIAN Canlas', '2020-10-22 09:34:45'),
(230, 1, 'Added new feeding to test first name test Last name', '2020-10-22 09:34:53'),
(231, 1, 'Added new feeding to XIAN Canlas', '2020-10-22 09:35:09'),
(232, 2, 'Create new staff account', '2020-10-22 10:33:59'),
(233, 2, 'Create new staff account', '2020-10-22 10:49:23'),
(234, 2, 'Create new staff account', '2020-10-22 10:53:49'),
(235, 2, 'Create new staff account', '2020-10-22 10:57:20'),
(236, 2, 'Create new staff account', '2020-10-22 11:11:12'),
(237, 41, 'Created new eligibility Record to joshua tan', '2020-10-22 11:14:21'),
(238, 41, 'Created new Petty Cash Voucher to rica dee', '2020-10-22 11:15:46'),
(239, 2, 'Create new staff account', '2020-10-22 11:40:33'),
(240, 2, 'Create new staff account', '2020-10-22 11:43:45'),
(241, 2, 'Create new staff account', '2020-10-22 11:47:49'),
(242, 2, 'Create new staff account', '2020-10-22 11:51:53'),
(243, 45, 'Created new eligibility Record to ann delarosa', '2020-10-22 11:54:06'),
(244, 45, 'Created new Petty Cash Voucher to jean see', '2020-10-22 11:55:26'),
(245, 2, 'Create new staff account', '2020-10-23 12:26:10'),
(246, 46, 'Created new eligibility Record to rene rose antipona', '2020-10-23 12:28:31'),
(247, 46, 'Created new Petty Cash Voucher to teresa  pabuaya', '2020-10-23 12:29:35'),
(248, 46, 'Created new Petty Cash Voucher to teresa  pabuaya', '2020-10-23 12:29:35'),
(249, 2, 'Created new cycle', '2020-10-23 12:34:44'),
(250, 28, 'Create new child information', '2020-10-23 12:38:53'),
(251, 28, 'Added new feeding to john alipala', '2020-10-23 12:39:48'),
(252, 2, 'Create new staff account', '2020-10-23 12:47:36'),
(253, 47, 'Created new eligibility Record to nestor dacles', '2020-10-23 12:49:37'),
(254, 47, 'Created new eligibility Record to nestor dacles', '2020-10-23 12:49:37'),
(255, 47, 'Created new Petty Cash Voucher to riza bacabac', '2020-10-23 12:50:35'),
(256, 2, 'Create new staff account', '2020-10-23 01:00:59'),
(257, 48, 'Created new eligibility Record to joy villanueva', '2020-10-23 01:03:05'),
(258, 48, 'Created new Petty Cash Voucher to johnny tan', '2020-10-23 01:03:49'),
(259, 48, 'Created new Petty Cash Voucher to johnny tan', '2020-10-23 01:03:49'),
(260, 28, 'Added new feeding to jochelle  bravo', '2020-10-23 01:06:53'),
(261, 28, 'Added new feeding to John Smith', '2020-10-23 01:07:09'),
(262, 28, 'Deleted Feeding', '2020-10-23 01:13:29'),
(263, 28, 'Added new feeding to XIAN Canlas', '2020-10-23 01:16:07'),
(264, 2, 'Create new staff account', '2020-10-23 01:20:03'),
(265, 49, 'Created new eligibility Record to anna montinola', '2020-10-23 01:22:19'),
(266, 49, 'Created new eligibility Record to anna montinola', '2020-10-23 01:22:19'),
(267, 49, 'Created new Petty Cash Voucher to riza basas', '2020-10-23 01:23:12'),
(268, 28, 'Added new feeding to jochelle  bravo', '2020-10-23 01:26:53'),
(269, 28, 'Added new feeding to jochelle  bravo', '2020-10-23 02:12:39'),
(270, 2, 'Create new staff account', '2020-10-23 02:17:35'),
(271, 50, 'Created new eligibility Record to maria delacruz', '2020-10-23 02:19:16'),
(272, 50, 'Created new Petty Cash Voucher to johnny tan', '2020-10-23 02:19:59'),
(273, 50, 'Created new Petty Cash Voucher to johnny tan', '2020-10-23 02:19:59'),
(274, 2, 'Create new staff account', '2020-10-23 02:36:20'),
(275, 51, 'Created new eligibility Record to alnora walda', '2020-10-23 02:37:54'),
(276, 51, 'Created new Petty Cash Voucher to Arcadia Maritinez', '2020-10-23 02:38:37'),
(277, 2, 'Updated eligibility Record to Macy Esponilla', '2020-10-23 09:44:02'),
(278, 2, 'Updated eligibility Record to Macy Esponilla', '2020-10-23 09:44:02'),
(279, 2, 'Updated eligibility Record to Richelle Bravo', '2020-10-23 11:33:11'),
(280, 2, 'Updated eligibility Record to Richelle Bravo', '2020-10-23 11:35:03'),
(281, 2, 'Updated eligibility Record to Richelle Bravo', '2020-10-23 11:35:03'),
(282, 28, 'Added new feeding to john alipala', '2020-10-23 12:20:31'),
(283, 2, 'Updated eligibility Record to nestor dacles', '2020-10-24 09:56:33'),
(284, 2, 'Updated eligibility Record to nestor dacles', '2020-10-24 09:56:33'),
(285, 28, 'Deleted Feeding', '2020-10-24 10:00:25'),
(286, 28, 'Deleted Feeding', '2020-10-24 10:00:25'),
(287, 2, 'Update Nutrional Standard', '2020-10-24 10:36:08'),
(288, 2, 'Update Nutrional Standard', '2020-10-24 10:36:08'),
(289, 2, 'Update Nutrional Standard', '2020-10-24 10:36:08'),
(290, 2, 'Update Nutrional Standard', '2020-10-24 10:36:08'),
(291, 2, 'Update Nutrional Standard', '2020-10-24 10:36:08'),
(292, 2, 'Update Nutrional Standard', '2020-10-24 10:36:08'),
(293, 2, 'Update Nutrional Standard', '2020-10-24 10:36:09'),
(294, 2, 'Update Nutrional Standard', '2020-10-24 10:36:09'),
(295, 28, 'Added new feeding to john alipala', '2020-10-24 10:51:15'),
(296, 28, 'Create new child information', '2020-10-24 11:08:11'),
(297, 28, 'Updated child information joshua Alimania Delos reyes', '2020-10-24 11:08:54'),
(298, 28, 'Updated child information joshua Alimania Delos reyes', '2020-10-24 11:08:54'),
(299, 28, 'Added new feeding to joshua Delos reyes', '2020-10-24 06:50:52'),
(300, 28, 'Deleted Feeding', '2020-10-24 06:51:36'),
(301, 28, 'Added new feeding to joshua Delos reyes', '2020-10-24 06:51:49'),
(302, 28, 'Deleted Feeding', '2020-10-24 07:08:35'),
(303, 2, 'Update Nutrional Standard', '2020-10-28 10:48:20'),
(304, 2, 'Update Nutrional Standard', '2020-10-28 10:48:20'),
(305, 2, 'Update Nutrional Standard', '2020-10-28 10:48:21'),
(306, 2, 'Update Nutrional Standard', '2020-10-28 10:48:21'),
(307, 28, 'Added new feeding to john alipala', '2020-10-29 12:31:44'),
(308, 28, 'Deleted Feeding', '2020-10-29 12:33:05'),
(309, 28, 'Added new feeding to john alipala', '2020-10-29 12:33:21'),
(310, 28, 'Deleted Feeding', '2020-10-29 12:34:11'),
(311, 28, 'Added new feeding to john alipala', '2020-10-29 12:38:32'),
(312, 28, 'Deleted Feeding', '2020-10-29 12:38:48'),
(313, 28, 'Added new feeding to john alipala', '2020-10-29 12:39:03'),
(314, 28, 'Deleted Feeding', '2020-10-29 12:46:29'),
(315, 28, 'Added new feeding to joshua Delos reyes', '2020-10-29 12:46:45'),
(316, 28, 'Updated child information vins Alimania Delos reyes', '2020-10-29 12:49:15'),
(317, 28, 'Added new feeding to john alipala', '2020-10-29 12:49:52'),
(318, 28, 'Create new child information', '2020-10-29 12:56:20'),
(319, 28, 'Deleted Feeding', '2020-10-29 12:58:55'),
(320, 28, 'Added new feeding to john montinola', '2020-10-29 01:08:44'),
(321, 29, 'Create new child information', '2020-10-29 01:55:25'),
(322, 29, 'Added new feeding to John Rey Nadales', '2020-10-29 01:56:34'),
(323, 1, 'Added new feeding to XIAN Canlas', '2020-10-29 06:54:05'),
(324, 1, 'Create new child information', '2020-10-29 06:56:51'),
(325, 2, 'Created new Petty Cash Voucher to dsadas', '2020-11-08 09:53:02');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_petty_cash`
--

CREATE TABLE `tbl_petty_cash` (
  `pettycash_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `payee` varchar(225) NOT NULL,
  `address` text NOT NULL,
  `reason` text NOT NULL,
  `amount` decimal(12,2) NOT NULL,
  `date_added` datetime NOT NULL,
  `finish_by` int(11) NOT NULL,
  `date_received` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_petty_cash`
--

INSERT INTO `tbl_petty_cash` (`pettycash_id`, `user_id`, `payee`, `address`, `reason`, `amount`, `date_added`, `finish_by`, `date_received`) VALUES
(2, 1, 'q', 'qqqq', '', '0.00', '2020-08-31 09:18:05', 0, '0000-00-00 00:00:00'),
(3, 1, 'dsad', 'dasd', '', '0.00', '2020-08-31 09:20:34', 0, '0000-00-00 00:00:00'),
(4, 1, 'sdad', 'sadas', '', '1243214.00', '2020-08-31 09:20:50', 0, '0000-00-00 00:00:00'),
(5, 1, 'sdad', 'sadas', '', '1243214.00', '2020-07-31 09:20:50', 0, '0000-00-00 00:00:00'),
(6, 2, 'John Doe', 'tangub', '', '25000.00', '2020-09-24 05:17:20', 0, '0000-00-00 00:00:00'),
(7, 41, 'rica dee', 'cansilayan', '', '1000.00', '2020-10-22 11:15:46', 0, '0000-00-00 00:00:00'),
(8, 45, 'jean see', 'Blumentritt', '', '1000.00', '2020-10-22 11:55:26', 0, '0000-00-00 00:00:00'),
(9, 46, 'teresa  pabuaya', 'Blumentritt', '', '1000.00', '2020-10-23 12:29:34', 0, '0000-00-00 00:00:00'),
(10, 46, 'teresa  pabuaya', 'Blumentritt', '', '1000.00', '2020-10-23 12:29:35', 0, '0000-00-00 00:00:00'),
(11, 47, 'riza bacabac', 'zone V1', '', '1000.00', '2020-10-23 12:50:35', 0, '0000-00-00 00:00:00'),
(12, 48, 'johnny tan', 'Blumentritt', '', '1000.00', '2020-10-23 01:03:49', 0, '0000-00-00 00:00:00'),
(13, 48, 'johnny tan', 'Blumentritt', '', '1000.00', '2020-10-23 01:03:49', 0, '0000-00-00 00:00:00'),
(14, 49, 'riza basas', 'zone V', '', '1000.00', '2020-10-23 01:23:12', 0, '0000-00-00 00:00:00'),
(15, 50, 'johnny tan', 'Blumentritt', '', '1000.00', '2020-10-23 02:19:59', 0, '0000-00-00 00:00:00'),
(16, 50, 'johnny tan', 'Blumentritt', '', '1000.00', '2020-10-23 02:19:59', 0, '0000-00-00 00:00:00'),
(17, 51, 'Arcadia Maritinez', 'Blumentritt', '', '1000.00', '2020-10-23 02:38:37', 0, '0000-00-00 00:00:00'),
(18, 2, 'dsadas', 'dsadas', 'sadasdsad', '12421.00', '2020-11-08 09:53:02', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_standard`
--

CREATE TABLE `tbl_standard` (
  `standard_id` int(11) NOT NULL,
  `status` varchar(2) NOT NULL,
  `start_range` decimal(12,2) NOT NULL,
  `end_range` decimal(12,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_standard`
--

INSERT INTO `tbl_standard` (`standard_id`, `status`, `start_range`, `end_range`) VALUES
(1, 'N', '0.01', '10.99'),
(2, 'UW', '11.00', '20.99'),
(3, 'SU', '21.00', '30.00'),
(4, 'OW', '31.00', '40.00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `user_id` int(11) NOT NULL,
  `fname` varchar(225) NOT NULL,
  `mname` varchar(225) NOT NULL,
  `lname` varchar(225) NOT NULL,
  `bday` date NOT NULL,
  `contact_number` varchar(225) NOT NULL,
  `address` text NOT NULL,
  `un` varchar(225) NOT NULL,
  `pw` varchar(225) NOT NULL,
  `status` varchar(225) NOT NULL,
  `filename` text NOT NULL,
  `email` varchar(225) NOT NULL,
  `archive_status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`user_id`, `fname`, `mname`, `lname`, `bday`, `contact_number`, `address`, `un`, `pw`, `status`, `filename`, `email`, `archive_status`) VALUES
(1, 'Lucy ', 'Sy', 'Pardenal', '1987-08-30', '09125673346', 'zone V', 'lucy', 'qqq', 'V', '1590469994.png', 'lucy@gmail.com', 0),
(2, 'Ezra', '', 'tan', '2020-05-22', '5325', '132', 'dswd', 'admin123', 'A', '1597973767.png', '', 0),
(3, 'asdas', '', 'dsad', '2020-05-27', '1412', '', 'das', 'dsa', 'C', '', '', 0),
(4, 'sadsa', '', 'das', '2020-05-01', '3423', '', 'sdas', 'dsa', 'C', '', '', 0),
(5, 'das', '', 'dasda', '2020-05-22', '414', '', '1241', 'q', 'C', '', '', 0),
(17, 'John', '', 'Doe', '2020-05-26', '42343', 'rewr', 'jd', '12345', 'L', '1597909582.png', '', 0),
(18, 'editha', '', 'barrioga', '1987-09-09', '09123544512', 'Lopez jaena', 'editha', '12345', 'S', '1590643326.png', 'joshuaarzaga@yaho.com', 0),
(23, 'emelta', '', 'letrama', '1989-06-08', '09485232343', 'Blumentritt', 'emelta', '12345', 's', '', 'emelta@gmail.com', 0),
(24, 'juan', '', 'delacruz', '1990-07-01', '09500238475', 'Brgy.Tangub', 'jiwon_lee', '12345', 'S', '1597973208.png', 'juandelacruz@gmail.com', 1),
(25, 'razel', '', 'soberano', '1986-08-23', '09426453441', 'Sta. cruz', 'sad', '12345', 'S', '', 'razel@gmail.com', 0),
(26, 'Connie', '', 'Jayme', '1987-09-24', '09120123463', 'mambucal', 'connie', '12345', 'V', '', 'connnie@gmail.com', 0),
(27, 'Genelyn', '', 'Benjamin', '1989-09-01', '09182358671', 'zone IV', 'genelyn', '12345', 'V', '', 'genelyn@gmail.com', 0),
(28, 'Nida', '', 'Italia', '1989-03-27', '09082467241', 'minoyan', 'nida', '12345', 'V', '', 'sadsad@gmail.com', 0),
(29, 'Rebecca', '', 'Tajanlangit', '1989-08-29', '09193048134', 'Pandanon', 'rebecca', '12345', 'V', '', 'rebecca@gmail.com', 0),
(30, 'Joannce ', '', 'Roce', '1990-07-23', '09485268712', 'Blumentritt', 'joannce', '12345', 'V', '', 'joannce@gmail.com', 0),
(31, 'Mary ann', '', 'Mabayog', '1991-08-29', '09485269710', 'Iglau-an', 'ann', '12345', 'S', '', 'mabayo@gmail.com', 1),
(32, 'Erminia', '', 'marasigan', '2020-08-01', '09234567781', 'zone III', 'erminia', '12345', 'S', '', 'erminia@gmail.com', 0),
(33, 'Lida', '', 'Malibong', '2018-02-15', '09452352084', 'cansilayan', 'doo', '12345', 'S', '', 'lidamalibong@gmail.com', 0),
(34, 'Sarah', '', 'Delos reyes', '1995-09-10', '09485264711', 'Blumentritt', 'sarah', '12345', 'S', '', 'sarah0@gmai.com', 0),
(35, 'maria', '', 'Delos reyes', '1988-12-06', '09576321345', 'Blumentritt', 'maria', '12345', 'V', '', 'maria@gmail.com', 0),
(36, 'rosario', '', 'ledesma', '1987-10-07', '09281235131', 'Blumentritt', 'rosario', '12345', 'S', '', 'rosario@gmail.com', 0),
(37, 'robert', '', 'bacabac', '1985-12-31', '09143526245', 'Blumentritt', 'robert', '12345', 'S', '', 'robert@gmail.com', 0),
(38, 'john', '', 'delacruz', '1991-12-30', '09485268712', 'Blumentritt', 'john', '12345', 'S', '', 'john@gmail.com', 0),
(39, 'irene', '', 'pamatian', '1991-11-29', '09485268712', 'Blumentritt', 'irene', '12345', 'S', '', 'irene@gmail.com', 0),
(40, 'norberto', '', 'navaja', '1992-11-30', '09423621355', 'zone V', 'norberto', '12345', 'S', '', 'norberto@gmail.com', 0),
(41, 'chuck', '', 'palmes', '1993-10-29', '09394857721', 'zone V', 'chuck', '12345', 'S', '', 'chuck@gmail.com', 0),
(42, 'mico ', '', 'agno', '1992-12-30', '09435767621', 'mambucal', 'mico', '12345', 'S', '', 'mico@gmai.com', 0),
(43, 'roxanne', '', 'casiple', '1994-10-27', '09294856251', 'zone V', 'roxanne', '12345', 'S', '', 'roxanne@gmail.com', 0),
(44, 'jean', '', 'Rodrigo', '2013-08-27', '09567123542', 'zone V', 'jean', '12345', 'S', '', 'jean@gmail.com', 0),
(45, 'melvin', '', 'goto', '1992-09-29', '09458621242', 'Simon village', 'melvin', '12345', 'S', '', 'melvin@gmail.com', 0),
(46, 'matthew ', '', 'bantam', '1994-11-27', '09586721351', 'zone II', 'matthew', '12345', 'S', '', 'matthew@gmail.com', 0),
(47, 'carl', '', 'magbanua', '1993-06-25', '09094367523', 'zone 1', 'carl', '12345', 'S', '', 'carl@gmail.com', 0),
(48, 'jerry', '', 'agno', '1992-09-29', '094857612351', 'Blumentritt', 'jerry', '12345', 'S', '', 'jerry@gmail.com', 0),
(49, 'noryean', '', 'sanches', '1990-09-30', '09485763123', 'brgy. 2', 'noryean', '12345', 'S', '', 'noryean@gmail.com', 0),
(50, 'marlene', '', 'jocson', '1994-06-27', '09198456723', 'zone VII', 'marlene', '12345', 'S', '', 'marlene@gmail.com', 0),
(51, 'serge', '', 'tribaco', '1992-11-28', '09094123521', 'zone VIII', 'serge', '12345', 'S', '', 'serge@gmail.com', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_child`
--
ALTER TABLE `tbl_child`
  ADD PRIMARY KEY (`child_id`);

--
-- Indexes for table `tbl_eligibility`
--
ALTER TABLE `tbl_eligibility`
  ADD PRIMARY KEY (`eligibility_id`);

--
-- Indexes for table `tbl_event`
--
ALTER TABLE `tbl_event`
  ADD PRIMARY KEY (`event_id`);

--
-- Indexes for table `tbl_family`
--
ALTER TABLE `tbl_family`
  ADD PRIMARY KEY (`family_id`);

--
-- Indexes for table `tbl_feeding_details`
--
ALTER TABLE `tbl_feeding_details`
  ADD PRIMARY KEY (`feeding_id`);

--
-- Indexes for table `tbl_feeding_header`
--
ALTER TABLE `tbl_feeding_header`
  ADD PRIMARY KEY (`cycle_id`);

--
-- Indexes for table `tbl_logs`
--
ALTER TABLE `tbl_logs`
  ADD PRIMARY KEY (`logs_id`);

--
-- Indexes for table `tbl_petty_cash`
--
ALTER TABLE `tbl_petty_cash`
  ADD PRIMARY KEY (`pettycash_id`);

--
-- Indexes for table `tbl_standard`
--
ALTER TABLE `tbl_standard`
  ADD PRIMARY KEY (`standard_id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_child`
--
ALTER TABLE `tbl_child`
  MODIFY `child_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `tbl_eligibility`
--
ALTER TABLE `tbl_eligibility`
  MODIFY `eligibility_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `tbl_event`
--
ALTER TABLE `tbl_event`
  MODIFY `event_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_family`
--
ALTER TABLE `tbl_family`
  MODIFY `family_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `tbl_feeding_details`
--
ALTER TABLE `tbl_feeding_details`
  MODIFY `feeding_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `tbl_feeding_header`
--
ALTER TABLE `tbl_feeding_header`
  MODIFY `cycle_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_logs`
--
ALTER TABLE `tbl_logs`
  MODIFY `logs_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=326;

--
-- AUTO_INCREMENT for table `tbl_petty_cash`
--
ALTER TABLE `tbl_petty_cash`
  MODIFY `pettycash_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `tbl_standard`
--
ALTER TABLE `tbl_standard`
  MODIFY `standard_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
