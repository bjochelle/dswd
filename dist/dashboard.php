<div class="subheader py-2 py-lg-6  subheader-solid " id="kt_subheader">
    <div class=" container-fluid  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
        <div class="d-flex align-items-center flex-wrap mr-1">
			<div class="d-flex align-items-baseline flex-wrap mr-5">
	            <h5 class="text-dark font-weight-bold my-1 mr-5">
					Dashboard
				</h5>
	        </div>
        </div>
    </div>
</div>
<!--end::Subheader-->

    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class=" container ">
            <!-- begin: Invoice-->
             <div class="row  py-8 px-8  px-md-0">
                 <div class="col-md-6">
                <!--begin::Card-->
                <div class="card card-custom gutter-b">
                    <div class="card-header">
                        <div class="card-title">
                            <h3 class="card-label">
                                MISSION
                            </h3>
                        </div>
                    </div>
                    <div class="card-body">
                        <!--begin::Chart-->
                        <p>To lead in the formulation, implementation, and coordination of social welfare and development policies and programs for and with the poor vulnerable and disadvantage.</p>
                        <!--end::Chart-->
                    </div>
                </div>
                <!--end::Card-->          
                </div>

                  <div class="col-md-6">
                <!--begin::Card-->
                <div class="card card-custom gutter-b">
                    <div class="card-header">
                        <div class="card-title">
                            <h3 class="card-label">
                                VISION
                            </h3>
                        </div>
                    </div>
                    <div class="card-body">
                        <!--begin::Chart-->
                        <p>The department of social welfare and development envisions all filipinos free from hunger and poverty, have equal accessto oppurtunities, enabled by a fair, abd peaceful society.</p>
                        <!--end::Chart-->
                    </div>
                </div>
                <!--end::Card-->          
                </div>
           <?php if($profile['status'] != 'V'){?>


           
                <div class="col-md-12">
                <!--begin::Card-->
                <div class="card card-custom gutter-b">
                    <div class="card-header">
                        <div class="card-title">
                            <h3 class="card-label">
                                Petty Cash
                            </h3>
                        </div>
                    </div>
                    <div class="card-body">
                        <!--begin::Chart-->
                        <div id="chart_3"></div>
                        <!--end::Chart-->
                    </div>
                </div>
                <!--end::Card-->          
                </div>

                  <div class="col-md-12">
                <!--begin::Card-->
                <div class="card card-custom gutter-b">
                    <div class="card-header">
                        <div class="card-title">
                            <h3 class="card-label">
                                Eligibility
                            </h3>
                        </div>
                    </div>
                    <div class="card-body">
                        <!--begin::Chart-->
                        <div id="chart_1"></div>
                        <!--end::Chart-->
                    </div>
                </div>
                <!--end::Card-->
                                    
                </div>
           
            <?php }else{

            	$fetch_cycle = mysql_query("SELECT * FROM `tbl_feeding_header` where status = 0")or die(mysql_error());
            	while($row_cycle = mysql_fetch_array($fetch_cycle)){?>
            	  <div class="col-md-6">
                <!--begin::Card-->
                <div class="card card-custom gutter-b">
                    <div class="card-header">
                        <div class="card-title">
                            <h3 class="card-label">
                                <span class="fas fa-bullhorn"></span> Feeding Announcement 
                            </h3>
                        </div>
                    </div>
                    <div class="card-body">
                    	<div class="wizard-label">
                                <h3 class="wizard-title">
                                     <span class="fas fa-redo"></span>  Feeding Cyle <span style="float: right;"> <?php echo $row_cycle['cycle'];?></span>
                                </h3>
                        </div>
                        <div class="wizard-label">
                                <h3 class="wizard-title">
                                    <span class="fas fa-calendar-day"></span> Date Started <span style="float: right;"> <?php echo date('F d,Y',strtotime($row_cycle['date_added']));?></span>
                                </h3>
                        </div>
                        
                        <div class="wizard-label">
                                <h3 class="wizard-title">
                                     <span class="fas fa-user-tie"></span>  Created By <span style="float: right;"> <?php echo getFullName($row_cycle['user_id']);?></span>
                                </h3>
                        </div>
                        <div class="wizard-label">
                                <h3 class="wizard-title">
                                     <span style="float: right;"><a href="home.php?view=addFeeding&id=<?php echo $row_cycle['cycle_id'];?>"><button class="btn btn-sm btn-primary"><span class="
                                     	fas fa-utensils"></span> Start Feeding</button></a></span>
                                </h3>
                        </div>
                    </div>
                </div>
                <!--end::Card-->
                                    
                </div>
            <?php }}?>
             </div>
            <!-- end: Invoice-->
        </div>
        <!--end::Container-->
    </div>
<!--end::Entry-->
<script>
    $(document).ready(function(){
        getChart();
        getChart1();

    });

    function getChart() {
    	 
		const apexChart = "#chart_3";
		var options = {
			  chart: {
			      height: 350,
			      type: 'bar',
			  },
			  plotOptions:
			   {
				bar: {
					horizontal: false,
					columnWidth: '55%',
					endingShape: 'rounded'
				},
			},
			  series: [],
			  dataLabels: {
				enabled: false
			},
			stroke: {
				show: true,
				width: 2,
				colors: ['transparent']
			},
			xaxis: {
				categories: ['Jan','Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct','Nov','Dec'],
			},
			yaxis: {
				title: {
					text: 'Php (Amount)'
				}
			},
			fill: {
				opacity: 1
			},
			tooltip: {
				y: {
					formatter: function (val) {
						return "Php " + val 
					}
				}
			},
  noData: {
    text: 'Loading...'
  }
}

var chart = new ApexCharts(
  document.querySelector(apexChart),
  options
);

chart.render();

var url ="ajax/getChart.php"
$.getJSON(url, function(response) {
  chart.updateSeries([{
    name: 'Amount',
    data: response
  }])
});

	}

	    function getChart1() {
    	 
		const apexChart = "#chart_1";
		var options = {
			  chart: {
			      height: 350,
			      type: 'bar',
			  },
			  plotOptions:
			   {
				bar: {
					horizontal: false,
					columnWidth: '55%',
					endingShape: 'rounded'
				},
			},
			  series: [],
			  dataLabels: {
				enabled: false
			},
			stroke: {
				show: true,
				width: 2,
				colors: ['transparent']
			},
			xaxis: {
				categories: ['Jan','Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct','Nov','Dec'],
			},
			yaxis: {
				title: {
					text: 'Php (Amount)'
				}
			},
			fill: {
				opacity: 1
			},
			tooltip: {
				y: {
					formatter: function (val) {
						return "Php " + val 
					}
				}
			},
  noData: {
    text: 'Loading...'
  }
}

var chart = new ApexCharts(
  document.querySelector(apexChart),
  options
);

chart.render();

var url ="ajax/getChart1.php"
$.getJSON(url, function(response) {
  chart.updateSeries([{
    name: 'Amount',
    data: response
  }])
});
}


</script>