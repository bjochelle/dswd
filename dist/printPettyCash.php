<?php 
$id = $_GET['id'];
    $el = mysql_fetch_array(mysql_query("SELECT * FROM tbl_petty_cash where pettycash_id='$id'"));
    function convert_number_to_words($number) {
   
        $hyphen      = '-';
        $conjunction = '  ';
        $separator   = ' ';
        $negative    = 'negative ';
        $decimal     = ' pesos and ';
        $dictionary  = array(
            0                   => 'Zero',
            1                   => 'One',
            2                   => 'Two',
            3                   => 'Three',
            4                   => 'Four',
            5                   => 'Five',
            6                   => 'Six',
            7                   => 'Seven',
            8                   => 'Eight',
            9                   => 'Nine',
            10                  => 'Ten',
            11                  => 'Eleven',
            12                  => 'Twelve',
            13                  => 'Thirteen',
            14                  => 'Fourteen',
            15                  => 'Fifteen',
            16                  => 'Sixteen',
            17                  => 'Seventeen',
            18                  => 'Eighteen',
            19                  => 'Nineteen',
            20                  => 'Twenty',
            30                  => 'Thirty',
            40                  => 'Fourty',
            50                  => 'Fifty',
            60                  => 'Sixty',
            70                  => 'Seventy',
            80                  => 'Eighty',
            90                  => 'Ninety',
            100                 => 'Hundred',
            1000                => 'Thousand',
            1000000             => 'Million',
            1000000000          => 'Billion',
            1000000000000       => 'Trillion',
            1000000000000000    => 'Quadrillion',
            1000000000000000000 => 'Quintillion'
        );
       
        if (!is_numeric($number)) {
            return false;
        }
       
        if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
            // overflow
            trigger_error(
                'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
                E_USER_WARNING
            );
            return false;
        }
     
        if ($number < 0) {
            return $negative . convert_number_to_words(abs($number));
        }
       
        $string = $fraction = null;
       
        if (strpos($number, '.') !== false) {
            list($number, $fraction) = explode('.', $number);
        }
       
        switch (true) {
            case $number < 21:
                $string = $dictionary[$number];
                break;
            case $number < 100:
                $tens   = ((int) ($number / 10)) * 10;
                $units  = $number % 10;
                $string = $dictionary[$tens];
                if ($units) {
                    $string .= $hyphen . $dictionary[$units];
                }
                break;
            case $number < 1000:
                $hundreds  = $number / 100;
                $remainder = $number % 100;
                $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
                if ($remainder) {
                    $string .= $conjunction . convert_number_to_words($remainder);
                }
                break;
            default:
                $baseUnit = pow(1000, floor(log($number, 1000)));
                $numBaseUnits = (int) ($number / $baseUnit);
                $remainder = $number % $baseUnit;
                $string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
                if ($remainder) {
                    $string .= $remainder < 100 ? $conjunction : $separator;
                    $string .= convert_number_to_words($remainder);
                }
                break;
        }
       
        if (null !== $fraction && is_numeric($fraction)) {
            $string .= $decimal;
            $words = array();
            foreach (str_split((string) $fraction) as $number) {
                $words[] = $dictionary[$number];
            }
            $string .= implode(' ', $words);
        }
       
        return $string;
    }

?>
<style type="text/css" media="Print">
    @media print{
     .col-md-1{width: 8%; float: left;}
     .col-md-2{width: 16%; float: left;}
     .col-md-3{width: 25%; float: left;}
     .col-md-4{width: 33%; float: left;}
     .col-md-5{width: 42%; float: left;}
     .col-md-6{width: 50%; float: left;}
     .col-md-7{width: 58%; float: left;}
     .col-md-8{width: 66%; float: left;}
     .col-md-9{width: 75%; float: left;}
     .col-md-10{width: 83%; float: left;}
     .col-md-11{width: 92%; float: left;}
     .col-md-12{width: 100%; float: left;}
     body{background: white;}
    }
</style>
<div class="subheader py-2 py-lg-6  subheader-solid " id="kt_subheader">
    <div class=" container-fluid  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
        <div class="d-flex align-items-center flex-wrap mr-1">
			<div class="d-flex align-items-baseline flex-wrap mr-5">
	            <h5 class="text-dark font-weight-bold my-1 mr-5">
					Petty Cash Voucher
				</h5>

	             <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
	                <li class="breadcrumb-item">
	                    <a href="#"class="text-muted">Print</a>
					</li>
	            </ul>
	        </div>
        </div>
         <button type="button" class="btn btn-primary font-weight-bold pull-right" onclick="printDiv();">Print Form</button>
    </div>
</div>
<!--end::Subheader-->

<!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class=" container ">
            <!-- begin::Card-->
<div class="card card-custom overflow-hidden" id="report_data" style="background: white;">
    <div class="card-body p-0">
        <!-- begin: Invoice-->
        <!-- begin: Invoice header-->
            <div class="col-md-12">
                <div class=" justify-content-between pb-10 pt-10 pb-md-20 flex-column flex-md-row">
                     
                    <h1 class="display-4 text-center font-weight-boldest "><div class="col-md-3 ">
                       <img src="assets/media/logo.jpg" style="width: 100%;">
                    </div>PETTY CASH VOUCHER</h1>
                    <h3 class="text-center">Municipality of Murcia</h3>
                </div>
                <h3 class="text-left">No. </h3>

            </div>
        <!-- end: Invoice header-->
        <!-- begin: Invoice footer-->
        <div class="row justify-content-center">
   
         <div class="flex-column mb-10 mb-md-0 p-10">
            <div class="col-md-12">
                   
                        <div class="row">
                               
                                <div class="col-md-6">
                                    <div class="font-weight-boldest font-size-lg">
                                    Payee : <span class="font-weight-bold"> <?php echo ucwords($el['payee']);?></span>
                                    </div>
                                    <div class="font-weight-boldest font-size-lg">
                                        <div class="pl-0 pt-7">Address :
                                            <span class="font-weight-bold">  <?php echo ucwords($el['address']);?></span>
                                        </div>
                                    </div>
                                    <div class="font-weight-boldest font-size-lg">
                                        <div class="pl-0 pt-7">Particulars:<br><br><br>
                                        <span class="font-weight-bold">
                                        To payment for financial assitance for _____ charge to emergency Assistance/AIDE TO  INDIVUDUAL CRISI SITUATION as per supporting papaers herto attached the amount of 
                                     </span>
                                        </div>
                                    </div>
                                    <div class="font-weight-boldest font-size-lg">
                                        <div class="pl-0 pt-7"> Amount  :<span class="font-weight-bold">  <?php echo convert_number_to_words($el['amount'])." centavos";?></span>
                                        </div>
                                    </div>

                                    <div class="font-weight-boldest font-size-lg">
                                        <div class="pl-0 pt-7"> Note  :<span class="font-weight-bold">  <?php echo ucwords($el['reason']);?></span>
                                        </div>
                                    </div>

                                    <div class="font-weight-boldest font-size-lg">
                                        <div class="pl-0 pt-7"> Approved by  :<span class="font-weight-bold">  </span>
                                        </div>
                                    </div>

                                    <br><br><br><br>
                                    <div class="font-weight-boldest font-size-lg">
                                        <div class="col-md-12 text-center ">
                                            <hr style="border-top: 1px solid black;">
                                            <span class="font-weight-bolder mb-2"> ANA L. NARANJA </span><br>OIC-MSWDO
                                        </div>
                                    </div>
                                    <div class="font-weight-boldest font-size-lg">
                                        <div class="pl-0 pt-7"> Paid by  :<span class="font-weight-bold">  </span>
                                        </div>
                                    </div>
                                    <br><br><br><br>
                                    <div class="font-weight-boldest font-size-lg">
                                        <div class="col-md-12 text-center ">
                                            <hr style="border-top: 1px solid black;">
                                            <span class="font-weight-bolder mb-2"> LEVY C. PADASAY </span><br>CASHIER III
                                        </div>
                                    </div>
                                    
                                </div>

                                <div class="col-md-6">
                                    <div class="font-weight-boldest font-size-lg">
                                        <div class="pl-0 pt-7">Date:<br>
                                            <span class="font-weight-bold"><?php echo $date;?></span>
                                        </div>
                                    </div>
                                    <div class="font-weight-boldest font-size-lg">
                                        <div class="pl-0 pt-7"> Responsibility Center :<br>
                                            <span class="font-weight-bold"></span>
                                        </div>
                                    </div>
                                    <div class="font-weight-boldest font-size-lg">
                                        <div class="pl-0 pt-7"> Amount  :<br>
                                        <br><br><br>
                                        <div class="col-md-12 text-right ">
                                            <span class="font-weight-bold"> &#8369; <?php echo $el['amount'];?> </span>
                                        </div>
                                        </div>
                                    </div>
                                    <br><br><br><br><br><br><br><br><br>
                                    <div class="font-weight-boldest font-size-lg">
                                        <div class="col-md-12 text-center ">
                                        <span class="font-weight-bolder mb-2"> <?php echo ucwords($el['payee']);?></span>
                                            <hr style="border-top: 1px solid black;margin:0px;">
                                            Signature over Printed Name
                                        </div>
                                    </div>
                                    <br>
                                    <div class="font-weight-boldest font-size-lg">
                                        <div class="pl-0 pt-7"> Date Received:<span class="font-weight-bold">  </span>
                                        </div>
                                    </div>
                                    

                                    
                                </div>
                    </div>
                </div>
            </div>
        <!-- end: Invoice footer-->
        <!-- end: Invoice-->
    </div>
</div>
<!-- end::Card-->
        </div>
        <!--end::Container-->
    </div>
<!--end::Entry-->
<script type="text/javascript">
    function printDiv() 
{

  var divToPrint=document.getElementById('report_data');

  var newWin=window.open('','Print-Window');

  newWin.document.open();

  newWin.document.write('<html><link href="assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css"/><link href="assets/plugins/custom/prismjs/prismjs.bundle.css" rel="stylesheet" type="text/css"/><link href="assets/css/style.bundle.css" rel="stylesheet" type="text/css"/><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

  newWin.document.close();

  setTimeout(function(){newWin.close();},10);

}
</script>