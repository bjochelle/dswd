<?php 
    $child_id= $_GET['id'];

    $child_row = mysql_fetch_array(mysql_query("SELECT * FROM  tbl_child where child_id='$child_id'"));

    $count_cycle = mysql_num_rows(mysql_query("SELECT * FROM  tbl_feeding_header where user_id='$id'"));
    $count_feed = mysql_num_rows(mysql_query("SELECT * FROM  tbl_feeding_details where child_id='$child_id'"));
    $total_feeding = $count_cycle * 3;

    if($total_feeding == 0){
        $progress_perc = 0;

    }else{
    $progress_perc = ($count_feed/$total_feeding)*100;

    }


?>
<div class="subheader py-2 py-lg-6  subheader-solid " id="kt_subheader">
    <div class=" container-fluid  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
        <div class="d-flex align-items-center flex-wrap mr-1">
			<div class="d-flex align-items-baseline flex-wrap mr-5">
	            <h5 class="text-dark font-weight-bold my-1 mr-5">
					View children Information
				</h5>

	             <!-- <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
	                <li class="breadcrumb-item">
	                    <a href=""class="text-muted">General</a>
					</li>
	                 <li class="breadcrumb-item">
	                    <a href="#" class="text-muted"> dasdsa Page</a>
					</li>
	            </ul> -->
	        </div>
        </div>
    </div>
</div>
<!--end::Subheader-->

<!--begin::Entry-->
                <div class="d-flex flex-column-fluid">
                    <!--begin::Container-->
                    <div class=" container ">
                        <!--begin::Card-->
            <div class="card card-custom gutter-b">
                <div class="card-body">
                    <!--begin::Details-->
                    <div class="d-flex mb-9">
                        <!--begin: Pic-->
                        <div class="flex-shrink-0 mr-7 mt-lg-0 mt-3">
                            <div class="symbol symbol-50 symbol-lg-120">
                                <img src="assets/media/users/blank.png" alt="image"/>
                            </div>
            
                            <div class="symbol symbol-50 symbol-lg-120 symbol-primary d-none">
                                <span class="font-size-h3 symbol-label font-weight-boldest">JM</span>
                            </div>
                        </div>
                        <!--end::Pic-->
            
                        <!--begin::Info-->
                        <div class="flex-grow-1">
                            <!--begin::Title-->
                            <div class="d-flex justify-content-between flex-wrap mt-1">
                                <div class="d-flex mr-3">
                                    <a href="#" class="text-dark-75 text-hover-primary font-size-h5 font-weight-bold mr-3"><?php echo ucwords($child_row['fname']." ".$child_row['mname']." ".$child_row['lname']);?></a>
                                    <a href="#"><i class="flaticon2-correct text-success font-size-h5"></i></a>
                                </div>
                            </div>
                            <!--end::Title-->
            
                            <!--begin::Content-->
                            <div class="d-flex flex-wrap justify-content-between mt-1">
                                <div class="d-flex flex-column flex-grow-1 pr-8">
                                    <div class="d-flex flex-wrap mb-4">
                                        <a href="#" class="text-dark-50 text-hover-primary font-weight-bold mr-lg-8 mr-5 mb-lg-0 mb-2"><i class="fas fa-bible mr-2 font-size-lg"></i> <?php echo $child_row['religion'];?></a>
                                        <a href="#" class="text-dark-50 text-hover-primary font-weight-bold mr-lg-8 mr-5 mb-lg-0 mb-2"><i class="fas fa-transgender-alt mr-2 font-size-lg"></i> <?php echo $child_row['gender'];?> </a>
                                        <a href="#" class="text-dark-50 text-hover-primary font-weight-bold"><i class="fas fa-calendar-day font-size-lg"></i> <?php echo date("F d, Y",strtotime($child_row['bday']));?></a>
                                    </div>
            
                                    <span class="font-weight-bold text-dark-50"><i class="fas fa-map-pin font-size-lg"></i> <?php echo $child_row['address'];?> </span>
                                </div>
            
                                <div class="d-flex align-items-center w-25 flex-fill float-right mt-lg-12 mt-8">
                                    <span class="font-weight-bold text-dark-75">Feeding Progress</span>
                                    <div class="progress progress-xs mx-3 w-100">
                                        <div class="progress-bar bg-success" role="progressbar" style="width: <?php echo number_format($progress_perc);?>%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                    <span class="font-weight-bolder text-dark"><?php echo number_format($progress_perc);?>%</span>
                                </div>
                            </div>
                            <!--end::Content-->
                        </div>
                        <!--end::Info-->
                    </div>
                    <!--end::Details-->
                </div>
            </div>
            <!--end::Card-->
            
            <!--begin::Row-->
            <div class="row">
                <div class="col-lg-12">
                    <!--begin::Advance Table Widget 2-->
            <div class="card card-custom card-stretch gutter-b">
                <!--begin::Header-->
                <div class="card-header border-0 pt-5">
                    <h3 class="card-title align-items-start flex-column">
                        <span class="card-label font-weight-bolder text-dark">Child Information </span>
                    </h3>
                </div>
                <!--end::Header-->
            
                <!--begin::Body-->
                <div class="card-body pt-3 pb-0">
                    <label><strong>Family Composition: </strong></label>
                    <div class="table-responsive">
                      <table class="table table-bordered table-hover table-checkable"  style="width: 100%">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Relationship</th>
                                <th>Age</th>
                                <th>Civil Status</th>
                                <th>Occupation</th>
                                <th>Income</th>
                            </tr>
                        </thead>
                            <tbody id="fam_body">
                                <?php 
                                $fam_fetch = mysql_query("SELECT * FROM `tbl_family`  where child_id='$child_id'");
                                while ($fam_row = mysql_fetch_array($fam_fetch)) {?>
                                    <tr>
                                        <td><?php echo $fam_row['fam_name'];?></td>
                                        <td><?php echo $fam_row['fam_rel'];?></td>
                                        <td><?php echo $fam_row['fam_age'];?></td>
                                        <td><?php echo $fam_row['fam_civil_status'];?></td>
                                        <td><?php echo $fam_row['fam_occupation'];?></td>
                                        <td><?php echo $fam_row['fam_income'];?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                       </table>
                    </div>
                    <br>
                    <label class="card-label font-weight-bolder text-dark"><strong>Problems/Nees Commonly Encounterd</strong></label>
                    <br>
                    <label>Living/Residing With:  <?php echo $child_row['livingwt'];?> </label>
                    <br>
                    <label>Areas of interest:  <?php echo $child_row['a_int'];?> </label>
                    <br><br>
                    <label class="card-label font-weight-bolder text-dark"><strong>Problem/needs commonly encounter(Parent)</strong></label>
                    <br>
                    <label>A. Economic:  <?php echo $child_row['prob'];?> </label><br>
                    <label>B. Socio/Emiotional(Child):  <?php echo $child_row['socio'];?> </label><br>
                    <label>C. Health(Child):  <?php echo $child_row['health'];?> </label><br><br>
                    <label class="card-label font-weight-bolder text-dark"><strong> Deworming </strong></label><br>
                    <div class="row">
                    <div class='col-md-6'> First  Dose : <?php echo $child_row['deworming_fir'];?></div>
                    <div class='col-md-6'> Second  Dose : <?php echo $child_row['deworming_sec'];?></div></div><br>
                     <label><strong>Feeding History: </strong></label>
                    <table class="table table-bordered table-hover table-checkable"  style="margin-top: 13px !important">
                        <thead>
                            
                            <tr>
                                <th class="p-0" >#</th>
                                <th class="p-0" >BMI</th>
                                <th class="p-0" >Feed Type</th>
                                <th class="p-0" >Status</th>
                                <th class="p-0" >Date</th>  
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                 <?php 
                                $fetch_feeding = mysql_query("SELECT * FROM `tbl_feeding_details`  where child_id='$child_id'");
                                $count = 1;
                                while ($row_feeding = mysql_fetch_array($fetch_feeding)) {
                                     if($row_feeding['isfeedingremarks']==1){
                                        $value ='Before feeding';
                                    }else if($row_feeding['isfeedingremarks']==2){
                                        $value ='After 60 days';
                                    }else{
                                        $value ='After 120 days';
                                    }

                                    if($row_feeding['standard_id']==1){
                                        $status ='Normal';
                                    }else if($row_feeding['standard_id']==2){
                                        $status ='Underweight';
                                    }else if($row_feeding['standard_id']==3){
                                        $status ='Severely Underweight';
                                    }else if($row_feeding['standard_id']==4){
                                        $status ='Over Weight';
                                    }else{
                                        $status ='BMI out of range';
                                    }

                                    ?>
                                    <tr>
                                        <td><?php echo $count++;?></td>
                                        <td><?php echo $row_feeding['bmi'];?></td>
                                        <td><?php echo $value;?></td>
                                        <td><?php echo $status;?></td>
                                        <td><?php echo date('F d, Y',strtotime($row_feeding['date_added']));?></td>
                                    </tr>
                                <?php } ?>
                            </tr>
                        </tbody>
                    </table>

                </div>
                <!--end::Body-->
            </div>
            <!--end::Advance Table Widget 2-->
                </div>
            
            </div>
            <!--end::Row-->
            
        </div>
        <!--end::Container-->
    </div>
<!--end::Entry-->