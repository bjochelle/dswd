	<!-- Modal Edit-->
	<div class="modal fade" id="editModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Cycle</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
			<form class="form" id="kt_form_edit">
                    <div class="form-group row">
						<label class="col-form-label text-right col-lg-3 col-sm-12">Cycle *</label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<input required type="text" class="form-control" name="cycle" placeholder="Enter your Firstname" id="cycle" />
						</div>
                    </div>
                    <div class="form-group row">
						<label class="col-form-label text-right col-lg-3 col-sm-12">Date Started *</label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<input required type="date" class="form-control" name="date_added" placeholder="date_added" id="date_added" />
						</div>
                    </div>

                    
					<input type="hidden" class="form-control" name="status" value="Update"/>
					<input type="hidden" class="form-control" name="cycle_id" id='id'/>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary font-weight-bold">Save changes</button>
			</div>
			</form>
        </div>
    </div>
</div>
<!--end: Modal Edit-->