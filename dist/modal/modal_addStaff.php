	<!-- Modal Add-->
    <div class="modal fade" id="staticBackdrop" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Volunteer</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
			<form class="form" id="kt_form_1">
                    <div class="form-group row">
						<label class="col-form-label text-right col-lg-3 col-sm-12">Firstname *</label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<input required type="text" class="form-control" name="fname" placeholder="Enter your Firstname"/>
						</div>
                    </div>

                    <div class="form-group row">
						<label class="col-form-label text-right col-lg-3 col-sm-12">Lastname *</label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<input required type="text" class="form-control" name="lname" placeholder="Enter your Lastname"/>
						</div>
                    </div>

                    <div class="form-group row">
						<label class="col-form-label text-right col-lg-3 col-sm-12">Address *</label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<input required type="text" class="form-control" name="address" placeholder="Enter your Address"/>
						</div>
                    </div>

                    <div class="form-group row">
						<label class="col-form-label text-right col-lg-3 col-sm-12">Birthdate *</label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<input required type="date" class="form-control" name="bday" placeholder="Enter your Address"/>
						</div>
                    </div>

                    
					<div class="form-group row">
						<label class="col-form-label text-right col-lg-3 col-sm-12">Email *</label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<input required type="email" class="form-control" name="email" placeholder="Enter your email"/>
						</div>
                    </div>

					<div class="form-group row">
						<label class="col-form-label text-right col-lg-3 col-sm-12">Phone * </label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<div class="input-group">
								<input required type="number" class="form-control" name="contact_number" placeholder="Enter phone"/>
							</div>
						</div>
                    </div>
                    
                    <div class="form-group row">
						<label class="col-form-label text-right col-lg-3 col-sm-12">Username *</label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<input required type="text" class="form-control" name="un" placeholder="Enter your Address"/>
						</div>
					</div>
					
					<input type="hidden" class="form-control" name="status" value="Create"/>

		
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary font-weight-bold">Save changes</button>
			</div>
			</form>
        </div>
    </div>
</div>
<!--end: Modal Add-->