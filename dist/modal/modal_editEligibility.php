	<!-- Modal Add-->
    <div class="modal fade" id="editModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">View/Edit Eligibility</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
			<form class="form" id="kt_form_edit">
				<h6>Applicant Information</h6>
				
                    <div class="form-group row">
						<label class="col-form-label text-right col-lg-3 col-sm-12">Applicant</label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<input required type="text" class="form-control" name="name" placeholder="Enter your Name" id="name"/>
						</div>
                    </div>

                    <div class="form-group row">
						<label class="col-form-label text-right col-lg-3 col-sm-12">Age *</label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<input required type="number" min="18" class="form-control" name="age" placeholder="Enter your age" id="age" oninvalid="this.setCustomValidity('Below the legal age')"/>
						</div>
                    </div>

                    <div class="form-group row">
						<label class="col-form-label text-right col-lg-3 col-sm-12">Address *</label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<input required type="text" class="form-control" name="address" placeholder="Enter your address" id="address"/>
						</div>
                    </div>
                    <hr>

				<h6>Benificiary Information</h6>

					<div class="form-group row">
						<label class="col-form-label text-right col-lg-3 col-sm-12">Benificiary </label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<input required type="text" class="form-control" name="patient_name" placeholder="Enter your name" id="patient_name"/>
						</div>
                    </div>

                    <div class="form-group row">
						<label class="col-form-label text-right col-lg-3 col-sm-12">Age *</label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<input required type="number" min="1" class="form-control" name="patient_age" placeholder="Enter your age" id="patient_age"/>
						</div>
                    </div>

                    <div class="form-group row">
						<label class="col-form-label text-right col-lg-3 col-sm-12">Relationship *</label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<input required type="text" class="form-control" name="patient_rel" placeholder="Enter your relationship" id="patient_rel"/>
						</div>
					</div>
                    <hr>

                    <div class="form-group row">
						<label class="col-form-label text-right col-lg-3 col-sm-12">Amount of : *</label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<input required type="number" class="form-control" name="amount" placeholder="Enter amount" id="amount"/>
						</div>
					</div>

                    <div class="form-group row">
						<label class="col-form-label text-right col-lg-3 col-sm-12">Invest for : *</label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<input required type="text" class="form-control" name="inv" placeholder="Enter investment for" id="inv"/>
						</div>
					</div>

					<div class="form-group row">
						<label class="col-form-label text-right col-lg-3 col-sm-12">Pharmacy Name : *</label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<input required type="text" class="form-control" name="pharmacy_number" id="pharmacy_number" placeholder="Pharmacy Name"/>
						</div>
					</div>

					<div class="form-group row">
						<label class="col-form-label text-right col-lg-3 col-sm-12">Receipt No. : *</label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<input required type="text" class="form-control" name="rcpt_no" id="rcpt_no" placeholder="Receipt No."/>
						</div>
					</div>
					
					
					<input type="hidden" class="form-control" name="status" value="Update"/>
					<input type="hidden" class="form-control" name="id" id="id"/>


		
            </div>
            <div class="modal-footer" id="modal_footer">
                <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary font-weight-bold">Save changes</button>
			</div>
			</form>
        </div>
    </div>
</div>
<!--end: Modal Add-->