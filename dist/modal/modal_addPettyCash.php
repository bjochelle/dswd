	<!-- Modal Add-->
    <div class="modal fade" id="staticBackdrop" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Petty Cash Voucher</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
			<form class="form" id="kt_form_1">
                    <div class="form-group row">
						<label class="col-form-label text-right col-lg-3 col-sm-12">Appicant Name</label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<input required type="text" class="form-control" name="name" placeholder="Enter your Name"/>
						</div>
                    </div>

                    <div class="form-group row">
						<label class="col-form-label text-right col-lg-3 col-sm-12">Address *</label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<input required type="text" class="form-control" name="address" placeholder="Enter your address"/>
						</div>
                    </div>

                    <div class="form-group row">
						<label class="col-form-label text-right col-lg-3 col-sm-12">Amount *</label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<input required type="number" min="1000.00" step="0.01" class="form-control" name="amount" placeholder="Enter your amount"/>
						</div>
                    </div>
                    
                    <div class="form-group row">
                        <label class="col-form-label text-right col-lg-3 col-sm-12">Note *</label>
                        <div class="col-lg-9 col-md-9 col-sm-12">
                            <input type="text" class="form-control" name="reason" placeholder="Enter your note"/>
                        </div>
                    </div>
                    

					<input type="hidden" class="form-control" name="status" value="Create"/>

		
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary font-weight-bold">Save changes</button>
			</div>
			</form>
        </div>
    </div>
</div>
<!--end: Modal Add-->