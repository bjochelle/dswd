	<!-- Modal Add-->
    <div class="modal fade" id="editModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">View/Edit Eligibility</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
			<form class="form" id="kt_form_edit">
            <div class="form-group row">
						<label class="col-form-label text-right col-lg-3 col-sm-12">Appicant Name</label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<input required type="text" class="form-control" name="name" placeholder="Enter your Name" id="name"/>
						</div>
                    </div>

                    <div class="form-group row">
						<label class="col-form-label text-right col-lg-3 col-sm-12">Address *</label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<input required type="text" class="form-control" name="address" placeholder="Enter your address" id="address"/>
						</div>
                    </div>

                    <div class="form-group row">
						<label class="col-form-label text-right col-lg-3 col-sm-12">Amount *</label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<input required type="number" min="1000.00" step="0.01" class="form-control" name="amount" placeholder="Enter your amount" id="amount"/>
						</div>
                    </div>

                      <div class="form-group row">
                        <label class="col-form-label text-right col-lg-3 col-sm-12">Note *</label>
                        <div class="col-lg-9 col-md-9 col-sm-12">
                            <input type="text" class="form-control" name="reason" id="pettyCashReason"placeholder="Enter your note"/>
                        </div>
                    </div>
					
					<input type="hidden" class="form-control" name="status" value="Update"/>
					<input type="hidden" class="form-control" name="id" id="id"/>


		
            </div>
            <div class="modal-footer" id="modal_footer">
                <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary font-weight-bold">Save changes</button>
			</div>
			</form>
        </div>
    </div>
</div>
<!--end: Modal Add-->