	<!-- Modal Edit-->
	<div class="modal fade" id="editModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Event</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
			<form class="form" id="kt_form_edit">
                  <div class="form-group row">
                        <label class="col-form-label text-right col-lg-3 col-sm-12">Event Name *</label>
                        <div class="col-lg-9 col-md-9 col-sm-12">
                            <input required type="text" class="form-control" name="event_name" placeholder="Event Name" id="event_name"/>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label text-right col-lg-3 col-sm-12">Date & Time *</label>
                        <div class="col-lg-9 col-md-9 col-sm-12">
                            <div class="input-group date" id="kt_datetimepicker_2" data-target-input="nearest">
                                <input type="text" class="form-control datetimepicker-input" placeholder="Select date &amp; time" data-target="#kt_datetimepicker_2" name="event_when" id="event_when">
                                <div class="input-group-append" data-target="#kt_datetimepicker_2" data-toggle="datetimepicker">
                                    <span class="input-group-text">
                                        <i class="ki ki-calendar"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>

                     <div class="form-group row">
                        <label class="col-form-label text-right col-lg-3 col-sm-12">Where *</label>
                        <div class="col-lg-9 col-md-9 col-sm-12">
                            <input required type="text" class="form-control" name="event_where" placeholder="" id="event_where"/>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label text-right col-lg-3 col-sm-12">Description *</label>
                        <div class="col-lg-9 col-md-9 col-sm-12">
                            <textarea required type="date" class="form-control" name="event_description" id="event_description"/> </textarea>
                        </div>
                    </div>

                    
					<input type="hidden" class="form-control" name="status" value="Update"/>
					<input type="hidden" class="form-control" name="event_id" id='id'/>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary font-weight-bold">Save changes</button>
			</div>
			</form>
        </div>
    </div>
</div>
<!--end: Modal Edit-->