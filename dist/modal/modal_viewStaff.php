	<!-- Modal View-->
	<div class="modal fade" id="viewModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title view_header" id="exampleModalLabel">Volunteer Profile</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
			   <!--begin::User-->
			   <div class="d-flex align-items-center">
                    <div class="symbol symbol-60 symbol-xxl-100 mr-5 align-self-start align-self-xxl-center">
                        <div class="symbol-label" style="background-image:url('assets/media/users/300_13.jpg')"></div>
                    </div>
                    <div>
                        <a href="#" class="font-weight-bold font-size-h5 text-dark-75 text-hover-primary" id="viewname">
                           
                        </a>
                        <div class="text-muted" id="viewstatus">
                          
                        </div>
                    
                    </div>
                </div>
                <!--end::User-->

                <!--begin::Contact-->
                <div class="pt-8 pb-6">
                    <div class="d-flex align-items-center justify-content-between mb-2">
                        <span class="font-weight-bold mr-2">Email:</span>
                        <a href="#" class="text-muted text-hover-primary" id="viewemail"></a>
                    </div>
                    <div class="d-flex align-items-center justify-content-between mb-2">
                        <span class="font-weight-bold mr-2">Phone:</span>
                        <span class="text-muted" id="viewphone"></span>
                    </div>
                    <div class="d-flex align-items-center justify-content-between">
                        <span class="font-weight-bold mr-2">Birthday:</span>
                        <span class="text-muted" id="viewbday"></span>
                    </div>
                </div>
                <!--end::Contact-->

                <!--begin::Contact-->
                <div class="pb-6">
				<span class="font-weight-bold mr-2">Location:</span>
				<span class="text-muted" id="viewaddress"></span>
                </div>
                <!--end::Contact-->
			</div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
			</div>
        </div>
    </div>
</div>
<!--end: Modal View-->