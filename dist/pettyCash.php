<div class="subheader py-2 py-lg-6  subheader-solid " id="kt_subheader">
    <div class=" container-fluid  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
        <div class="d-flex align-items-center flex-wrap mr-1">
			<div class="d-flex align-items-baseline flex-wrap mr-5">
	            <h5 class="text-dark font-weight-bold my-1 mr-5">
					Petty Cash
				</h5>
	        </div>
        </div>
    </div>
</div>
<!--end::Subheader-->

<!--begin::Card-->
<div class="card card-custom">
	<div class="card-header">
		<div class="card-title">
			<span class="card-icon"><i class="flaticon-notepad text-primary"></i></span>
			<h3 class="card-label">List of Petty Cash</h3>
		</div>
		<div class="card-toolbar">
<!--begin::Button-->
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#staticBackdrop"><span class="fas fa-plus-circle"></span>
	New Record
</button>
<!--end::Button-->
		</div>
	</div>
    <?php require('modal/modal_addPettyCash.php');
        require('modal/modal_editPettyCash.php');?>

	<div class="card-body">
		<!--begin: Datatable-->
		<table class="table table-bordered table-hover table-checkable" id="kt_datatable" style="margin-top: 13px !important">
			<thead>
				<tr>
					<th></th>
					<th>Payee</th>
					<th>Address</th>
					<th>Amount</th>
					<th>Date</th>
					<th>Actions</th>
				</tr>
			</thead>
        </table>
		<!--end: Datatable-->
	</div>
</div>
<!--end::Card-->

<script>
function getData(){
      var table = $('#kt_datatable').DataTable();
      table.destroy();
      var status = 'datatable';
      $("#kt_datatable").dataTable({
        "processing":true,
        "ajax":{
          "type":"POST",
          "url":"ajax/pettyCashCrud.php",
          "dataSrc":"data",
          "data":{
            status:status
          }
        },
        "columns":[
          {
            "data":"count"
          },
          {
            "data":"payee"
          },
          {
            "data":"address"
          },
          {
            "data":"amount"
          },
          {
            "data":"date_added"
          },         
          {
            "mRender": function(data,type,row){
				return '<div class="d-flex align-items-center">'
				+'<a href="javascript:;" class="btn btn-sm btn-clean btn-icon mr-1" title="Edit details" onclick="edit('+row.id+',1)"><i class="la la-edit"></i></a>'
				+'<a href="javascript:;" class="btn btn-sm btn-clean btn-icon" title="View"><i class="la la-eye" onclick="edit('+row.id+',0)"></i></a>'
                +'<a href="javascript:;" class="btn btn-sm btn-clean btn-icon mr-1" title="Print" onclick="printForm('+row.id+')"><i class="la la-print"></i></a></div>';
            }
          }
        ]
      });
    }


	$("#kt_form_1").submit(function(e){
	  e.preventDefault();
      $.ajax({
        url:"ajax/pettyCashCrud.php",
        method:"POST",
        data:$("#kt_form_1").serialize(),
        success: function(data){
           if(data == 0){
                alertMe("Sorry, looks like there are some errors detected, please try again.","error");
		   }else{
                $("#staticBackdrop").modal("hide");
                Swal.fire({
                    title: "Do you want to print?",
                    text: "It will redirect to print page",
                    icon: "question",
                    showCancelButton: true,
                    confirmButtonText: "Yes, print it!"
                }).then(function(result) {
                    if (result.value) {
                        window.location.replace("home.php?view=printPettyCash&id="+data);
                    }else{
                        $("#kt_form_1")[0].reset();
                        getData();
                    }
                });
		   }
        }
      });
	});

	$("#kt_form_edit").submit(function(e){
	  e.preventDefault();
      var id = $("#id").val();
      $.ajax({
        url:"ajax/pettyCashCrud.php",
        method:"POST",
        data:$("#kt_form_edit").serialize(),
        success: function(data){
           if(data == 1){
                Swal.fire({
                    title: "Do you want to print?",
                    text: "It will redirect to print page",
                    icon: "question",
                    showCancelButton: true,
                    confirmButtonText: "Yes, print it!"
                }).then(function(result) {
                    if (result.value) {
                        window.location.replace("home.php?view=printPettyCash&id="+id);
                    }else{
                        $("#editModal").modal("hide");
				        getData();
                    }
                });

		   }else{
				alertMe("Sorry, looks like there are some errors detected, please try again.","error");
		   }
          
        }
      });
	});


	function edit(id,stat){
		$("#editModal").modal("show");
		$("#id").val(id);
        if(stat==1){
            $("#modal_footer").show();
        }else{
             $("#modal_footer").hide();
        }
		$.ajax({
			url:"ajax/pettyCashCrud.php",
			type:"POST",
			data:{
				id:id,
				status:'View'
			},success:function(data){
				var o = JSON.parse(data);
				$("#name").val(o.payee);
				$("#address").val(o.address);
				$("#amount").val(o.amount);
        $("#pettyCashReason").val(o.reason);


			}
		});
	}

function printForm(id){
    window.location.replace("home.php?view=printPettyCash&id="+id);
}
jQuery(document).ready(function() {
	getData();
});
</script>