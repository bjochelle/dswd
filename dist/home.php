<?php 

$page = (isset($_GET['page']) && $_GET['page'] !='') ? $_GET['page'] : '';
$view = (isset($_GET['view']) && $_GET['view'] !='') ? $_GET['view'] : '';
$sub = (isset($_GET['sub']) && $_GET['sub'] !='') ? $_GET['sub'] : '';

    session_start();
    $id= $_SESSION['id'];
    $status= $_SESSION['status'];

if($id == ""){
  header("Location: ../dist/");
}

 include "core/config.php";

date_default_timezone_set('Asia/Manila');
$date = date("Y-m-d");

$profile = mysql_fetch_array(mysql_query("SELECT * FROM tbl_user where user_id='$id'"));

if($profile['status']=='V'){
	$status_name='Volunteer';
}else if($profile['status']=='A'){
	$status_name='Admin';
}else{
	$status_name='Staff';
}

$name= $profile['fname']." ".$profile['mname']." ".$profile['lname'];



$count_feeding = mysql_num_rows(mysql_query('SELECT * FROM `tbl_feeding_header` where status =0'));

$count_event = mysql_num_rows(mysql_query('SELECT * FROM `tbl_event` where status =0'));
?>

<!DOCTYPE html>

<html lang="en" >
    <!--begin::Head-->
    <head><base href="">
                <meta charset="utf-8"/>
        <title>DSWD</title>
        <meta name="description" content="Page with empty content"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>

        <!--begin::Fonts-->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700"/>        <!--end::Fonts-->

                 


        <!--begin::Global Theme Styles(used by all pages)-->
                    <link href="assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css"/>
                    <link href="assets/plugins/custom/prismjs/prismjs.bundle.css" rel="stylesheet" type="text/css"/>
                    <link href="assets/css/style.bundle.css" rel="stylesheet" type="text/css"/>
                <!--end::Global Theme Styles-->

        <!--begin::Layout Themes(used by all pages)-->

<link href="assets/css/themes/layout/header/base/light.css" rel="stylesheet" type="text/css"/>
<link href="assets/css/themes/layout/header/menu/light.css" rel="stylesheet" type="text/css"/>
<link href="assets/css/themes/layout/brand/dark.css" rel="stylesheet" type="text/css"/>
<link href="assets/css/themes/layout/aside/dark.css" rel="stylesheet" type="text/css"/>        <!--end::Layout Themes-->

 
        <link rel="shortcut icon" href="assets/media/logos/dswd.png"/>



<!--begin::Global Theme Bundle(used by all pages)-->
<script src="assets/plugins/global/plugins.bundle.js"></script>
<script src="assets/plugins/custom/prismjs/prismjs.bundle.js"></script>
<script src="assets/js/scripts.bundle.js"></script>
<!--end::Global Theme Bundle-->

    <!--begin::Page Vendors(used by this page)-->
<script src="assets/plugins/custom/fullcalendar/fullcalendar.bundle.js"></script>
<!-- <script src="//maps.google.com/maps/api/js?key=AIzaSyBTGnKT7dt597vo9QgeQ7BFhvSRP4eiMSM"></script> -->
<script src="assets/plugins/custom/gmaps/gmaps.js"></script>
		<!--end::Page Vendors-->
		
		
<!--begin::Page Vendors Styles(used by this page)-->
<link href="assets/css/pages/wizard/wizard-2.css" rel="stylesheet" type="text/css"/>

<link href="assets/plugins/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugins/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css"/>
<script src="assets/js/pages/crud/forms/widgets/bootstrap-daterangepicker.js"></script>
<script src="assets/js/pages/crud/forms/widgets/bootstrap-datepicker.js"></script>
 <script src="assets/js/pages/crud/forms/widgets/select2.js"></script>


<!-- <script src="assets/js/pages/custom/education/student/profile.js"></script> -->
<!--end::Page Vendors Styles-->

<script>
	$(document).ready(function(){
		$('#kt_datepicker_1').datepicker({
			rtl: KTUtil.isRTL(),
			todayHighlight: true,
			orientation: '&quot;bottom left&quot;',
			templates: arrows
		});

		$('#kt_datetimepicker_1').datetimepicker();

		// $('.select2').select2({
		// 	placeholder: &quot;Select a state&quot;
		// });
	})

	function alertMe(text,icon){
		swal.fire({
			text: text,
			icon: icon,
			buttonsStyling: false,
			confirmButtonText: "Ok, got it!",
			customClass: {
				confirmButton: "btn font-weight-bold btn-light-primary"
			}
		}).then(function() {
			KTUtil.scrollTop();
		});
	}

	function alertPrint(frompage,page,id){
		
	}


</script>
            </head>
    <!--end::Head-->

    <!--begin::Body-->
    <body  id="kt_body"  class="header-fixed header-mobile-fixed subheader-enabled subheader-fixed subheader-mobile-fixed aside-enabled aside-fixed aside-minimize-hoverable page-loading"  >

    	<!--begin::Main-->
	<!--begin::Header Mobile-->
<div id="kt_header_mobile" class="header-mobile align-items-center  header-mobile-fixed " >
	<!--begin::Logo-->
	<a href="#">
		<img alt="Logo" src="assets/media/logos/dswd.png"/>
	</a>
	<!--end::Logo-->

	<!--begin::Toolbar-->
	<div class="d-flex align-items-center">
					<!--begin::Aside Mobile Toggle-->
			<button class="btn p-0 burger-icon burger-icon-left" id="kt_aside_mobile_toggle">
				<span></span>
			</button>
			<!--end::Aside Mobile Toggle-->

					<!--begin::Header Menu Mobile Toggle-->
			<button class="btn p-0 burger-icon ml-4" id="kt_header_mobile_toggle">
				<span></span>
			</button>
			<!--end::Header Menu Mobile Toggle-->

		<!--begin::Topbar Mobile Toggle-->
		<button class="btn btn-hover-text-primary p-0 ml-2" id="kt_header_mobile_topbar_toggle">
			<span class="svg-icon svg-icon-xl"><!--begin::Svg Icon | path:assets/media/svg/icons/General/User.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <polygon points="0 0 24 0 24 24 0 24"/>
        <path d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
        <path d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z" fill="#000000" fill-rule="nonzero"/>
    </g>
</svg><!--end::Svg Icon--></span>		</button>
		<!--end::Topbar Mobile Toggle-->
	</div>
	<!--end::Toolbar-->
</div>
<!--end::Header Mobile-->
	<div class="d-flex flex-column flex-root">
		<!--begin::Page-->
		<div class="d-flex flex-row flex-column-fluid page">

<!--begin::Aside-->
<div class="aside aside-left  aside-fixed  d-flex flex-column flex-row-auto"  id="kt_aside">
	<!--begin::Brand-->
	<div class="brand flex-column-auto " id="kt_brand">
		<!--begin::Logo-->
		<a href="#" class="brand-logo" style="font-size: 30px;font-weight: bolder;">
			<img alt="Logo" src="assets/media/logos/dswd.png" style="width: 30%;" /> DSWD 
		</a>
		<!--end::Logo-->

					<!--begin::Toggle-->
			<button class="brand-toggle btn btn-sm px-0" id="kt_aside_toggle">
				<span class="svg-icon svg-icon svg-icon-xl"><!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Angle-double-left.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <polygon points="0 0 24 0 24 24 0 24"/>
        <path d="M5.29288961,6.70710318 C4.90236532,6.31657888 4.90236532,5.68341391 5.29288961,5.29288961 C5.68341391,4.90236532 6.31657888,4.90236532 6.70710318,5.29288961 L12.7071032,11.2928896 C13.0856821,11.6714686 13.0989277,12.281055 12.7371505,12.675721 L7.23715054,18.675721 C6.86395813,19.08284 6.23139076,19.1103429 5.82427177,18.7371505 C5.41715278,18.3639581 5.38964985,17.7313908 5.76284226,17.3242718 L10.6158586,12.0300721 L5.29288961,6.70710318 Z" fill="#000000" fill-rule="nonzero" transform="translate(8.999997, 11.999999) scale(-1, 1) translate(-8.999997, -11.999999) "/>
        <path d="M10.7071009,15.7071068 C10.3165766,16.0976311 9.68341162,16.0976311 9.29288733,15.7071068 C8.90236304,15.3165825 8.90236304,14.6834175 9.29288733,14.2928932 L15.2928873,8.29289322 C15.6714663,7.91431428 16.2810527,7.90106866 16.6757187,8.26284586 L22.6757187,13.7628459 C23.0828377,14.1360383 23.1103407,14.7686056 22.7371482,15.1757246 C22.3639558,15.5828436 21.7313885,15.6103465 21.3242695,15.2371541 L16.0300699,10.3841378 L10.7071009,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(15.999997, 11.999999) scale(-1, 1) rotate(-270.000000) translate(-15.999997, -11.999999) "/>
    </g>
</svg><!--end::Svg Icon--></span>			</button>
			<!--end::Toolbar-->
			</div>
	<!--end::Brand-->

	<!--begin::Aside Menu-->
	<div class="aside-menu-wrapper flex-column-fluid" id="kt_aside_menu_wrapper">

		<!--begin::Menu Container-->
		<div
			id="kt_aside_menu"
			class="aside-menu my-4 "
			data-menu-vertical="1"
			 data-menu-scroll="1" data-menu-dropdown-timeout="500" 			>
          <!--begin::Menu Container-->

            <ul class="menu-nav ">

            	<li class="menu-item  menu-item-active" aria-haspopup="true" ><a  href="home.php?view=dashboard" class="menu-link ">
                   <span class="fas fa-home menu-icon"></span>
                  <span class="menu-text">Dashboard </span> 

	                <?php if($count_feeding != 0 ){?>

	                 <span class="menu-label"><span class="label label-rounded label-danger" id="feeding_announcement"><?=$count_feeding;?></span>
	             		<?php } ?>

	                  </a>
             	 </li>
              
			  <?php if($status=='A' || $status=='S'){?>
			  	<?php if($status == 'A'){?>
			  		<li class="menu-item " aria-haspopup="true" ><a  href="home.php?view=staff" class="menu-link ">
	                  <span class="fas fa-user-tie menu-icon"></span>
	                  <span class="menu-text">Staff</span></a>
				 	</li>
			  	<?php }?>
			  
			  
			  	<li class="menu-item " aria-haspopup="true" ><a  href="home.php?view=eligibility" class="menu-link ">
                  <span class="fas fa-certificate menu-icon"></span>
                  <span class="menu-text">Eligibility</span></a>
				  </li>
				  <li class="menu-item " aria-haspopup="true" ><a  href="home.php?view=pettyCash" class="menu-link ">
	                  <span class="fas fa-file-invoice-dollar menu-icon"></span>
	                  <span class="menu-text">Petty Cash Voucher</span></a>
				  </li>
				   <li class="menu-item  menu-item-submenu" aria-haspopup="true"  data-menu-toggle="hover">
				  	<a  href="javascript:;" class="menu-link menu-toggle">
					<span class="fas fa-file-alt menu-icon"></span>
					<span class="menu-text">Report</span>
					<i class="menu-arrow"></i>
				
					</a>
					<div class="menu-submenu ">
						<i class="menu-arrow"></i>
						<ul class="menu-subnav">
							<li class="menu-item " aria-haspopup="true" ><a  href="home.php?view=reportE" class="menu-link "><i class="menu-bullet menu-bullet-dot"><span></span></i><span class="menu-text">Eligibility Report</span></a></li>
							<li class="menu-item " aria-haspopup="true" ><a  href="home.php?view=reportP" class="menu-link "><i class="menu-bullet menu-bullet-dot"><span></span></i><span class="menu-text">Petty Cash Report </span></a></li>
							<li class="menu-item " aria-haspopup="true" ><a  href="home.php?view=reportFeeding" class="menu-link "><i class="menu-bullet menu-bullet-dot"><span></span></i><span class="menu-text">Feeding Report </span></a></li>
						</ul>
					</div>
				</li>

			  <?php }?>

			   
			    <?php if($status == 'A'){?>
	              <li class="menu-item " aria-haspopup="true" ><a  href="home.php?view=standard" class="menu-link ">
	                  <span class="fas fa-balance-scale menu-icon"></span>
	                  <span class="menu-text">Nutritional Status Standard</span></a>
				  </li>
				  <li class="menu-item " aria-haspopup="true" ><a  href="home.php?view=skip" class="menu-link ">
                  <span class="fas fa-list menu-icon"></span>
                  <span class="menu-text">Skip Feeding History</span></a>
			  </li>
			   <?php }?>
			    <?php if($status=='S'){?>
				<li class="menu-item " aria-haspopup="true" ><a  href="home.php?view=volunteer" class="menu-link ">
	                <span class="fas fa-hospital-user menu-icon"></span>
	                <span class="menu-text">Volunteer</span></a>
				 </li>
				 <?php }?>
		
			  <?php if($status=='V' || $status == 'A'){?>
			  
			  <li class="menu-item " aria-haspopup="true" ><a  href="home.php?view=children" class="menu-link ">
                  <span class="fas fa-child menu-icon"></span>
                  <span class="menu-text">Children</span></a>
			  </li>

			    <?php if($status=='V'){?>
			  	<li class="menu-item" aria-haspopup="true"><a  href="home.php?view=eventList" class="menu-link ">
			  	<?php }else{?>
			  		<li class="menu-item" aria-haspopup="true" ><a  href="home.php?view=event" class="menu-link ">
			  	<?php }?>
	                  <span class="fas fa-calendar menu-icon"></span>
	                  <span class="menu-text">Event</span>
	                  <?php if($count_event != 0 ){?>
	                  <span class="menu-label"><span class="label label-rounded label-danger" id="feeding_announcement"><?=$count_event;?></span></span>
	                  <?php } ?>

	              </a>
				 	</li>
			 
			  <?php }?>
			   <?php if($status!='V'){?>
			   <li class="menu-item " aria-haspopup="true" ><a  href="home.php?view=feeding" class="menu-link ">
                  <span class="fas fa-utensils menu-icon"></span>
                  <span class="menu-text">Supplementary Feeding</span></a>
			  </li>
			   <?php }?>

			
			 
              </ul>

		</div>
		<!--end::Menu Container-->
	</div>
	<!--end::Aside Menu-->
</div>
<!--end::Aside-->

			<!--begin::Wrapper-->
			<div class="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper">
				<!--begin::Header-->
<div id="kt_header" class="header  header-fixed " >
	<!--begin::Container-->
	<div class=" container-fluid  d-flex align-items-stretch justify-content-between">
					<!--begin::Header Menu Wrapper-->
			<div class="header-menu-wrapper header-menu-wrapper-left" id="kt_header_menu_wrapper">
					
			</div>
			<!--end::Header Menu Wrapper-->

		<!--begin::Topbar-->
		<div class="topbar">
		      <!--begin::User-->
		            <div class="topbar-item">
		                <div class="btn btn-icon btn-icon-mobile w-auto btn-clean d-flex align-items-center btn-lg px-2" id="kt_quick_user_toggle">
							<span class="text-muted font-weight-bold font-size-base d-none d-md-inline mr-1">Hi,</span>
		                    <span class="text-dark-50 font-weight-bolder font-size-base d-none d-md-inline mr-3"><?php echo $name;?></span>
		                    <span class="symbol symbol-lg-35 symbol-25 symbol-light-success">
		                        <span class="symbol-label font-size-h5 font-weight-bold"><?php echo $profile['status'];?></span>
		                    </span>
		                </div>
		            </div>
		            <!--end::User-->
		        		    		</div>
		<!--end::Topbar-->
	</div>
	<!--end::Container-->
</div>
<!--end::Header-->

				<!--begin::Content-->
<div class="content  d-flex flex-column flex-column-fluid" id="kt_content">
	<!--begin::Entry-->
	<div class="d-flex flex-column-fluid">
		<!--begin::Container-->
		<div class=" container ">
        <?php
        if($view == 'dashboard'){
            require 'dashboard.php';
        }else if($view == 'volunteer'){
            require 'volunteer.php';
        }else if($view == 'staff'){
            require 'staff.php';
        }else if($view == 'addChild'){
            require 'addChild.php';
        }else if($view == 'profile'){
            require 'profile.php';
        }else if($view == 'logs'){
            require 'logs.php';
        }else if($view == 'children'){
            require 'children.php';
        }else if($view == 'viewChild'){
            require 'viewChild.php';
        }else if($view == 'eligibility'){
            require 'eligibility.php';
        }else if($view == 'printEligibility'){
            require 'printEligibility.php';
        }else if($view == 'pettyCash'){
            require 'pettyCash.php';
        }else if($view == 'printPettyCash'){
            require 'printPettyCash.php';
        }else if($view == 'reportE'){
            require 'report.php';
        }else if($view == 'reportP'){
            require 'reportPetty.php';
        }else if($view == 'editChild'){
            require 'editChild.php';
        }else if($view == 'feeding'){
            require 'feeding.php';
        }else if($view == 'addFeeding'){
            require 'addFeeding.php';
        }else if($view == 'feedingReport'){
            require 'feedingReport.php';
        }else if($view == 'standard'){
            require 'standard.php';
        }else if($view == 'viewFeeding'){
            require 'viewFeeding.php';
        }else if($view == 'reportFeeding'){
            require 'reportFeeding.php';
        }else if($view == 'skip'){
            require 'skip.php';
        }else if($view == 'event'){
            require 'event.php';
        }else if($view == 'eventList'){
            require 'eventList.php';
        }

        ?>
		</div>
		<!--end::Container-->
	</div>
<!--end::Entry-->
				</div>
				<!--end::Content-->

									<!--begin::Footer-->
<div class="footer bg-white py-4 d-flex flex-lg-column " id="kt_footer">
	<!--begin::Container-->
	<div class=" container-fluid  d-flex flex-column flex-md-row align-items-center justify-content-between">
		<!--begin::Copyright-->
		<div class="text-dark order-2 order-md-1">
			<span class="text-muted font-weight-bold mr-2">2020&copy;</span>
			<a href="#" class="text-dark-75 text-hover-primary">CHMSC.tech</a>
		</div>
		<!--end::Copyright-->


	</div>
	<!--end::Container-->
</div>
<!--end::Footer-->
							</div>
			<!--end::Wrapper-->
		</div>
		<!--end::Page-->
	</div>
<!--end::Main-->





                    		<!-- begin::User Panel-->
<div id="kt_quick_user" class="offcanvas offcanvas-right p-10">
	<!--begin::Header-->
	<div class="offcanvas-header d-flex align-items-center justify-content-between pb-5">
		<h3 class="font-weight-bold m-0">
			User Profile
		</h3>
		<a href="#" class="btn btn-xs btn-icon btn-light btn-hover-primary" id="kt_quick_user_close">
			<i class="ki ki-close icon-xs text-muted"></i>
		</a>
	</div>
	<!--end::Header-->

	<!--begin::Content-->
    <div class="offcanvas-content pr-5 mr-n5">
		<!--begin::Header-->
        <div class="d-flex align-items-center mt-5">
            <div class="symbol symbol-100 mr-5">
                <div class="symbol-label" style="background-image:url('assets/media/svg/avatars/007-boy-2.svg')"></div>
				<i class="symbol-badge bg-success"></i>
            </div>
            <div class="d-flex flex-column">
                <a href="#" class="font-weight-bold font-size-h5 text-dark-75 text-hover-primary">
					<?php echo $name;?>
				</a>
                <div class="text-muted mt-1">
				<?php echo $status_name;?>
                </div>
                <div class="navi mt-2">
                    <a href="#" class="navi-item">
                        <span class="navi-link p-0 pb-2">
                            <span class="navi-icon mr-1">
								<span class="svg-icon svg-icon-lg svg-icon-primary"><!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Mail-notification.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <rect x="0" y="0" width="24" height="24"/>
        <path d="M21,12.0829584 C20.6747915,12.0283988 20.3407122,12 20,12 C16.6862915,12 14,14.6862915 14,18 C14,18.3407122 14.0283988,18.6747915 14.0829584,19 L5,19 C3.8954305,19 3,18.1045695 3,17 L3,8 C3,6.8954305 3.8954305,6 5,6 L19,6 C20.1045695,6 21,6.8954305 21,8 L21,12.0829584 Z M18.1444251,7.83964668 L12,11.1481833 L5.85557487,7.83964668 C5.4908718,7.6432681 5.03602525,7.77972206 4.83964668,8.14442513 C4.6432681,8.5091282 4.77972206,8.96397475 5.14442513,9.16035332 L11.6444251,12.6603533 C11.8664074,12.7798822 12.1335926,12.7798822 12.3555749,12.6603533 L18.8555749,9.16035332 C19.2202779,8.96397475 19.3567319,8.5091282 19.1603533,8.14442513 C18.9639747,7.77972206 18.5091282,7.6432681 18.1444251,7.83964668 Z" fill="#000000"/>
        <circle fill="#000000" opacity="0.3" cx="19.5" cy="17.5" r="2.5"/>
    </g>
</svg><!--end::Svg Icon--></span>							</span>
                            <span class="navi-text text-muted text-hover-primary"><?php echo $profile['email'];?></span>
                        </span>
                    </a>

					<a href="ajax/logout.php" class="btn btn-sm btn-light-primary font-weight-bolder py-2 px-5">Sign Out</a>
                </div>
            </div>
        </div>
		<!--end::Header-->

		<!--begin::Separator-->
		<div class="separator separator-dashed mt-8 mb-5"></div>
		<!--end::Separator-->

		<!--begin::Nav-->
		<div class="navi navi-spacer-x-0 p-0">
		    <!--begin::Item-->
		    <a href="home.php?view=profile" class="navi-item">
		        <div class="navi-link">
		            <div class="symbol symbol-40 bg-light mr-3">
		                <div class="symbol-label">
							<span class="svg-icon svg-icon-md svg-icon-success"><!--begin::Svg Icon | path:assets/media/svg/icons/General/Notification2.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <rect x="0" y="0" width="24" height="24"/>
        <path d="M13.2070325,4 C13.0721672,4.47683179 13,4.97998812 13,5.5 C13,8.53756612 15.4624339,11 18.5,11 C19.0200119,11 19.5231682,10.9278328 20,10.7929675 L20,17 C20,18.6568542 18.6568542,20 17,20 L7,20 C5.34314575,20 4,18.6568542 4,17 L4,7 C4,5.34314575 5.34314575,4 7,4 L13.2070325,4 Z" fill="#000000"/>
        <circle fill="#000000" opacity="0.3" cx="18.5" cy="5.5" r="2.5"/>
    </g>
</svg><!--end::Svg Icon--></span>						</div>
		            </div>
		            <div class="navi-text">
		                <div class="font-weight-bold">
		                    My Profile
		                </div>
		                <div class="text-muted">
		                    Account settings and more
		                </div>
		            </div>
		        </div>
		    </a>
		    <!--end:Item-->

		    <!--begin::Item-->
		    <a href="home.php?view=logs"  class="navi-item">
		        <div class="navi-link">
					<div class="symbol symbol-40 bg-light mr-3">
						<div class="symbol-label">
							<span class="svg-icon svg-icon-md svg-icon-danger"><!--begin::Svg Icon | path:assets/media/svg/icons/Files/Selected-file.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <polygon points="0 0 24 0 24 24 0 24"/>
        <path d="M4.85714286,1 L11.7364114,1 C12.0910962,1 12.4343066,1.12568431 12.7051108,1.35473959 L17.4686994,5.3839416 C17.8056532,5.66894833 18,6.08787823 18,6.52920201 L18,19.0833333 C18,20.8738751 17.9795521,21 16.1428571,21 L4.85714286,21 C3.02044787,21 3,20.8738751 3,19.0833333 L3,2.91666667 C3,1.12612489 3.02044787,1 4.85714286,1 Z M8,12 C7.44771525,12 7,12.4477153 7,13 C7,13.5522847 7.44771525,14 8,14 L15,14 C15.5522847,14 16,13.5522847 16,13 C16,12.4477153 15.5522847,12 15,12 L8,12 Z M8,16 C7.44771525,16 7,16.4477153 7,17 C7,17.5522847 7.44771525,18 8,18 L11,18 C11.5522847,18 12,17.5522847 12,17 C12,16.4477153 11.5522847,16 11,16 L8,16 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
        <path d="M6.85714286,3 L14.7364114,3 C15.0910962,3 15.4343066,3.12568431 15.7051108,3.35473959 L20.4686994,7.3839416 C20.8056532,7.66894833 21,8.08787823 21,8.52920201 L21,21.0833333 C21,22.8738751 20.9795521,23 19.1428571,23 L6.85714286,23 C5.02044787,23 5,22.8738751 5,21.0833333 L5,4.91666667 C5,3.12612489 5.02044787,3 6.85714286,3 Z M8,12 C7.44771525,12 7,12.4477153 7,13 C7,13.5522847 7.44771525,14 8,14 L15,14 C15.5522847,14 16,13.5522847 16,13 C16,12.4477153 15.5522847,12 15,12 L8,12 Z M8,16 C7.44771525,16 7,16.4477153 7,17 C7,17.5522847 7.44771525,18 8,18 L11,18 C11.5522847,18 12,17.5522847 12,17 C12,16.4477153 11.5522847,16 11,16 L8,16 Z" fill="#000000" fill-rule="nonzero"/>
    </g>
</svg><!--end::Svg Icon--></span>						</div>
				   	</div>
		            <div class="navi-text">
		                <div class="font-weight-bold">
		                    My Activities
		                </div>
		                <div class="text-muted">
		                    Logs and notifications
		                </div>
		            </div>
		        </div>
		    </a>
		    <!--end:Item-->

		    <!--begin::Item-->
		    <?php if($profile['status'] == 'V'){?>
		    <a href="custom/pages/error/" class="navi-item">
		        <div class="navi-link">
					<div class="symbol symbol-40 bg-light mr-3">
						<div class="symbol-label">
							<span class="svg-icon svg-icon-md svg-icon-primary"><!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Mail-opened.svg-->
								<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
								    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
								        <rect x="0" y="0" width="24" height="24"/>
								        <path d="M6,2 L18,2 C18.5522847,2 19,2.44771525 19,3 L19,12 C19,12.5522847 18.5522847,13 18,13 L6,13 C5.44771525,13 5,12.5522847 5,12 L5,3 C5,2.44771525 5.44771525,2 6,2 Z M7.5,5 C7.22385763,5 7,5.22385763 7,5.5 C7,5.77614237 7.22385763,6 7.5,6 L13.5,6 C13.7761424,6 14,5.77614237 14,5.5 C14,5.22385763 13.7761424,5 13.5,5 L7.5,5 Z M7.5,7 C7.22385763,7 7,7.22385763 7,7.5 C7,7.77614237 7.22385763,8 7.5,8 L10.5,8 C10.7761424,8 11,7.77614237 11,7.5 C11,7.22385763 10.7761424,7 10.5,7 L7.5,7 Z" fill="#000000" opacity="0.3"/>
								        <path d="M3.79274528,6.57253826 L12,12.5 L20.2072547,6.57253826 C20.4311176,6.4108595 20.7436609,6.46126971 20.9053396,6.68513259 C20.9668779,6.77033951 21,6.87277228 21,6.97787787 L21,17 C21,18.1045695 20.1045695,19 19,19 L5,19 C3.8954305,19 3,18.1045695 3,17 L3,6.97787787 C3,6.70173549 3.22385763,6.47787787 3.5,6.47787787 C3.60510559,6.47787787 3.70753836,6.51099993 3.79274528,6.57253826 Z" fill="#000000"/>
								    </g>
								</svg><!--end::Svg Icon-->
							</span>
						</div>
				   	</div>
		            <div class="navi-text">
		                <div class="font-weight-bold">
		                    My Tasks
		                </div>
		                <div class="text-muted">
		                    latest tasks and projects
		                </div>
		            </div>
		        </div>
		    </a>
		    <?php }?>
		    <!--end:Item-->
		</div>
		<!--end::Nav-->
	
    </div>
	<!--end::Content-->
</div>
<!-- end::User Panel-->


                            <!--begin::Scrolltop-->
<div id="kt_scrolltop" class="scrolltop">
	<span class="svg-icon"><!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Up-2.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <polygon points="0 0 24 0 24 24 0 24"/>
        <rect fill="#000000" opacity="0.3" x="11" y="10" width="2" height="10" rx="1"/>
        <path d="M6.70710678,12.7071068 C6.31658249,13.0976311 5.68341751,13.0976311 5.29289322,12.7071068 C4.90236893,12.3165825 4.90236893,11.6834175 5.29289322,11.2928932 L11.2928932,5.29289322 C11.6714722,4.91431428 12.2810586,4.90106866 12.6757246,5.26284586 L18.6757246,10.7628459 C19.0828436,11.1360383 19.1103465,11.7686056 18.7371541,12.1757246 C18.3639617,12.5828436 17.7313944,12.6103465 17.3242754,12.2371541 L12.0300757,7.38413782 L6.70710678,12.7071068 Z" fill="#000000" fill-rule="nonzero"/>
    </g>
</svg><!--end::Svg Icon--></span></div>
<!--end::Scrolltop-->

        <script>var HOST_URL = "https://preview.keenthemes.com/metronic/theme/html/tools/preview";</script>
        <!--begin::Global Config(global config for global JS scripts)-->
        <script>
            var KTAppSettings = {
    "breakpoints": {
        "sm": 576,
        "md": 768,
        "lg": 992,
        "xl": 1200,
        "xxl": 1400
    },
    "colors": {
        "theme": {
            "base": {
                "white": "#ffffff",
                "primary": "#3699FF",
                "secondary": "#E5EAEE",
                "success": "#1BC5BD",
                "info": "#8950FC",
                "warning": "#FFA800",
                "danger": "#F64E60",
                "light": "#E4E6EF",
                "dark": "#181C32"
            },
            "light": {
                "white": "#ffffff",
                "primary": "#E1F0FF",
                "secondary": "#EBEDF3",
                "success": "#C9F7F5",
                "info": "#EEE5FF",
                "warning": "#FFF4DE",
                "danger": "#FFE2E5",
                "light": "#F3F6F9",
                "dark": "#D6D6E0"
            },
            "inverse": {
                "white": "#ffffff",
                "primary": "#ffffff",
                "secondary": "#3F4254",
                "success": "#ffffff",
                "info": "#ffffff",
                "warning": "#ffffff",
                "danger": "#ffffff",
                "light": "#464E5F",
                "dark": "#ffffff"
            }
        },
        "gray": {
            "gray-100": "#F3F6F9",
            "gray-200": "#EBEDF3",
            "gray-300": "#E4E6EF",
            "gray-400": "#D1D3E0",
            "gray-500": "#B5B5C3",
            "gray-600": "#7E8299",
            "gray-700": "#5E6278",
            "gray-800": "#3F4254",
            "gray-900": "#181C32"
        }
    },
    "font-family": "Poppins"
};
        </script>
        <!--end::Global Config-->

 
                    <!--begin::Page Scripts(used by this page)-->
                    <script src="assets/plugins/custom/datatables/datatables.bundle.js"></script>
     <script src="assets/js/pages/widgets.js"></script>
                        <!--end::Page Scripts-->
            </body>
    <!--end::Body-->
</html>