<?php 
$id = $_GET['id'];
    $el = mysql_fetch_array(mysql_query("SELECT * FROM tbl_eligibility where eligibility_id='$id'"));
?>

<div class="subheader py-2 py-lg-6  subheader-solid " id="kt_subheader">
    <div class=" container-fluid  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
        <div class="d-flex align-items-center flex-wrap mr-1">
			<div class="d-flex align-items-baseline flex-wrap mr-5">
	            <h5 class="text-dark font-weight-bold my-1 mr-5">
					Eligibility
				</h5>

	             <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
	                <li class="breadcrumb-item">
	                    <a href="#"class="text-muted">Print</a>
					</li>
	            </ul>
	        </div>

        </div>
                <button type="button" class="btn btn-primary font-weight-bold pull-right" onclick="printDiv();">Print Form</button>

    </div>
</div>
<!--end::Subheader-->


<!--begin::Entry-->
    <div class="d-flex flex-column-fluid" style="background: white;">
        <!--begin::Container-->
        <div class=" container " style="background: white;">
            <!-- begin::Card-->
<div class="card card-custom overflow-hidden" id="report_data">
    <div class="card-body p-0">
        <!-- begin: Invoice-->
        <!-- begin: Invoice header-->
        <div class="row justify-content-center bgi-size-cover bgi-no-repeat py-8 px-8 py-md-27 px-md-0" style="background-image: url(assets/media/bg/bg-6.jpg);">
            <div class="col-md-10">
                <div class="d-flex justify-content-between pb-10 pb-md-20 flex-column flex-md-row">
                     <div class="col-md-4 ">
                       <img src="assets/media/logo.jpg" style="width: 100%;">
                    </div>
                    <h1 class="display-4 text-white font-weight-boldest mb-10">CERTIFICATE OF ELIGIBILITY</h1>
                </div>
                <div class="border-bottom w-100 opacity-20"></div>
                <div class="d-flex justify-content-between text-white pt-6">
                    <div class="d-flex flex-column flex-root">
                        <span class="font-weight-bolde mb-2r">Source of funding</span>
                        <span class="opacity-70">Municipal funds</span>
                    </div>
                    <div class="d-flex flex-column flex-root">
                        <span class="font-weight-bolder mb-2">Clientele Category</span>
                        <span class="opacity-70">Family Head/ Needy Adult<br/>Out of School youth<br/>Disabled & Spi. Group<br/>Distressed</span>
                    </div>
                </div>
            </div>
        </div>
        <!-- end: Invoice header-->

  

        <!-- begin: Invoice footer-->
        <div class="row justify-content-center py-8 px-8 py-md-10 px-md-0" style="background: white;">
            <div class="col-md-10">
                <div class="d-flex justify-content-between flex-column flex-md-row font-size-lg">
                    <div class="d-flex flex-column mb-10 mb-md-0">

                        <div class="d-flex justify-content-between mb-3">
                            <p>This is to certify that  <span class="font-weight-bold" style="text-decoration: underline;"> <?php echo ucwords($el['name']);?> </span>  age <span class="font-weight-bold" style="text-decoration: underline;"> <?php echo $el['age'];?> </span> residing at Barangay <span class="font-weight-bold" style="text-decoration: underline;"> <?php echo ucwords($el['address']);?> </span> with the following member/s</p>
                        </div>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th class="pl-0 font-weight-bold text-uppercase">Name</th>
                                        <th class="text-right font-weight-bold text-uppercase">Age</th>
                                        <th class="text-right font-weight-bold text-uppercase">Relationship</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="font-weight-boldest font-size-lg">
                                        <td class="pl-0 pt-7"><span class="font-weight-bold"> <?php echo ucwords($el['patient_name']);?> </span></td>
                                        <td class="text-right pt-7"><span class="font-weight-bold" > <?php echo $el['patient_age'];?> </span></td>
                                        <td class="text-right pt-7"><span class="font-weight-bold"> <?php echo ucwords($el['patient_rel']);?> </span></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="d-flex justify-content-between mb-3">
                            <p>Has been found eligible for cash assistance from Emergency Assistance in the amount of <span class="font-weight-bold">&#8369; <?php echo number_format($el['amount'],2);?> </span> to invest  for <span class="font-weight-bold"> <?php echo $el['inv'];?> </span> to which <span class="font-weight-bold"><?php echo $el['pharmacy_number'];?></span> with the recipt no. <span class="font-weight-bold"><?php echo $el['rcpt_no'];?>  </span> worth of external/community /client counterpart has obtained records of the cases / Sea Plan date <span class="font-weight-bold"> <?php echo $date;?> </span> are in the file of MSWD, Murcia unit office.</p>
                            
                        </div>
                          <br><br>
                      

                         <div class="row justify-content-center bgi-size-cover bgi-no-repeat py-8 px-8 py-md-27 px-md-0">
                            <div class="col-md-12">
                                <div class="border-bottom w-100 opacity-20"></div>
                                <div class="d-flex justify-content-between pt-6">
                                    <div class="d-flex flex-column flex-root">
                                         <hr style="border-top: 1px solid black;margin: 0px;    margin-right: 30px;">
                                         <span class="font-weight-bolder mb-2">   Signature or Thumb of client</span>
                                    </div>
                                    <div class="d-flex flex-column flex-root">
                                         <hr style="border-top: 1px solid black;margin: 0px;">
                                <span class="font-weight-bolder mb-2"> DSWD Worker</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <br><br>
                        <br><br>

                        <span class="font-weight-bolder mb-2">Recommending Approval</span>
                        <br><br>
                        <br><br>

                          <div class="row justify-content-center bgi-size-cover bgi-no-repeat py-8 px-8 py-md-27 px-md-0">
                            <div class="col-md-12">
                                <div class="border-bottom w-100 opacity-20"></div>
                                <div class="d-flex justify-content-between pt-6">
                                    <div class="d-flex flex-column flex-root">
                                         <hr style="border-top: 1px solid black;margin: 0px;    margin-right: 30px;">
                                         <span class="font-weight-bolder mb-2"> ANA L. NARANJA </span>MSWDO<br>Murcia, Neg. Occ
                                    </div>
                                    <div class="d-flex flex-column flex-root">
                                         <hr style="border-top: 1px solid black;margin: 0px;">
                                <span class="font-weight-bolder mb-2"> HON. VICTOR GERARDO M/ ROJAS</span>Municipal Mayor<br>Murcia, Neg. Occ
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end: Invoice footer-->
        <!-- end: Invoice-->
    </div>
</div>
<!-- end::Card-->
        </div>
        <!--end::Container-->
    </div>
<!--end::Entry-->

<script type="text/javascript">
    function printDiv() 
{

  var divToPrint=document.getElementById('report_data');

  var newWin=window.open('','Print-Window');

  newWin.document.open();

  newWin.document.write('<html><link href="assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css"/><link href="assets/plugins/custom/prismjs/prismjs.bundle.css" rel="stylesheet" type="text/css"/><link href="assets/css/style.bundle.css" rel="stylesheet" type="text/css"/><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

  newWin.document.close();

  setTimeout(function(){newWin.close();},10);

}
</script>