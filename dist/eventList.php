<div class="subheader py-2 py-lg-6  subheader-solid " id="kt_subheader">
    <div class=" container-fluid  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
        <div class="d-flex align-items-center flex-wrap mr-1">
			<div class="d-flex align-items-baseline flex-wrap mr-5">
	            <h5 class="text-dark font-weight-bold my-1 mr-5">
					Dashboard
				</h5>
	        </div>
        </div>
    </div>
</div>
<!--end::Subheader-->

    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class=" container ">
            <!-- begin: Invoice-->
             <div class="row  py-8 px-8  px-md-0">
                <?php

            	$fetch_event = mysql_query("SELECT * FROM `tbl_event` where status = 0")or die(mysql_error());
            	while($row_event = mysql_fetch_array($fetch_event)){?>
            	  <div class="col-md-6">
                <!--begin::Card-->
                <div class="card card-custom gutter-b">
                    <div class="card-header">
                        <div class="card-title">
                            <h3 class="card-label">
                                <span class="fas fa-bullhorn"></span> Event Announcement 
                            </h3>
                        </div>
                    </div>
                    <div class="card-body">
                    	<div class="wizard-label">
                                <h3 class="wizard-title">
                                     <span class="fas fa-redo"></span>  Event Name <span style="float: right;"> <?php echo $row_event['event_name'];?></span>
                                </h3>
                        </div>
                        <div class="wizard-label">
                                <h3 class="wizard-title">
                                    <span class="fas fa-calendar-day"></span> When <span style="float: right;"> <?php echo date('M d,Y h:i a',strtotime($row_event['event_when']));?></span>
                                </h3>
                        </div>
                        
                        <div class="wizard-label">
                                <h3 class="wizard-title">
                                     <span class="fas fa-user-tie"></span>  Where <span style="float: right;"> <?php echo $row_event['event_where'];?></span>
                                </h3>
                        </div>
                         <div class="wizard-label">
                                <h3 class="wizard-title">
                                     <span class="fas fa-user-tie"></span>  Description <span style="float: right;"> <?php echo $row_event['event_description'];?></span>
                                </h3>
                        </div>

                    </div>
                </div>
                <!--end::Card-->
                                    
                </div>
            <?php }?>
             </div>
            <!-- end: Invoice-->
        </div>
        <!--end::Container-->
    </div>
<!--end::Entry-->
<script>
    $(document).ready(function(){
        getChart();
        getChart1();

    });

    function getChart() {
    	 
		const apexChart = "#chart_3";
		var options = {
			  chart: {
			      height: 350,
			      type: 'bar',
			  },
			  plotOptions:
			   {
				bar: {
					horizontal: false,
					columnWidth: '55%',
					endingShape: 'rounded'
				},
			},
			  series: [],
			  dataLabels: {
				enabled: false
			},
			stroke: {
				show: true,
				width: 2,
				colors: ['transparent']
			},
			xaxis: {
				categories: ['Jan','Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct','Nov','Dec'],
			},
			yaxis: {
				title: {
					text: 'Php (Amount)'
				}
			},
			fill: {
				opacity: 1
			},
			tooltip: {
				y: {
					formatter: function (val) {
						return "Php " + val 
					}
				}
			},
  noData: {
    text: 'Loading...'
  }
}

var chart = new ApexCharts(
  document.querySelector(apexChart),
  options
);

chart.render();

var url ="ajax/getChart.php"
$.getJSON(url, function(response) {
  chart.updateSeries([{
    name: 'Amount',
    data: response
  }])
});

	}

	    function getChart1() {
    	 
		const apexChart = "#chart_1";
		var options = {
			  chart: {
			      height: 350,
			      type: 'bar',
			  },
			  plotOptions:
			   {
				bar: {
					horizontal: false,
					columnWidth: '55%',
					endingShape: 'rounded'
				},
			},
			  series: [],
			  dataLabels: {
				enabled: false
			},
			stroke: {
				show: true,
				width: 2,
				colors: ['transparent']
			},
			xaxis: {
				categories: ['Jan','Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct','Nov','Dec'],
			},
			yaxis: {
				title: {
					text: 'Php (Amount)'
				}
			},
			fill: {
				opacity: 1
			},
			tooltip: {
				y: {
					formatter: function (val) {
						return "Php " + val 
					}
				}
			},
  noData: {
    text: 'Loading...'
  }
}

var chart = new ApexCharts(
  document.querySelector(apexChart),
  options
);

chart.render();

var url ="ajax/getChart1.php"
$.getJSON(url, function(response) {
  chart.updateSeries([{
    name: 'Amount',
    data: response
  }])
});
}


</script>