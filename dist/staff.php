<div class="subheader py-2 py-lg-6  subheader-solid " id="kt_subheader">
    <div class=" container-fluid  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
        <div class="d-flex align-items-center flex-wrap mr-1">
			<div class="d-flex align-items-baseline flex-wrap mr-5">
	            <h5 class="text-dark font-weight-bold my-1 mr-5">
					Staff
				</h5>

	             <!-- <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
	                <li class="breadcrumb-item">
	                    <a href=""class="text-muted">General</a>
					</li>
	                 <li class="breadcrumb-item">
	                    <a href="#" class="text-muted"> dasdsa Page</a>
					</li>
	            </ul> -->
	        </div>
        </div>
    </div>
</div>
<!--end::Subheader-->

<!--begin::Card-->
<div class="card card-custom">
	<div class="card-header">
		<div class="card-title">
			<span class="card-icon"><i class="flaticon-notepad text-primary"></i></span>
			<h3 class="card-label">List of Staff</h3>
		</div>
		<div class="card-toolbar">
<!--begin::Button-->
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#staticBackdrop"><span class="fas fa-plus-circle"></span>
	New Record
</button>
<!--end::Button-->
		</div>
	</div>
    <?php require('modal/modal_addStaff.php');
    require('modal/modal_editStaff.php');
    require('modal/modal_viewStaff.php');?>
	<div class="card-body">
		<!--begin: Datatable-->
		<table class="table table-bordered table-hover table-checkable" id="kt_datatable" style="margin-top: 13px !important">
			<thead>
				<tr>
					<th></th>
					<th>Name</th>
					<th>Address</th>
					<th>Phone</th>
					<th>Email</th>
					<th>Status</th>
					<!-- <?php echo 
					($status == 'A')?'<th>Status</th>':'';
					?> -->
					<th>Actions</th>
				</tr>
			</thead>


        </table>
		<!--end: Datatable-->
	</div>
</div>
<!--end::Card-->

<script>
function getData(){
      var table = $('#kt_datatable').DataTable();
      table.destroy();
      var status = 'datatable';

      $("#kt_datatable").dataTable({
        "processing":true,
        "ajax":{
          "type":"POST",
          "url":"ajax/staffCrud.php",
          "dataSrc":"data",
          "data":{
            status:status
          }
        },
        "columns":[
          {
            "data":"count"
          },
          {
            "data":"name"
          },
          {
            "data":"address"
          },
          {
            "data":"contact_number"
          },
          {
            "data":"email"
          }, 
           {
            "data":"status"
          },       
          {
            "mRender": function(data,type,row){
            	if(row.archive_status == '0'){
            		var btn_archive = '<a href="javascript:;" class="btn btn-sm btn-clean btn-icon" title="Archive"><i class="la la-box-open" onclick="archive('+row.id+')"></i></a>';
            	}else{
            		var btn_archive = '';
            	}
				return '<div class="d-flex align-items-center">'
				+'<div class="dropdown dropdown-inline mr-1"><div class="dropdown-menu dropdown-menu-sm 	dropdown-menu-right"></div></div>'
				+'<a href="javascript:;" class="btn btn-sm btn-clean btn-icon mr-1" title="Edit details" onclick="edit('+row.id+')"><i class="la la-edit"></i></a>'
				+'<a href="javascript:;" class="btn btn-sm btn-clean btn-icon" title="View Details"><i class="la la-eye" onclick="view('+row.id+')"></i></a>'
				+btn_archive+'</div>';
            }
          }
        ]
      });
    }


	$("#kt_form_1").submit(function(e){
	  e.preventDefault();
      $.ajax({
        url:"ajax/staffCrud.php",
        method:"POST",
        data:$("#kt_form_1").serialize(),
        success: function(data){
           if(data == 1){
				alertMe("All is cool! Successfully added staff","success");
				$("#staticBackdrop").modal("hide");
				getData();
		   }else if(data == 2){
				alertMe("Sorry, username or name is already existing, please try again.","error");
		   }else{
				alertMe("Sorry, looks like there are some errors detected, please try again.","error");
		   }
          
        }
      });
	});

	$("#kt_form_edit").submit(function(e){
	  e.preventDefault();
      $.ajax({
        url:"ajax/staffCrud.php",
        method:"POST",
        data:$("#kt_form_edit").serialize(),
        success: function(data){
           if(data == 1){
				alertMe("All is cool! Successfully updated staff","success");
				$("#editModal").modal("hide");
				getData();
		   }else if(data == 2){
				alertMe("Sorry, username or name is already existing, please try again.","error");
		   }else{
				alertMe("Sorry, looks like there are some errors detected, please try again.","error");
		   }
          
        }
      });
	});


	function edit(id){
		$("#editModal").modal("show");
			$("#exampleModalLabel").html("Edit Staff");
		$("#id").val(id);
		$.ajax({
			url:"ajax/staffCrud.php",
			type:"POST",
			data:{
				id:id,
				status:'View'
			},success:function(data){
				var o = JSON.parse(data);
				$("#fname").val(o.fname);
				$("#lname").val(o.lname);
				$("#contact_number").val(o.contact_number);
				$("#bday").val(o.bday);
				$("#address").val(o.address);
				$("#email").val(o.email);
				$("#un").val(o.un);
			}
		});
	}

		function archive(id){
		$.ajax({
			url:"ajax/archiveStatus.php",
			type:"POST",
			data:{
				id:id
			},success:function(data){
				if(data == 1){
					alertMe("All is cool! Successfully archived staff","success");
					getData();
			   }else{
					alertMe("Sorry, looks like there are some errors detected, please try again.","error");
			   }
				
			}
		});
	}
	
	function view(id){
		$("#viewModal").modal("show");
		$(".view_header").html("Staff Profile");
		$.ajax({
			url:"ajax/staffCrud.php",
			type:"POST",
			data:{
				id:id,
				status:'View'
			},success:function(data){
				var o = JSON.parse(data);
				$("#viewname").text(o.fname+" "+o.lname);
				$("#viewstatus").text(o.status);
				$("#viewphone").text(o.contact_number);

				$("#viewbday").text(o.bday);
				$("#viewaddress").text(o.address);
				$("#viewemail").text(o.email);
			}
		});
	}

jQuery(document).ready(function() {
	getData();

	$("#exampleModalLabel").html("Add Staff");
});
</script>