<div class="subheader py-2 py-lg-6  subheader-solid " id="kt_subheader">
    <div class=" container-fluid  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
        <div class="d-flex align-items-center flex-wrap mr-1">
			<div class="d-flex align-items-baseline flex-wrap mr-5">
	            <h5 class="text-dark font-weight-bold my-1 mr-5">
					Volunteers
				</h5>

	             <!-- <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
	                <li class="breadcrumb-item">
	                    <a href=""class="text-muted">General</a>
					</li>
	                 <li class="breadcrumb-item">
	                    <a href="#" class="text-muted"> dasdsa Page</a>
					</li>
	            </ul> -->
	        </div>
        </div>
    </div>
</div>
<!--end::Subheader-->

<!--begin::Card-->
<div class="card card-custom">
	<div class="card-header">
		<div class="card-title">
			<span class="card-icon"><i class="flaticon-notepad text-primary"></i></span>
			<h3 class="card-label">List of volunteer</h3>
		</div>
		<div class="card-toolbar">
<!--begin::Button-->
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#staticBackdrop"><span class="fas fa-plus-circle"></span>
	New Record
</button>
<!--end::Button-->
		</div>
	</div>
	<!-- Modal Add-->
<div class="modal fade" id="staticBackdrop" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Volunteer</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
			<form class="form" id="kt_form_1">
                    <div class="form-group row">
						<label class="col-form-label text-right col-lg-3 col-sm-12">Firstname *</label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<input required type="text" class="form-control" name="fname" placeholder="Enter your Firstname"/>
						</div>
                    </div>

                    <div class="form-group row">
						<label class="col-form-label text-right col-lg-3 col-sm-12">Lastname *</label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<input required type="text" class="form-control" name="lname" placeholder="Enter your Lastname"/>
						</div>
                    </div>

                    <div class="form-group row">
						<label class="col-form-label text-right col-lg-3 col-sm-12">Address *</label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<input required type="text" class="form-control" name="address" placeholder="Enter your Address"/>
						</div>
                    </div>

                    <div class="form-group row">
						<label class="col-form-label text-right col-lg-3 col-sm-12">Birthdate *</label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<input required type="date" class="form-control" name="bday" placeholder="Enter your Address"/>
						</div>
                    </div>

                    
					<div class="form-group row">
						<label class="col-form-label text-right col-lg-3 col-sm-12">Email *</label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<input required type="email" class="form-control" name="email" placeholder="Enter your email"/>
						</div>
                    </div>

					<div class="form-group row">
						<label class="col-form-label text-right col-lg-3 col-sm-12">Phone * </label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<div class="input-group">
								<input required type="number" class="form-control" name="contact_number" placeholder="Enter phone"/>
							</div>
						</div>
                    </div>
                    
                    <div class="form-group row">
						<label class="col-form-label text-right col-lg-3 col-sm-12">Username *</label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<input required type="text" class="form-control" name="un" placeholder="Enter your Address"/>
						</div>
					</div>
					
					<input type="hidden" class="form-control" name="status" value="Create"/>

		
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary font-weight-bold">Save changes</button>
			</div>
			</form>
        </div>
    </div>
</div>
<!--end: Modal Add-->

	<!-- Modal View-->
	<div class="modal fade" id="editModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Volunteer</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
			<form class="form" id="kt_form_edit">
                    <div class="form-group row">
						<label class="col-form-label text-right col-lg-3 col-sm-12">Firstname *</label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<input required type="text" class="form-control" name="fname" placeholder="Enter your Firstname" id="fname" />
						</div>
                    </div>

                    <div class="form-group row">
						<label class="col-form-label text-right col-lg-3 col-sm-12">Lastname *</label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<input required type="text" class="form-control" name="lname" placeholder="Enter your Lastname" id="lname" />
						</div>
                    </div>

                    <div class="form-group row">
						<label class="col-form-label text-right col-lg-3 col-sm-12">Address *</label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<input required type="text" class="form-control" name="address" placeholder="Enter your Address" id="address" />
						</div>
                    </div>

                    <div class="form-group row">
						<label class="col-form-label text-right col-lg-3 col-sm-12">Birthdate *</label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<input required type="date" class="form-control" name="bday" placeholder="Enter your Address" id="bday" />
						</div>
                    </div>

                    
					<div class="form-group row">
						<label class="col-form-label text-right col-lg-3 col-sm-12">Email *</label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<input required type="email" class="form-control" name="email" placeholder="Enter your email" id="email" />
						</div>
                    </div>

					<div class="form-group row">
						<label class="col-form-label text-right col-lg-3 col-sm-12">Phone * </label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<div class="input-group">
								<input required type="number" class="form-control" name="contact_number" placeholder="Enter phone" id="contact_number" />
							</div>
						</div>
                    </div>
                    
                    <div class="form-group row">
						<label class="col-form-label text-right col-lg-3 col-sm-12">Username *</label>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<input required type="text" class="form-control" name="un" placeholder="Enter your Address" id="un" />
						</div>
					</div>
					
					<input type="hidden" class="form-control" name="status" value="Update"/>
					<input type="hidden" class="form-control" name="id" id='id'/>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary font-weight-bold">Save changes</button>
			</div>
			</form>
        </div>
    </div>
</div>
<!--end: Modal Edit-->

	<!-- Modal View-->
	<div class="modal fade" id="viewModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Volunteer Profile</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
			   <!--begin::User-->
			   <div class="d-flex align-items-center">
                    <div class="symbol symbol-60 symbol-xxl-100 mr-5 align-self-start align-self-xxl-center">
                        <div class="symbol-label" style="background-image:url('assets/media/users/300_13.jpg')"></div>
                    </div>
                    <div>
                        <a href="#" class="font-weight-bold font-size-h5 text-dark-75 text-hover-primary" id="viewname">
                           
                        </a>
                        <div class="text-muted" id="viewstatus">
                          
                        </div>
                    
                    </div>
                </div>
                <!--end::User-->

                <!--begin::Contact-->
                <div class="pt-8 pb-6">
                    <div class="d-flex align-items-center justify-content-between mb-2">
                        <span class="font-weight-bold mr-2">Email:</span>
                        <a href="#" class="text-muted text-hover-primary" id="viewemail"></a>
                    </div>
                    <div class="d-flex align-items-center justify-content-between mb-2">
                        <span class="font-weight-bold mr-2">Phone:</span>
                        <span class="text-muted" id="viewphone"></span>
                    </div>
                    <div class="d-flex align-items-center justify-content-between">
                        <span class="font-weight-bold mr-2">Birthday:</span>
                        <span class="text-muted" id="viewbday"></span>
                    </div>
                </div>
                <!--end::Contact-->

                <!--begin::Contact-->
                <div class="pb-6">
				<span class="font-weight-bold mr-2">Location:</span>
				<span class="text-muted" id="viewaddress"></span>
                </div>
                <!--end::Contact-->
			</div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
			</div>
        </div>
    </div>
</div>
<!--end: Modal View-->

	<div class="card-body">
		<!--begin: Datatable-->
		<table class="table table-bordered table-hover table-checkable" id="kt_datatable" style="margin-top: 13px !important">
			<thead>
				<tr>
					<th></th>
					<th>Name</th>
					<th>Address</th>
					<th>Phone</th>
					<th>Email</th>
					<th>Actions</th>
				</tr>
			</thead>


        </table>
		<!--end: Datatable-->
	</div>
</div>
<!--end::Card-->

<script>
function getData(){
      var table = $('#kt_datatable').DataTable();
      table.destroy();
      var value = 'V';
      $("#kt_datatable").dataTable({
        "processing":true,
        "ajax":{
          "type":"POST",
          "url":"ajax/datatables/dt_user.php",
          "dataSrc":"data",
          "data":{
            value:value
          }
        },
        "columns":[
          {
            "data":"count"
          },
          {
            "data":"name"
          },
          {
            "data":"address"
          },
          {
            "data":"contact_number"
          },
          {
            "data":"email"
          },       
          {
            "mRender": function(data,type,row){
				return '<div class="d-flex align-items-center">'
				+'<div class="dropdown dropdown-inline mr-1"><div class="dropdown-menu dropdown-menu-sm 	dropdown-menu-right"></div></div>'
				+'<a href="javascript:;" class="btn btn-sm btn-clean btn-icon mr-1" title="Edit details" onclick="edit('+row.id+')"><i class="la la-edit"></i></a>'
				+'<a href="javascript:;" class="btn btn-sm btn-clean btn-icon" title="Delete"><i class="la la-eye" onclick="view('+row.id+')"></i></div>';
            }
          }
        ]
      });
    }


	$("#kt_form_1").submit(function(e){
	  e.preventDefault();
      $.ajax({
        url:"ajax/volunteerCrud.php",
        method:"POST",
        data:$("#kt_form_1").serialize(),
        success: function(data){
           if(data == 1){
				alertMe("All is cool! Successfully added volunteer","success");
				$("#staticBackdrop").modal("hide");
				getData();
		   }else if(data == 2){
				alertMe("Sorry, username or name is already existing, please try again.","error");
		   }else{
				alertMe("Sorry, looks like there are some errors detected, please try again.","error");
		   }
          
        }
      });
	});

	$("#kt_form_edit").submit(function(e){
	  e.preventDefault();
      $.ajax({
        url:"ajax/volunteerCrud.php",
        method:"POST",
        data:$("#kt_form_edit").serialize(),
        success: function(data){
           if(data == 1){
				alertMe("All is cool! Successfully updated volunteer","success");
				$("#editModal").modal("hide");
				getData();
		   }else if(data == 2){
				alertMe("Sorry, username or name is already existing, please try again.","error");
		   }else{
				alertMe("Sorry, looks like there are some errors detected, please try again.","error");
		   }
          
        }
      });
	});


	function edit(id){
		$("#editModal").modal("show");
		$("#id").val(id);
		$.ajax({
			url:"ajax/volunteerCrud.php",
			type:"POST",
			data:{
				id:id,
				status:'View'
			},success:function(data){
				var o = JSON.parse(data);
				$("#fname").val(o.fname);
				$("#lname").val(o.lname);
				$("#contact_number").val(o.contact_number);
				$("#bday").val(o.bday);
				$("#address").val(o.address);
				$("#email").val(o.email);
				$("#un").val(o.un);
			}
		});
	}
	
	function view(id){
		$("#viewModal").modal("show");
		$.ajax({
			url:"ajax/volunteerCrud.php",
			type:"POST",
			data:{
				id:id,
				status:'View'
			},success:function(data){
				var o = JSON.parse(data);
				$("#viewname").text(o.fname+" "+o.lname);
				$("#viewstatus").text(o.status);
				$("#viewphone").text(o.contact_number);

				$("#viewbday").text(o.bday);
				$("#viewaddress").text(o.address);
				$("#viewemail").text(o.email);
			}
		});
	}

jQuery(document).ready(function() {
	getData();
});
</script>