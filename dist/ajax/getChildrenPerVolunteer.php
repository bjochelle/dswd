
<?php 
    include "../core/config.php";
    $status = $_POST['status'];
    session_start();
    $user_id = $_SESSION['id'];
    date_default_timezone_set('Asia/Manila');
    $date = date("Y-m-d");

    if($status == 'View'){

        if(isset($_POST["cycle_id"]) && isset($_POST["cycle_id"]) != ""){
            $cycle_id =$_POST["cycle_id"];
            $user_id = $_POST["volunteer_id"];

            $query = "SELECT * from tbl_feeding_header where cycle_id='$cycle_id' ";
            $result = mysql_query($query) or die(mysql_error());
            $response = array();
            
            if(mysql_num_rows($result) > 0){
                while ($row = mysql_fetch_assoc($result)) {
                    $response = $row;
                    $count_child = mysql_num_rows(mysql_query("SELECT * from tbl_child where  user_id='$user_id' "));

                    $count_feeding = mysql_num_rows(mysql_query("SELECT * from tbl_feeding_details where cycle_id='$cycle_id' and user_id='$user_id' and standard_id='1'"));
                    $count_feeding1 = mysql_num_rows(mysql_query("SELECT * from tbl_feeding_details where cycle_id='$cycle_id' and user_id='$user_id' and standard_id='2'"));
                    $count_feeding2 = mysql_num_rows(mysql_query("SELECT * from tbl_feeding_details where cycle_id='$cycle_id' and user_id='$user_id' and standard_id='3'"));
                    $count_feeding3 = mysql_num_rows(mysql_query("SELECT * from tbl_feeding_details where cycle_id='$cycle_id' and user_id='$user_id' and standard_id='4'"));

                     $count_all = mysql_num_rows(mysql_query("SELECT * from tbl_feeding_details where cycle_id='$cycle_id' and user_id='$user_id'"));

                    $response['progress_n'] = ($count_feeding/($count_child*3))*100;
                    $response['progress_uw'] = ($count_feeding1/($count_child*3))*100;
                    $response['progress_su'] = ($count_feeding2/($count_child*3))*100;
                    $response['progress_ow'] = ($count_feeding3/($count_child*3))*100;
                    $response['progress_all'] = ($count_all/($count_child*3))*100;


                }
            }else
            {
                $response['status'] = 200;
                $response['message'] = "Data not found!";
            }
            echo json_encode($response);
        }

    }else if($status == 'datatable'){
          $cycle_id = $_POST['cycle_id'];
             $user_id = $_POST['volunteer_id'];


        $f = mysql_query("SELECT * from tbl_feeding_details  where cycle_id='$cycle_id' and user_id='$user_id'") or die(mysql_error());
        $count = 1;
        $response['data'] = array();
            while ($row = mysql_fetch_array($f)) {
            $list = array();
                $list['count'] = $count++;

            $child_info = mysql_fetch_array(mysql_query("SELECT fname,lname from tbl_child where child_id='$row[child_id]'"));


                $list['id'] = $row['feeding_id'];
                $list['child'] = ucwords($child_info[0]." ".$child_info[1]);

                if($row['isfeedingremarks']==1){
                    $value ='Before feeding';
                }else if($row['isfeedingremarks']==2){
                    $value ='After 60 days';
                }else{
                    $value ='After 120 days';
                }


                if($row['standard_id']==1){
                    $status ='Normal';
                }else if($row['standard_id']==2){
                    $status ='Underweight';
                }else if($row['standard_id']==3){
                    $status ='Severely Underweight';
                }else if($row['standard_id']==4){
                    $status ='Over Weight';
                }else{
                    $status ='BMI out of range';
                }

                $list['status'] = $status;
                $list['feed_type'] = $value;
                $list['weight'] = $row['weight'];
                $list['bmi'] = $row['bmi'];
                $list['volunteer'] = getFullName($row['user_id']);


                array_push($response['data'], $list);
            }
            echo json_encode($response);
    }
?>