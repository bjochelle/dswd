<?php 
include "../core/config.php";
    $child_id = $_POST['child_id'];
    $sort_by = $_POST['sort_by'];
    $input_sort = $_POST['input_sort'];

    session_start();
    $id = $_SESSION['id'];

    if($child_id == '-1'){
        $child_query = '';
        $child_class = '';
    }else{
        $child_query = 'child_id='.$child_id.' and ';
        $child_class = 'style="display:none;"';
    }

    if($sort_by == 'D'){
        $add_query =  $child_query."DATE_FORMAT(date_added,'%m/%d/%Y')='".$input_sort."'";
        $subtitle = $input_sort;
    }else if($sort_by == 'M'){
        $add_query = $child_query."DATE_FORMAT(date_added,'%Y-%m')='".date('Y').'-'.date('m',strtotime('1970-'.$input_sort.'-01'))."'";
        $subtitle = date('M',strtotime('1970-'.$input_sort.'-01')).", ".date('Y');
    }else if($sort_by == 'W'){
        $add_query = $child_query."DATE_FORMAT(date_added,'%U')=".$input_sort;
        $subtitle = getOrdinal($input_sort)." week";
    }else if($sort_by == 'Y'){
        $add_query = $child_query."DATE_FORMAT(date_added,'%Y')=".$input_sort;
        $subtitle = $input_sort;
    }

?>
         <!-- begin: Invoice header-->
         <div class="row justify-content-centers">
            <div class="col-md-12 text-center">
                    <k4 class="display-4 font-weight-boldest"> SUPPLEMENTARY REPORT</k4>
                    <br>
                <?php echo $subtitle ;?> 
               
            </div>
        </div>
        <!-- end: Invoice header-->

    <!-- begin: Invoice footer-->
    <div class="row justify-content-center py-8 px-8 px-md-0">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                             <tr>
                                <th class="font-weight-bold text-muted  text-uppercase " <?= $child_class;?>>Student Name</th>
                                <th class="font-weight-bold text-muted  text-uppercase" >BMI</th>
                                <th class="font-weight-bold text-muted  text-uppercase" >Feed Type</th>
                                <th class="font-weight-bold text-muted  text-uppercase" >Status</th>
                                <th class="font-weight-bold text-muted  text-uppercase" >Volunteer Name</th>    
                                <th class="font-weight-bold text-muted  text-uppercase" >Date Added</th>    
                            </tr>

                        </thead>
                        <tbody>
                        <?php $fecth = mysql_query("SELECT * FROM tbl_feeding_details where $add_query");
                        $total=0;
                        while($row=mysql_fetch_array($fecth)){
                            $child_info = mysql_fetch_array(mysql_query("SELECT fname,lname from tbl_child where child_id='$row[child_id]'"));
                           $child = ucwords($child_info[0]." ".$child_info[1]);

                            if($row['isfeedingremarks']==1){
                                $value ='Before feeding';
                            }else if($row['isfeedingremarks']==2){
                                $value ='After 60 days';
                            }else{
                                $value ='After 120 days';
                            }


                            if($row['standard_id']==1){
                                $status ='Normal';
                            }else if($row['standard_id']==2){
                                $status ='Underweight';
                            }else if($row['standard_id']==3){
                                $status ='Severely Underweight';
                            }else if($row['standard_id']==4){
                                $status ='Over Weight';
                            }else{
                                $status ='BMI out of range';
                            }

                            ?>
                            <tr class="font-weight-bolder">
                                <td <?= $child_class;?> ><?php echo $child;?></td>
                                <td class="text-danger font-size-h3 font-weight-boldest"><?php echo $row['bmi'];?></td>
                                <td><?php echo $value;?></td>
                                <td><?php echo $status;?></td>
                                <td><?php echo getFullName($row['user_id']);?></td>

                                <td><?php echo date("M d,Y",strtotime($row['date_added']));?></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- end: Invoice footer-->