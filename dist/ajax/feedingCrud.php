
<?php 
    include "../core/config.php";
    $status = $_POST['status'];
    session_start();
    $user_id = $_SESSION['id'];
    date_default_timezone_set('Asia/Manila');
    $date = date("Y-m-d");

    if($status == 'Create'){

        $cycle_id = $_POST['cycle_id'];
        $isfeedingremarks = $_POST['isfeedingremarks'];
        $child_id = $_POST['child_id'];
        $weight = $_POST['weight'];
        $height = $_POST['height'];

        $bmi = $weight/($height*$height);


        $child_info = mysql_fetch_array(mysql_query("SELECT DATEDIFF('$date',bday),gender,fname,lname from tbl_child where child_id='$child_id'"));

        $age = intval($child_info[0] / 365); //divide by 365 and throw away the remainder
        // $days_remaining = $days % 365;  

        $count = mysql_num_rows(mysql_query("SELECT * from tbl_feeding_details where cycle_id='$cycle_id' and isfeedingremarks='$isfeedingremarks' and child_id='$child_id'"));

        if($count>0){
            echo 2;
        }else{
            $standard_id = mysql_fetch_array(mysql_query("SELECT standard_id FROM `tbl_standard` where '$bmi' BETWEEN start_range AND  end_range"));
            $sql = mysql_query("INSERT INTO `tbl_feeding_details`(`cycle_id`, `child_id`, `weight`,`height`,`bmi`, `date_added`, `age_taken`, `gender`, `isfeedingremarks`,`standard_id`,`user_id`) VALUES ('$cycle_id','$child_id','$weight','$height','$bmi','$date','$age','$child_info[1]','$isfeedingremarks','$standard_id[0]','$user_id')");

            if($sql){
                insertlogs($user_id,"Added new feeding to ".$child_info[2]." ".$child_info[3]);
                echo 1;
            }else{
                echo 0;
            }
        }

     
    }else if($status == 'View'){

        if(isset($_POST["cycle_id"]) && isset($_POST["cycle_id"]) != ""){
            $cycle_id =$_POST["cycle_id"];
            $isfeedingremarks = $_POST["isfeedingremarks"];

            $query = "SELECT * from tbl_feeding_header where cycle_id='$cycle_id' ";
            $result = mysql_query($query) or die(mysql_error());
            $response = array();
            
            if(mysql_num_rows($result) > 0){
                while ($row = mysql_fetch_assoc($result)) {
                    $response = $row;
                    $count_child = mysql_num_rows(mysql_query("SELECT * from tbl_child where  user_id='$user_id' and archive_status=0"));

                    $count_feeding = mysql_num_rows(mysql_query("SELECT * from tbl_feeding_details where cycle_id='$cycle_id' and isfeedingremarks='$isfeedingremarks' and standard_id='1'and user_id='$user_id'"));
                    $count_feeding1 = mysql_num_rows(mysql_query("SELECT * from tbl_feeding_details where cycle_id='$cycle_id' and isfeedingremarks='$isfeedingremarks' and standard_id='2' and user_id='$user_id'"));
                    $count_feeding2 = mysql_num_rows(mysql_query("SELECT * from tbl_feeding_details where cycle_id='$cycle_id' and isfeedingremarks='$isfeedingremarks' and standard_id='3' and user_id='$user_id'"));
                    $count_feeding3 = mysql_num_rows(mysql_query("SELECT * from tbl_feeding_details where cycle_id='$cycle_id' and isfeedingremarks='$isfeedingremarks' and standard_id='4' and user_id='$user_id'"));

                     $count_all = mysql_num_rows(mysql_query("SELECT * from tbl_feeding_details where cycle_id='$cycle_id' and isfeedingremarks='$isfeedingremarks' and user_id='$user_id'"));

                    $response['progress_n'] = ($count_feeding/$count_child)*100;
                    $response['progress_uw'] = ($count_feeding1/$count_child)*100;
                    $response['progress_su'] = ($count_feeding2/$count_child)*100;
                    $response['progress_ow'] = ($count_feeding3/$count_child)*100;
                    $response['progress_all'] = ($count_all/$count_child)*100;

                    $response['test'] = $cycle_id;


                }
            }else
            {
                $response['status'] = 200;
                $response['message'] = "Data not found!";
            }
            echo json_encode($response);
        }
    }else if($status == 'Update'){
        $cycle_id = $_POST['cycle_id'];
        $cycle = $_POST['cycle'];
        $date_added = $_POST['date_added'];


         $count = mysql_num_rows(mysql_query("SELECT * from tbl_feeding_header where cycle='$cycle' and  cycle_id!='$cycle_id'"));

        if($count>0){
            echo 2;
        }else{
            $query = mysql_query("UPDATE `tbl_feeding_header` SET `cycle`='$cycle',`date_added`='$date_added' WHERE cycle_id='$cycle_id'");
            if($query){
                insertlogs($user_id,"Updated cycle");
                echo 1;
            }else{
                echo 0;
            }
        }
    }else if($status == 'Delete'){
            $feeding_id = $_POST['feeding_id'];

            $query = mysql_query("DELETE FROM `tbl_feeding_details` WHERE feeding_id='$feeding_id'");
            if($query){
                insertlogs($user_id,"Deleted Feeding");
                echo 1;
            }else{
                echo 0;
            }
    }else if($status == 'datatable'){
        $cycle_id = $_POST['cycle_id'];


        $f = mysql_query("SELECT * from tbl_feeding_details  where   cycle_id='$cycle_id' and user_id='$user_id'") or die(mysql_error());
        $count = 1;
        $response['data'] = array();
            while ($row = mysql_fetch_array($f)) {
            $list = array();
                $list['count'] = $count++;

                $row_child = mysql_fetch_array(mysql_query("SELECT fname,lname FROM `tbl_child` where child_id='$row[child_id]'"));


                $list['id'] = $row['feeding_id'];
                $list['child'] = ucwords($row_child['fname']." ".$row_child['lname']);

                if($row['isfeedingremarks']==1){
                    $value ='Before feeding';
                }else if($row['isfeedingremarks']==2){
                    $value ='After 60 days';
                }else{
                    $value ='After 120 days';
                }


                if($row['standard_id']==1){
                    $status ='Normal';
                }else if($row['standard_id']==2){
                    $status ='Underweight';
                }else if($row['standard_id']==3){
                    $status ='Severely Underweight';
                }else if($row['standard_id']==4){
                    $status ='Over Weight';
                }else{
                    $status ='BMI out of range';
                }

                $list['status'] = $status;
                $list['feed_type'] = $value;
                $list['weight'] = $row['weight'];
                $list['bmi'] = $row['bmi'];


                array_push($response['data'], $list);
            }
            echo json_encode($response);

    }
?>