<?php 

function getOrdinalName($number){
 // get first digit
 $digit = abs($number) % 10;
 $ext = 'th';
$ext = ((abs($number) %100 < 21 && abs($number) %100 > 4) ? 'th' : (($digit < 4) ? ($digit < 3) ? ($digit < 2) ? ($digit < 1) ? 'th' : 'st' : 'nd' : 'rd' : 'th'));
 return $number.$ext;
}

	include "../core/config.php";
    $sort_by = $_POST['sort_by'];
    $input_sort = $_POST['input_sort'];

    session_start();
    $id = $_SESSION['id'];

    if($sort_by == 'D'){
        $add_query = "DATE_FORMAT(date_added,'%m/%d/%Y')='".$input_sort."'";
        $subtitle = $input_sort;
    }else if($sort_by == 'M'){
        $add_query = "DATE_FORMAT(date_added,'%Y-%m')='".date('Y').'-'.date('m',strtotime('1970-'.$input_sort.'-01'))."'";
        $subtitle = date('M',strtotime('1970-'.$input_sort.'-01')).", ".date('Y');
    }else if($sort_by == 'W'){
        $add_query = "DATE_FORMAT(date_added,'%U')=".$input_sort;
        $subtitle = getOrdinalName($input_sort)." week";
    }else if($sort_by == 'Y'){
        $add_query = "DATE_FORMAT(date_added,'%Y')=".$input_sort;
        $subtitle = $input_sort;
    }

?>
         <!-- begin: Invoice header-->
         <div class="row justify-content-centers">
            <div class="col-md-12 text-center">
                    <k4 class="display-4 font-weight-boldest"> ELIGIBILITY REPORT</k4>
                    <br>
                <?php echo $subtitle ;?> 
               
            </div>
        </div>
        <!-- end: Invoice header-->

    <!-- begin: Invoice footer-->
    <div class="row justify-content-center py-8 px-8 px-md-0">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th class="font-weight-bold text-muted  text-uppercase">Name</th>
                                <th class="font-weight-bold text-muted  text-uppercase">Amount</th>
                                <th class="font-weight-bold text-muted  text-uppercase">Invest for</th>
                                <th class="font-weight-bold text-muted  text-uppercase">Date</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php 

                        $fecth = mysql_query("SELECT * FROM tbl_eligibility where $add_query");


                        $total=0;
                        while($row=mysql_fetch_array($fecth)){
                            $total+=$row['amount'];?>
                            <tr class="font-weight-bolder">
                                <td><?php echo $row['name'];?></td>
                                <td class="text-danger font-size-h3 font-weight-boldest">&#8369; <?php echo $row['amount'];?></td>
                                <td><?php echo $row['inv'];?></td>

                                <td><?php echo date("M d,Y",strtotime($row['date_added']));?></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                        <tfoot>
                        <tr class="font-weight-bolder">
                                <td>Total</td>
                                <td class="text-danger font-size-h3 font-weight-boldest " colspan="2" style="text-decoration-line: underline;">&#8369; <?php echo number_format($total,2);?></td>

                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
        <!-- end: Invoice footer-->