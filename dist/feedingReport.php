<?php 

$cycle_id = $_GET['id'];



$row_cycle = mysql_fetch_array(mysql_query("SELECT * FROM `tbl_feeding_header` where cycle_id='$cycle_id'"));

function getCountYear($gender,$standard_id,$isfeedingremarks,$age_taken){

    $id = $_SESSION['id'];
    $status= $_SESSION['status'];
    $cycle_id = $_GET['id'];

    if($status=='V'){
        $add_query = 'and user_id='.$id;
    }else{
         $add_query = '';
    }

    if($gender == '-1'){
        $count = mysql_num_rows(mysql_query("SELECT * from tbl_feeding_details where age_taken='$age_taken' and cycle_id='$cycle_id' $add_query "));
    }else{
        $count = mysql_num_rows(mysql_query("SELECT * from tbl_feeding_details where standard_id = '$standard_id' and isfeedingremarks='$isfeedingremarks' and gender='$gender' and age_taken='$age_taken' and cycle_id='$cycle_id'  $add_query "));
    }
    if($count == 0){
        return '';
    }else{
        return $count;
    }
}

function getTotal($standard_id,$isfeedingremarks){
    $id = $_SESSION['id'];
    $status= $_SESSION['status'];
    $cycle_id = $_GET['id'];

    if($status=='V'){
        $add_query = 'where user_id='.$id.' and cycle_id='.$cycle_id;
        $add_query1 = ' and user_id='.$id.' and cycle_id='.$cycle_id;

    }else{
         $add_query = 'where cycle_id='.$cycle_id;
         $add_query1 = 'cycle_id='.$cycle_id;
    }


    if($standard_id == '-1'){
        $count = mysql_num_rows(mysql_query("SELECT * from tbl_feeding_details $add_query"));
    }else{
        $count = mysql_num_rows(mysql_query("SELECT * from tbl_feeding_details where standard_id = '$standard_id' and isfeedingremarks='$isfeedingremarks' $add_query1"));
    }


    if($count == 0){
        return '';
    }else{
        return $count;
    }
}
?>
<style type="text/css">
    .table-bordered th, .table-bordered td{
        border: 1px solid #080808;
        text-align: center;
    }
</style>
<div class="subheader py-2 py-lg-6  subheader-solid " id="kt_subheader">
    <div class=" container-fluid  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
        <div class="d-flex align-items-center flex-wrap mr-1">
			<div class="d-flex align-items-baseline flex-wrap mr-5">
	            <h5 class="text-dark font-weight-bold my-1 mr-5">
					Report
				</h5>

	             <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
	                <li class="breadcrumb-item">
	                    <a href="#"class="text-muted">Supplementay Feeding Report</a>
					</li>
	            </ul>
	        </div>
            
        </div>
        <div class="">
        <button type="button" class="btn btn-primary font-weight-bold pull-right" onclick="printDiv();"> <span class="fas fa-print"></span> Print Invoice</button>
        </div>
    </div>
</div>
<!--end::Subheader-->

    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container p-0">
            <!-- begin::Card-->
<div class="card card-custom overflow-hidden" >
    <div class="card-body p-5">
        <!-- begin: Invoice-->
        <!-- begin: Invoice header-->
        <div class="row  py-8 px-8  px-md-0">
             <!--  -->
            <div class="col-md-12" id="DivIdToPrint">
				
               
                <div class="form-group row">
                    <div class="col-md-4 ">
                       <img src="assets/media/logo.jpg" style="width: 100%;">
                    </div>
                    <div class="col-md-4 text-center" id="main">

                        Department of Social Welfare and Development<br>Field Office VI<br>Supplementary Feedign Program<br><b>Nutrional Status
                        <?php echo date('Y');?>
                        (<?php echo getOrdinal($row_cycle['cycle']);?> Cycle)</b><br>Municipality of Murcia

                    </div>
                    <div class="col-md-4">
                       
                    </div>
                </div>
                   <div class="form-group row">
                        <div class="col-lg-5 col-md-9 col-sm-12 ">
                          <strong>Date of feeding started:</strong> <?php echo $row_cycle['date_added'];?> 
                        </div>
                        <div class="col-lg-5 col-md-9 col-sm-12 text-center">
                            <strong>Weight for Height</strong>
                        </div>
                    </div>

                    <div class="table-responsive">
                      <table class="table table-bordered table-hover table-checkable"  style="width: 100%">
                        <thead>
                            <tr >
                                <th rowspan="2" style="vertical-align : middle;text-align:center;width: 8%;">Age</th>
                                <th rowspan="2" style="vertical-align : middle;text-align:center;"># of Children weighted</th>
                                <th colspan="8">Weighted Before Feeding</th>
                                <th colspan="8">After 60 days of feeding</th>
                                <th colspan="8">After 120 days of feeding</th>
                            </tr>
                             <tr>
                                <th colspan="2">N</th>
                                <th colspan="2">UW</th>
                                <th colspan="2">SW</th>
                                <th colspan="2">OW</th>
                                <th colspan="2">N</th>
                                <th colspan="2">UW</th>
                                <th colspan="2">SW</th>
                                <th colspan="2">OW</th>
                                <th colspan="2">N</th>
                                <th colspan="2">UW</th>
                                <th colspan="2">SW</th>
                                <th colspan="2">OW</th>
                            </tr>
                             <tr>
                                <th colspan="2"></th>

                                <th>F</th><th>M</th>
                                <th>F</th><th>M</th>
                                <th>F</th><th>M</th>
                                <th>F</th><th>M</th>
                                <th>F</th><th>M</th>
                                <th>F</th><th>M</th>
                                <th>F</th><th>M</th>
                                <th>F</th><th>M</th>
                                <th>F</th><th>M</th>
                                <th>F</th><th>M</th>
                                <th>F</th><th>M</th>
                                <th>F</th><th>M</th>
                            </tr>

                            </thead>
                            <tbody id="fam_body">
                                <?php 
                                $start = 2;
                                while ($start<=5){
                                ?>
                                <tr>
                                    <td><?php echo $start;?> yo</td>
                                    <td><?php echo getCountYear('-1','-1','-1',$start);?></td>
                                    <td><?php echo getCountYear('M','1','1',$start);?></td>
                                    <td><?php echo getCountYear('F','1','1',$start);?></td>
                                    <td><?php echo getCountYear('M','2','1',$start);?></td>
                                    <td><?php echo getCountYear('F','2','1',$start);?></td>
                                    <td><?php echo getCountYear('M','3','1',$start);?></td>
                                    <td><?php echo getCountYear('F','3','1',$start);?></td>
                                    <td><?php echo getCountYear('M','4','1',$start);?></td>
                                    <td><?php echo getCountYear('F','4','1',$start);?></td>
                                    

                                    <td><?php echo getCountYear('M','1','2',$start);?></td>
                                    <td><?php echo getCountYear('F','1','2',$start);?></td>
                                    <td><?php echo getCountYear('M','2','2',$start);?></td>
                                    <td><?php echo getCountYear('F','2','2',$start);?></td>
                                    <td><?php echo getCountYear('M','3','2',$start);?></td>
                                    <td><?php echo getCountYear('F','3','2',$start);?></td>
                                    <td><?php echo getCountYear('M','4','2',$start);?></td>
                                    <td><?php echo getCountYear('F','4','2',$start);?></td>

                                    <td><?php echo getCountYear('M','1','3',$start);?></td>
                                    <td><?php echo getCountYear('F','1','3',$start);?></td>
                                    <td><?php echo getCountYear('M','2','3',$start);?></td>
                                    <td><?php echo getCountYear('F','2','3',$start);?></td>
                                    <td><?php echo getCountYear('M','3','3',$start);?></td>
                                    <td><?php echo getCountYear('F','3','3',$start);?></td>
                                    <td><?php echo getCountYear('M','4','3',$start);?></td>
                                    <td><?php echo getCountYear('F','4','3',$start);?></td>

                                </tr>
                                <?php $start++;}?>
                                </tbody>
                            <tfoot>
                                 <tr>
                                    <td colspan="2" class="text-right font-weight-bolder">Subtotal</td>
                                    <?php $start_subtotal = 1;
                                        while ($start_subtotal<=24){?>
                                          <td></td>
                                    <?php $start_subtotal++;}?>
                                </tr>
                                <tr>
                                    <td>Total</td>
                                    <td><?php echo getTotal('-1','-1');?></td>

                                    <td colspan="2"><?php echo getTotal('1','1');?></td>
                                    <td colspan="2"><?php echo getTotal('2','1');?></td>
                                    <td colspan="2"><?php echo getTotal('3','1');?></td>
                                    <td colspan="2"><?php echo getTotal('4','1');?></td>

                                    <td colspan="2"><?php echo getTotal('1','2');?></td>
                                    <td colspan="2"><?php echo getTotal('2','2');?></td>
                                    <td colspan="2"><?php echo getTotal('3','2');?></td>
                                    <td colspan="2"><?php echo getTotal('4','2');?></td>

                                    <td colspan="2"><?php echo getTotal('1','3');?></td>
                                    <td colspan="2"><?php echo getTotal('2','3');?></td>
                                    <td colspan="2"><?php echo getTotal('3','3');?></td>
                                    <td colspan="2"><?php echo getTotal('4','3');?></td>
                                    
                                </tr>
                                
                            </tfoot>
                        </table>
                    </div>

            </div>
        </div>

        <!-- end: Invoice header-->
        <!-- end: Invoice-->
    </div>
</div>
<!-- end::Card-->
        </div>
        <!--end::Container-->
    </div>
<!--end::Entry-->

<script type="text/javascript">
    function printDiv() 
{

  var divToPrint=document.getElementById('DivIdToPrint');

  var newWin=window.open('','Print-Window');

  newWin.document.open();

  newWin.document.write('<html> <link href="assets/css/style.bundle.css" rel="stylesheet" type="text/css"/><link href="assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css"/><link href="assets/plugins/custom/prismjs/prismjs.bundle.css" rel="stylesheet" type="text/css"/><style>img{display: block;width: 100%!important;} #main{width:70%;}</style><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

  newWin.document.close();

  setTimeout(function(){newWin.close();},10);

}
</script>