<?php 
function getOrdinalName($number){
 // get first digit
 $digit = abs($number) % 10;
 $ext = 'th';
$ext = ((abs($number) %100 < 21 && abs($number) %100 > 4) ? 'th' : (($digit < 4) ? ($digit < 3) ? ($digit < 2) ? ($digit < 1) ? 'th' : 'st' : 'nd' : 'rd' : 'th'));
 return $number.$ext;
}

?>

<div class="subheader py-2 py-lg-6  subheader-solid " id="kt_subheader">
    <div class=" container-fluid  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
        <div class="d-flex align-items-center flex-wrap mr-1">
			<div class="d-flex align-items-baseline flex-wrap mr-5">
	            <h5 class="text-dark font-weight-bold my-1 mr-5">
					Report
				</h5>

	             <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
	                <li class="breadcrumb-item">
	                    <a href="#"class="text-muted">Petty Cash Voucher</a>
					</li>
	            </ul>
	        </div>
        </div>
    </div>
</div>
<!--end::Subheader-->

    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class=" container ">
            <!-- begin::Card-->
<div class="card card-custom overflow-hidden">
    <div class="card-body p-5">
        <!-- begin: Invoice-->
        <!-- begin: Invoice header-->
        <div class="row  py-8 px-8  px-md-0">
            <div class="col-md-12">
            <div class="form-group row">
				<label class="col-form-label text-left col-lg-1 col-sm-12">Sort By </label>
                <div class="col-lg-2 col-md-9 col-sm-12">
                    <select class="form-control" onchange="getInput()" id="sort_by"> 
                        <option value=""></option>
                        <option value="D">Daily</option>
                        <option value="W">Weekly</option>
                        <option value="M">Monthly</option>
                        <option value="Y">Yearly</option>
                    </select>
                </div>

                <div id="input_date"  style="display:none;" class="form-group row input_field">
                    <label class="col-form-label text-left col-lg-3 col-sm-12">Date </label>
                    <div class="col-lg-6 col-md-9 col-sm-12">
                        <input type='text' class="form-control" id="kt_datepicker_1" readonly type="text"/>
                    </div>

                </div>
                
                <div id="input_week"  style="display:none;" class="form-group row input_field">
                    <label class="col-form-label text-left col-lg-4 col-sm-12">Week</label>
                    <div class="col-lg-8 col-md-9 col-sm-12">
                        <select class="form-control"  id="kt_week">
                        <?php 
                        $week=1;
                            while($week<=52){
                                echo "<option value=".$week.">".getOrdinalName($week)." week </option>";
                                $week++;
                            }
                        ?>
                        </select>
                    </div>
                </div>

                <div id="input_month"  style="display:none;" class="row input_field">
                    <label class="col-form-label text-left col-lg-5  col-sm-12">Month</label>
                    <!-- <div class="col-lg-4">
                         <select class="form-control">
                            <?php 
                            $year='2015';
                            $endyear = date('Y');
                            while($year<=$endyear){
                                if($year ==$endyear){
                                    echo "<option value=".$year." selected>".$year."</option>";
                                }else{
                                    echo "<option value=".$year.">".$year."</option>";
                                }
                                $year++;
                            }
                            ?>
                        </select>
                    </div> -->
                    <div class="col-lg-7 col-md-9 col-sm-12">
                        <select class="form-control" id="kt_month">
                            <?php 
                            $start=1;
                            $endyear = date('Y');

                            while($start<=12){
                                echo "<option value=".$start.">".date('M, Y',strtotime($endyear.'-'.$start.'-01'))."</option>";
                                $start++;
                            }
                            ?>
                        </select>
                    </div>
                </div>

                
                <div id="input_year" style="display:none;" class="form-group row input_field">

                    <label class="col-form-label text-left col-lg-5 col-sm-12">Year</label>
                    <div class="col-lg-7 col-md-9 col-sm-12">
                        <select class="form-control" id="kt_year">
                        <?php 
                            $year='2015';
                            $endyear = date('Y');
                            while($year<=$endyear){
                                if($year ==$endyear){
                                    echo "<option value=".$year." selected>".$year."</option>";
                                }else{
                                    echo "<option value=".$year.">".$year."</option>";
                                }
                                $year++;
                            }
                            ?>
                        </select>
                    </div>

                </div>

                <div class="col-lg-3 col-md-9 col-sm-12">
                <button type="button" class="btn btn-light-primary font-weight-bold" onclick="generate()"> <span class="fas fa-sync-alt"></span> Generate Report </button>
                </div>
                <div class="col-lg-3 col-md-9 col-sm-12">
                <button type="button" class="btn btn-default font-weight-bold" onclick="printDiv();"> <span class="fas fa-print"></span> Print Invoice</button>
				</div>
            </div>
            <hr>
                <div id="report_data">
                
                </div>
                
            </div>
        </div>
        <!-- end: Invoice header-->
        <!-- end: Invoice-->
    </div>
</div>
<!-- end::Card-->
        </div>
        <!--end::Container-->
    </div>
<!--end::Entry-->

<script>

function generate(id){
     var sort_by = $("#sort_by").val();
    var input_sort = (sort_by == 'D')?$("#kt_datepicker_1").val():((sort_by == 'W')?$("#kt_week").val():( (sort_by == 'M')?$("#kt_month").val():((sort_by == 'Y')?$("#kt_year").val():'')));

    $.ajax({
        url:"ajax/reportPetty.php",
        type:"POST",
        data:{
             sort_by:sort_by,
            input_sort:input_sort
        },success:function(data){
            $("#report_data").html(data);
        }
    });
}

function getInput(){
    var sort_by = $("#sort_by").val();
    $(".input_field").hide();

   (sort_by == 'D')?$("#input_date").show():((sort_by == 'W')?$("#input_week").show():( (sort_by == 'M')?$("#input_month").show():((sort_by == 'Y')?$("#input_year").show():'')));
}

function printDiv() 
{

  var divToPrint=document.getElementById('report_data');

  var newWin=window.open('','Print-Window');

  newWin.document.open();

  newWin.document.write('<html><link href="assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css"/><link href="assets/plugins/custom/prismjs/prismjs.bundle.css" rel="stylesheet" type="text/css"/><link href="assets/css/style.bundle.css" rel="stylesheet" type="text/css"/><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

  newWin.document.close();

  setTimeout(function(){newWin.close();},10);

}

</script>
    