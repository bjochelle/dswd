<div class="subheader py-2 py-lg-6  subheader-solid " id="kt_subheader">
    <div class=" container-fluid  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
        <div class="d-flex align-items-center flex-wrap mr-1">
			<div class="d-flex align-items-baseline flex-wrap mr-5">
	            <h5 class="text-dark font-weight-bold my-1 mr-5">
					Children
				</h5>

	             <!-- <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
	                <li class="breadcrumb-item">
	                    <a href=""class="text-muted">General</a>
					</li>
	                 <li class="breadcrumb-item">
	                    <a href="#" class="text-muted"> dasdsa Page</a>
					</li>
	            </ul> -->
	        </div>
        </div>
    </div>
</div>
<!--end::Subheader-->

<!--begin::Card-->
<div class="card card-custom">
	<div class="card-header">
		<div class="card-title">
			<span class="card-icon"><i class="flaticon-notepad text-primary"></i></span>
			<h3 class="card-label">List of children</h3>
		</div>
		<div class="card-toolbar">
		<!--begin::Button-->
		<?php if($profile['status']=='V'){?>
			<button type="button" class="btn btn-primary" onclick='redirect()' ><span class="fas fa-plus-circle"></span>
			New Record 
			</button>
		<?php }?>
		<!--end::Button-->
		</div>
	</div>

	<div class="card-body">
		<!--begin: Datatable-->
		<table class="table table-bordered table-hover table-checkable" id="kt_datatable" style="margin-top: 13px !important">
			<thead>
				<tr>
					<th></th>
					<th>Name</th>
					<th>Address</th>
					<th>Gender</th>
					<th>Birthday</th>
					<th>Status</th>
					<th>Volunteer Name</th>
					<th>Actions</th>
				</tr>
			</thead>
        </table>
		<!--end: Datatable-->
	</div>
</div>
<!--end::Card-->

<script>
function getData(){
      var table = $('#kt_datatable').DataTable();
      table.destroy();
      var status = 'datatable';
      $("#kt_datatable").dataTable({
        "processing":true,
        "ajax":{
          "type":"POST",
          "url":"ajax/childCrud.php",
          "dataSrc":"data",
          "data":{
            status:status
          }
        },
        "columns":[
          {
            "data":"count"
          },
          {
            "data":"name"
          },
          {
            "data":"address"
          },
          {
            "data":"gender"
          },
          {
            "data":"bday"
          },  
          {
            "data":"status"
          },
          {
            "data":"volunteer_name"
          },       
          {
            "mRender": function(data,type,row){

            	if('<?php echo $status;?>'=='V'){
            		var edit = '<a href="javascript:;" class="btn btn-sm btn-clean btn-icon mr-1" title="Edit details" onclick="edit('+row.id+')"><i class="la la-edit"></i></a>';
            	}else{
            		var edit = '';
            	}

            	if(row.archive_status == '0'){
            		var btn_archive = '<a href="javascript:;" class="btn btn-sm btn-clean btn-icon" title="Archive"><i class="la la-box-open" onclick="archive('+row.id+')"></i></a>';
            	}else{
            		var btn_archive = '';
            	}

				return '<div class="d-flex align-items-center">'
				+'<div class="dropdown dropdown-inline mr-1"><div class="dropdown-menu dropdown-menu-sm 	dropdown-menu-right"></div></div>'
				+edit
				+'<a href="javascript:;" class="btn btn-sm btn-clean btn-icon" title="View"><i class="la la-eye" onclick="view('+row.id+')"></i></a>'
				+btn_archive+'</div>';
            }
          }
        ]
      });
    }



		function archive(id){
		$.ajax({
			url:"ajax/childCrud.php",
			type:"POST",
			data:{
				id:id,
				status:'Archive'
			},success:function(data){
				alert(data)
				if(data == 1){
					alertMe("All is cool! Successfully archived staff","success");
					getData();
			   }else{
					alertMe("Sorry, looks like there are some errors detected, please try again.","error");
			   }
				
			}
		});
	}


	
	function view(id){
        window.location.replace("home.php?view=viewChild&id="+id);
	}

	function edit(id){
        window.location.replace("home.php?view=editChild&id="+id);
	}
function redirect(){
    window.location.replace("home.php?view=addChild");
}
jQuery(document).ready(function() {
	getData();
});
</script>