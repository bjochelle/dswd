<?php 

$child_id = $_GET['id'];
$child_row = mysql_fetch_array(mysql_query("SELECT * FROM  tbl_child where child_id='$child_id'"));
?>
<style>
        .wrap { max-width: 980px; margin: 10px auto 0; }
        #steps { margin: 80px 0 0 0 }
        .commands { overflow: hidden; margin-top: 30px; }
        .prev {float:left}
        .next, .submit {float:right}
        .error { color: #b33; }
        #progress { position: relative; height: 5px; background-color: #eee; margin-bottom: 20px; }
        #progress-complete { border: 0; position: absolute; height: 5px; min-width: 10px; background-color: #337ab7; transition: width .2s ease-in-out; }

    </style>
    
       <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/jquery.validate.min.js"></script>
    <script src="assets/js/jquery.formtowizard.js"></script>
    
    <script>
        $( function() {
            var $signupForm = $( '#SignupForm' );
            
            $signupForm.validate({errorElement: 'em'});
            
            $signupForm.formToWizard({
                submitButton: 'SaveAccount',
                nextBtnClass: 'btn btn-primary next',
                prevBtnClass: 'btn btn-default prev',
                buttonTag:    'button',
                validateBeforeNext: function(form, step) {
                    var stepIsValid = true;
                    var validator = form.validate();
                    $(':input', step).each( function(index) {
                        var xy = validator.element(this);
                        stepIsValid = stepIsValid && (typeof xy == 'undefined' || xy);
                    });
                    return stepIsValid;
                },
                progress: function (i, count) {
                    $('#progress-complete').width(''+(i/count*100)+'%');
                }
            });
        });
    </script>

<div class="subheader py-2 py-lg-6  subheader-solid " id="kt_subheader">
    <div class=" container-fluid  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
        <div class="d-flex align-items-center flex-wrap mr-1">
			<div class="d-flex align-items-baseline flex-wrap mr-5">
	            <h5 class="text-dark font-weight-bold my-1 mr-5">
					Edit  Child Profile
				</h5>
	        </div>
        </div>
    </div>
</div>
<!--end::Subheader-->

<!--begin::Card-->
<div class="card card-custom">
	<div class="card-header">
		<div class="card-title">
			<span class="card-icon"><i class="flaticon-notepad text-primary"></i></span>
			<h3 class="card-label">Child Form</h3>
		</div>
	</div>


	<div class="card-body">

    <div class="row wrap"><div class="col-lg-12">

    <div id='progress'><div id='progress-complete'></div></div>

    <form id="SignupForm" action="">
        <fieldset>
            <legend>Account information </legend>
               <!--begin::Input-->
               <div class="form-group">
                                    <label>First Name</label>
                                    <input type="text" class="form-control form-control-solid form-control-lg" name="fname" placeholder="First Name" value="<?php echo $child_row['fname'];?>" />
                                    <span class="form-text text-muted">Please enter your first name.</span>
                                </div>
                                <!--end::Input-->

                                <!--begin::Input-->
                                <div class="form-group">
                                    <label>Middle Name</label>
                                    <input type="text" class="form-control form-control-solid form-control-lg" name="mname" placeholder="Middle Name" value="<?php echo $child_row['mname'];?>" />
                                    <span class="form-text text-muted">Please enter your middle name.</span>
                                </div>
                                <!--end::Input-->

                                <!--begin::Input-->
                                <div class="form-group">
                                    <label>Last Name</label>
                                    <input type="text" class="form-control form-control-solid form-control-lg" name="lname" placeholder="Last Name" value="<?php echo $child_row['lname'];?>" />
                                    <span class="form-text text-muted">Please enter your last name.</span>
                                </div>
                                <!--end::Input-->

                                 <!--begin::Input-->
                                 <div class="form-group">
                                    <label>Address</label>
                                    <input type="text" class="form-control form-control-solid form-control-lg" name="address" placeholder="Address Line" value="<?php echo $child_row['address'];?>" />
                                    <span class="form-text text-muted">Please enter your Address.</span>
                                </div>
                                <!--end::Input-->

                                  <!--begin::Input-->
                                  <div class="form-group">
                                    <label>Religion</label>
                                    <input type="text" class="form-control form-control-solid form-control-lg" name="religion" placeholder="Religion" value="<?php echo $child_row['religion'];?>" />
                                    <span class="form-text text-muted">Please enter your religion.</span>
                                </div>
                                <!--end::Input-->


                                <div class="row">
                                    <div class="col-xl-6">
                                        <!--begin::Input-->
                                        <div class="form-group">
                                            <label>Date of birth</label>
                                            <input type="date" class="form-control form-control-solid form-control-lg" name="birthdate" placeholder="birthdate" value="<?php echo $child_row['bday'];?>" />
                                            <span class="form-text text-muted">Please enter your birthdate.</span>
                                        </div>
                                        <!--end::Input-->
                                    </div>
                                    <div class="col-xl-6">
                                    <!--begin::Input-->
                                    <div class="form-group">
                                        <label>Gender</label>
                                        <select name="gender" class="form-control form-control-solid form-control-lg" id="gender">
                                            <option value="Male">Male</option>
                                            <option value="Female">Female</option>
                                        </select>
                                    </div>
                                        <!--end::Input-->
        </fieldset>

        <fieldset>
            <legend>II .Family Compostion :</legend>
                <div class="row">
                <div class="col-xl-6">
                    <!--begin::Input-->
                    <div class="form-group">
                        <label>Number of Family Member living in the same house</label>
                        <select id="num_fam" class="form-control form-control-solid form-control-lg" onchange="gen()">
                        <?php 
                        $counter =0;
                        while($counter <=12){
                            echo '<option value='.$counter.'>'.$counter.'</option>';
                            $counter++;
                        }
                        ?>
                    </select>
                    </div>
                    <!--end::Input-->
                </div>
                <div class="table-responsive">
                  <table class="table table-bordered table-hover table-checkable"  style="width: 100%">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Relationship</th>
                            <th>Age</th>
                            <th>Civil Status</th>
                            <th>Occupation</th>
                            <th>Income</th>
                        </tr>
                        <tbody id="fam_body">
                            <?php 
                            $fam_fetch = mysql_query("SELECT * FROM `tbl_family`  where child_id='$child_id'");
                            while ($fam_row = mysql_fetch_array($fam_fetch)) {?>
                                <tr>
                                    <td><input type='text' name='fam_name[]' value="<?php echo $fam_row['fam_name'];?>"></td>
                                    <td><input type='text' name='fam_rel[]' value="<?php echo $fam_row['fam_rel'];?>"></td>
                                    <td><input type='text' name='fam_age[]' value="<?php echo $fam_row['fam_age'];?>"></td>
                                    <td><input type='text' name='fam_civil_status[]' value="<?php echo $fam_row['fam_civil_status'];?>"></td>
                                    <td><input type='text' name='fam_occupation[]' value="<?php echo $fam_row['fam_occupation'];?>"></td>
                                    <td><input type='text' name='fam_income[]' value="<?php echo $fam_row['fam_income'];?>"></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </thead>
                </table>
            </div>
              
            </div>
            <br>
            <br>
                    <!--end::Input-->

            <legend>Problems/Nees Commonly Encounterd</legend>
            <div class="row">
            
        
            <div class="row">
            <div class="col-xl-12">
            <!--begin::Input-->
                <div class="form-group">
                    <label><strong>Living/Residing With: </strong></label>
                    <div class="form-group">
                    <div class="checkbox-list">
                        <label class="checkbox">
                            <input type="radio" name="livingwt" value="Alone" <?php echo (strpos($child_row['livingwt'], 'Alone') !== false)? 'checked':'';?> />
                            <span></span>
                            Alone
                        </label>
                        <label class="checkbox">
                            <input type="radio" value="Parents" name="livingwt" <?php echo (strpos($child_row['livingwt'], 'Parents') !== false)? 'checked':'';?>   />
                            <span></span>
                            Parents
                        </label>
                        <label class="checkbox">
                            <input type="radio" value="Care" name="livingwt" <?php echo (strpos($child_row['livingwt'], 'Care') !== false)? 'checked':'';?>  />
                            <span></span>
                            <div>Care Institution</div>
                            
                        </label>
                        <label class="checkbox">
                            <input type="radio" name="livingwt" value="Children" <?php echo (strpos($child_row['livingwt'], 'Children') !== false)? 'checked':'';?> />
                            <span></span>
                            Children
                        </label>
                        <label class="checkbox">
                            <input type="radio" name="livingwt" value="Friends" <?php echo (strpos($child_row['livingwt'], 'Friends') !== false)? 'checked':'';?> />
                            <span></span>
                            Friends
                        </label>
                        <label class="checkbox">
                            <input type="radio" name="livingwt" value="Relatives" <?php echo (strpos($child_row['livingwt'], 'Relatives') !== false)? 'checked':'';?> />
                            <span></span>
                            Relatives
                        </label>
                        <label class="checkbox">
                            <input type="radio" name="livingwt" id="checkboc_lr" value="others" <?php echo (strpos($child_row['livingwt'], 'others') !== false)? 'checked':'';?> onclick="getValue('checkboc_lr','text_lr')" />
                            <span></span>
                            Others: 
                        </label>
                        <input type="text" name="livingwt" id="text_lr"  id="others" value='<?php $livingwt = explode(",", $child_row['livingwt']); echo (strpos($child_row['livingwt'], 'others') !== false)?  $livingwt[1]:'';?>' <?php echo (strpos($child_row['livingwt'], 'others') !== false)? '':'disabled';?>/>
                    </div>
                </div>
                </div>
            <!--end::Input-->

             <!--begin::Input-->
             <div class="form-group">
                    <label><strong>Areas of interest:</strong></label>
                    <div class="form-group">
                    <div class="checkbox-list">
                        <label class="checkbox">
                            <input type="checkbox" name="a_int[]" value="Medical" <?php echo (strpos($child_row['a_int'], 'Medical') !== false)? 'checked':'';?>/>
                            <span></span>
                            Medical
                        </label>
                        <label class="checkbox">
                            <input type="checkbox" name="a_int[]" value="Teaching" <?php echo (strpos($child_row['a_int'], 'Teaching') !== false)? 'checked':'';?>/>
                            <span></span>
                            Teaching
                        </label>
                        <label class="checkbox">
                            <input type="checkbox" name="a_int[]" value="Legal" <?php echo (strpos($child_row['a_int'], 'Legal') !== false)? 'checked':'';?>/>
                            <span></span>
                            <div>Legal Services</div>
                            
                        </label>
                        <label class="checkbox">
                            <input type="checkbox" name="a_int[]" value="Dental" <?php echo (strpos($child_row['a_int'], 'Dental') !== false)? 'checked':'';?>/>
                            <span></span>
                            Dental
                        </label>
                        <label class="checkbox">
                            <input type="checkbox" name="a_int[]" value="Counseling" <?php echo (strpos($child_row['a_int'], 'Counseling') !== false)? 'checked':'';?>/>
                            <span></span>
                            Counseling
                        </label>
                        <label class="checkbox">
                            <input type="checkbox" name="a_int[]" value="Evangelization" <?php echo (strpos($child_row['a_int'], 'Evangelization') !== false)? 'checked':'';?>/>
                            <span></span>
                            Evangelization
                        </label>
                        <label class="checkbox">
                            <input type="checkbox" id="checkbox_area" name="a_int[]" value="others" <?php echo (strpos($child_row['a_int'], 'others') !== false)? 'checked':'';?> onclick="getValue('checkbox_area','text_area')"/>
                            <span></span>
                            Others: 
                        </label>
                        <input type="text" name="a_int[]" id="text_area"  value='<?php $a_int = explode(",", $child_row['a_int']); echo (strpos($child_row['a_int'], 'others') !== false)?  end($a_int):'';?>'/>
                    </div>
                </div>
                </div>
            <!--end::Input-->
                
            </div> 
        </fieldset>

        <fieldset class="form-horizontal" role="form">
            <legend>III .Problem/needs commonly encounter(Parent):</legend>

              <!--begin::Input-->
              <div class="form-group">
                    <label><strong>A. Economic:</strong></label>
                    <div class="form-group">
                    <div class="checkbox-list">
                        <label class="checkbox">
                            <input type="checkbox" name="prob[]" value="Lack of income" <?php echo (strpos($child_row['prob'], 'Lack of income') !== false)? 'checked':'';?>  />
                            <span></span>
                            Lack of income/Resources
                        </label>
                        <label class="checkbox">
                            <input type="checkbox"  id="checkbox_skills" name="prob[]" value="Skills" <?php echo (strpos($child_row['prob'], 'Skills') !== false)? 'checked':'';?> />
                            <span></span>
                            Skills/Capacity Training
                        </label>
                        <label class="checkbox">
                            <input type="checkbox" id="checkbox_livelihood"  name="prob[]" value="Livelihood" <?php echo (strpos($child_row['prob'], 'Livelihood') !== false)? 'checked':'';?> />
                            <span></span>
                            Livelihood opportunities
                        </label>
                        <label class="checkbox">
                            <input type="checkbox"  id="checkbox_economic" name="prob[]" value="others "<?php echo (strpos($child_row['prob'], 'others') !== false)? 'checked':'';?>  onclick="getValue('checkbox_economic','textbox_economic')"/>
                            <span></span>
                            Others
                        </label>
                        <input type="text" name="prob[]" id="textbox_economic" value='<?php $prob = explode(",", $child_row['prob']); echo (strpos($child_row['prob'], 'others') !== false)?  end($prob):'';?>'/>
                    </div>
                </div>
                </div>
            <!--end::Input-->
              <!--begin::Input-->
              <div class="form-group">
                    <label><strong>B. Socio/Emiotional(Child)</strong></label>
                    <div class="form-group">
                    <div class="checkbox-list">
                        <label class="checkbox">
                            <input type="checkbox" name="socio[]" value="Feeling of neglect"  <?php echo (strpos($child_row['socio'], 'Feeling of neglect') !== false)? 'checked':'';?>/>
                            <span></span>
                            Feeling of neglect & rejection
                        </label>
                        <label class="checkbox">
                            <input type="checkbox" name="socio[]" value="Feeling of helplessness"  <?php echo (strpos($child_row['socio'], 'Feeling of helplessness') !== false)? 'checked':'';?> />
                            <span></span>
                            Feeling of helplessness & worthlessness
                        </label>
                        <label class="checkbox">
                            <input type="checkbox" name="socio[]" value="Feeling of loneliness"  <?php echo (strpos($child_row['socio'], 'Feeling of loneliness') !== false)? 'checked':'';?>/>
                            <span></span>
                            Feeling of loneliness & isolation
                            
                        </label>
                        <label class="checkbox">
                            <input type="checkbox" name="socio[]" value="Inadequate leisure"  <?php echo (strpos($child_row['socio'], 'Inadequate leisure') !== false)? 'checked':'';?>/>
                            <span></span>
                            Inadequate leisure/ recreational activites
                        </label>
                        <label class="checkbox">
                            <input type="checkbox" name="socio[]" value="Senior"  <?php echo (strpos($child_row['socio'], 'Senior') !== false)? 'checked':'';?>/>
                            <span></span>
                            Senior Citizen Friendly Environment
                        </label>
                        <label class="checkbox">
                            <input type="checkbox"  id="checkbox_socio" name="socio[]" value="others"  <?php echo (strpos($child_row['socio'], 'others') !== false)? 'checked':'';?> onclick="getValue('checkbox_socio','textbox_socio')"/> 
                            
                            <span></span>
                            Others
                        </label>
                       <input type="text" name="socio[]" id="textbox_socio" value='<?php $socio = explode(",", $child_row['socio']); echo (strpos($child_row['socio'], 'others') !== false)?  end($socio):'';?>'  placeholder="Specify:"/>
                    </div>
                </div>
                </div>
            <!--end::Input-->
            
              <!--begin::Input-->
              <div class="form-group">
                    <label><strong>C. Health(Child)</strong></label>
                    <div class="form-group">
                    <div class="checkbox-list">
                        <label class="checkbox">
                            <input type="checkbox" placeholder="Please specify" name="health[]" value="Condition" <?php echo (strpos($child_row['health'], 'Condition') !== false)? 'checked':'';?>/>
                            <span></span>
                            Condition / Illness
                        </label>
                        <label class="checkbox">
                            <input type="checkbox" placeholder="Please specify" name="health[]" value="With maintenance" <?php echo (strpos($child_row['health'], 'With maintenance') !== false)? 'checked':'';?> />
                            <span></span>
                            With maintenance
                        </label>
                        <label class="checkbox">
                            <input type="checkbox" name="health[]" value="High cost medicine" <?php echo (strpos($child_row['health'], 'High cost medicine') !== false)? 'checked':'';?>/>
                            <span></span>
                            High cost medicine
                        </label>
                        <label class="checkbox">
                            <input type="checkbox" name="health[]" value="Lack of medical professionals" <?php echo (strpos($child_row['health'], 'Lack of medical professionals') !== false)? 'checked':'';?>/>
                            <span></span>
                            Lack of medical professionals
                        </label>
                        <label class="checkbox">
                            <input type="checkbox" name="health[]" value="Lack/No access to sanitation" <?php echo (strpos($child_row['health'], 'Lack/No access to sanitation') !== false)? 'checked':'';?>/>
                            <span></span>
                            Lack/No access to sanitation
                        </label>
                        <label class="checkbox">
                            <input type="checkbox" id="checkbox_ailments" name="health[]" value="Health" <?php echo (strpos($child_row['health'], 'Health') !== false)? 'checked':'';?>/>
                            <span></span>
                            Health problems/Ailments
                        </label>

                        <label class="checkbox">
                            <input type="checkbox" name="health[]" value="No health insurance" <?php echo (strpos($child_row['health'], 'No health insurance') !== false)? 'checked':'';?>/>
                            <span></span>
                            Lack/No health insurance/s inadequate health services
                        </label>

                        <label class="checkbox">
                            <input type="checkbox" name="health[]" value="medical facilities" <?php echo (strpos($child_row['health'], 'medical facilities') !== false)? 'checked':'';?>/>
                            <span></span>
                            Lack of hospitals/medical facilities
                        </label>
                        <label class="checkbox">
                            <input type="checkbox"  id="checkbox_health" name="health[]" value="others" <?php echo (strpos($child_row['health'], 'others') !== false)? 'checked':'';?> onclick="getValue('checkbox_health','textbox_health')"/>
                            <span></span>
                            Others
                        </label>
                        <input type="text" name="health[]" id="textbox_health"  value='<?php $health = explode(",", $child_row['health']); echo (strpos($child_row['health'], 'others') !== false)?  end($health):'';?>'  placeholder="Specify:"/>
                    </div>
                <br>
                    <label><strong>Deworming<strong></label>
                    <div class="row">
                        <div class="col-md-6">
                            <label> First  Dose</label>
                            <input type="date" name="deworming_fir" value="<?php echo $child_row['deworming_fir'];?>" />
                        </div>
                        <div class="col-md-6">
                            <label> Second  Dose</label>
                            <input type="date" name="deworming_sec" value="<?php echo $child_row['deworming_sec'];?>" />
                        </div>
                    </div>
               <!--      <table class="table table-bordered table-hover table-checkable"  style="margin-top: 13px !important">
                    <thead>
                        <tr>
                            <th rowspan="2">Nutritional Status</th>
                            <th colspan="2">Baseline</th>
                            <th colspan="2">After 3 months </th>
                            <th colspan="2">After Feeding</th>
                        </tr>
                        <tr>
                            <th>Wt/ht</th>
                            <th>NS</th>
                            <th>Wt/ht</th>
                            <th>NS</th>
                            <th>Wt/ht</th>
                            <th>NS</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Weight/Length for age:</td>
                            <td><input type="number" name="wt_lgth_age" min="1" value="<?php echo $child_row['wt_lgth_age'];?>" ></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Weight for age:</td>
                            <td><input type="number" name="wt_age" min="1" value="<?php echo $child_row['wt_age'];?>"></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Weight for Height/Length:</td>
                            <td><input type="number" name="weigh_hght_lg" min="1" value="<?php echo $child_row['weigh_hght_lg'];?>"></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </tbody>
                </table> -->

                </div>
                </div>
            <!--end::Input-->
              <!--begin::Input-->
              <div class="form-group">
                    <label><strong>E. Identify Others specific needs</strong></label>
                    <div class="form-group">
                    <textarea class="form-control" name="specific_needs"><?php echo $child_row['specific_needs'];?></textarea>
           
                </div>
                </div>
            <!--end::Input-->
           <input type="hidden" name="status" value="Update">
           <input type="hidden" name="child_id" value="<?php echo $child_id;?>">

        </fieldset>
        <p>
            <button id="SaveAccount" class="btn btn-primary submit">Submit form</button>
        </p>
    </form>

</div></div>
	
	</div>
</div>
<!--end::Card-->
<script>
    $(document).ready(function(){
        edit();
    });
    $("#SignupForm").submit(function(e){
        e.preventDefault();

         var livingwt_value =  $("input[name='livingwt']:checked").map(function(){
            return this.value;
        }).get();

        if(livingwt_value == 'others'){
            var livingwt = "others," + $("#text_lr").val();
        }else{
            var livingwt = livingwt_value;
        }

        $.ajax({
        url:"ajax/childCrud.php",
        method:"POST",
        data:$("#SignupForm").serialize() + '&livingwt=' + livingwt,
        success: function(data){
            if(data == 1){
                alertMe("All is cool! Successfully added child information","success");
                window.setTimeout(function(){ 
                    window.location.replace("home.php?view=children");
                } ,1000);
           }else{
                alertMe("Sorry, looks like there are some errors detected, please try again.","error");
           }
          
        }
     });
    })
function gen(){
    var input = $("#num_fam").val();
    var start =1;
    var tbody_data="";
    while(start <= input){
        tbody_data+="<tr>"+
                "<td><input type='text' name='fam_name[]'></td>"+
                "<td><input type='text' name='fam_rel[]'></td>"+
                "<td><input type='text' name='fam_age[]'></td>"+
                "<td><input type='text' name='fam_civil_status[]'></td>"+
                "<td><input type='text' name='fam_occupation[]'></td>"+
                "<td><input type='text' name='fam_income[]'></td>"+
                "</tr>";
         start=start+1;
    }
     $("#fam_body").append(tbody_data);
}

    function edit(){
        $("#id").val('<?php echo $_GET['id'];?>');
        $.ajax({
            url:"ajax/childCrud.php",
            type:"POST",
            data:{
                id:'<?php echo $_GET['id'];?>',
                status:'View'
            },success:function(data){
                var o = JSON.parse(data);
                $("#gender").val(o.gender);

               /* $("#fname").val(o.fname);
                $("#contact_number").val(o.contact_number);
                $("#bday").val(o.bday);
                $("#address").val(o.address);
                $("#email").val(o.email);
                $("#un").val(o.un);*/
            }
        });
    }

function getValue(checkbox,textbox){
    if($("#"+checkbox).prop("checked") == true){
        $("#"+textbox).prop('disabled',false);
    } else{
        $("#"+textbox).prop('disabled',true);
    }
}


</script>
