<?php 
    $row_cycle = mysql_fetch_array(mysql_query("SELECT * FROM `tbl_feeding_header` where cycle_id='$_GET[id]'"));
?>
<div class="subheader py-2 py-lg-6  subheader-solid " id="kt_subheader">
    <div class=" container-fluid  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
        <div class="d-flex align-items-center flex-wrap mr-1">
            <div class="d-flex align-items-baseline flex-wrap mr-5">
                <h5 class="text-dark font-weight-bold my-1 mr-5">
                    Feeding
                </h5>

                 <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                    <li class="breadcrumb-item">
                        <a href=""class="text-muted">Add Feeding Supplementary</a>
                    </li>
                     <li class="breadcrumb-item">
                        <a href="#" class="text-muted">Nutritional Status <?php echo date('Y');?> (<?php echo getOrdinal($row_cycle['cycle']);?> cycle) </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!--end::Subheader-->

<div class="card card-custom gutter-b">
    <div class="card-body">
        <!--begin::Details-->
        <div class="d-flex mb-9">
            <!--begin::Info-->
            <div class="flex-grow-1">
                <!--begin::Title-->
                <div class="d-flex justify-content-between flex-wrap mt-1">
                    <div class="d-flex mr-12" style="width: 100%;">
                        <label style="width:25%;">Volunteers </label>
                        <select name="volunteer_id" id="volunteer_id" class="form-control form-control-solid form-control-lg">
                            <?php 
                            $fetch_v = mysql_query("SELECT * FROM `tbl_user` where status='V'");
                            while ($row_v = mysql_fetch_array($fetch_v)){?>
                                 <option value="<?php echo $row_v['user_id'];?>"><?php echo ucwords($row_v['fname']." ".$row_v['lname']);?></option>
                            <?php }?>
                        </select>
                        <a href="#" class="btn btn-sm btn-light-success font-weight-bolder text-uppercase mr-3" onclick="getData()" id="btn_fetch">Fetch Volunteers</a>
                    </div>
                </div>
                <!--end::Title-->
                <br>
              <div class="separator separator-solid"></div>

              <div>
                    <div class="d-flex align-items-center w-100 flex-fill mt-lg-12 mt-8">
                        <span class="font-weight-bold text-dark-75">Progress</span>
                        <div class="progress progress-xs mx-3 w-100">
                            <div class="progress-bar bg-success" role="progressbar" id="progress_main" style="" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        <span class="font-weight-bolder text-dark" id="progress_value"></span>
                    </div>
                </div>
                <!--end::Content-->
            </div>
            <!--end::Info-->
        </div>
        <!--end::Details-->

        <div class="separator separator-solid"></div>


    </div>
</div>
<!--end::Card-->

<!--begin::Row-->
<div class="row">
    <div class="col-lg-8">
        <!--begin::Advance Table Widget 2-->
<div class="card card-custom card-stretch gutter-b">
    <!--begin::Header-->
    <div class="card-header border-0 pt-5">
        <h3 class="card-title align-items-start flex-column">
            <span class="card-label font-weight-bolder text-dark">Finished Feeding</span>
            <span class="text-muted mt-3 font-weight-bold font-size-sm">List of children</span>
        </h3>
    </div>
    <!--end::Header-->

    <!--begin::Body-->
    <div class="card-body pt-3 pb-0">
        <!--begin::Table-->
        <div class="table-responsive">
            <table class="table table-borderless table-vertical-center" id="kt_datatable">
                <thead>
                    <tr>
                        <th class="p-0" ></th>
                        <th class="p-0" >Student Name</th>
                        <th class="p-0" >BMI</th>
                        <th class="p-0" >Feed Type</th>
                        <th class="p-0" >Status</th>
                        <th class="p-0" >Volunteer Name</th>    
                    </tr>
                </thead>
                <tbody>
                   
                </tbody>
            </table>
        </div>
        <!--end::Table-->
    </div>
    <!--end::Body-->
</div>
<!--end::Advance Table Widget 2-->
    </div>
    <div class="col-lg-4">
        <!--begin::Mixed Widget 14-->
<div class="card card-custom card-stretch gutter-b">
    <!--begin::Header-->
    <div class="card-header border-0 pt-5">
        <h3 class="card-title font-weight-bolder ">Nutritional Status</h3>
    </div>
    <!--end::Header-->

    <!--begin::Body-->
    <div class="card-body d-flex flex-column">
        <div class="flex-grow-1">
            <label>N- Normal (<span id="count_n"><span>) </label>
            <div class="progress">

                <div class="progress-bar bg-info" id="progress_n"  role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <br>
            <label>UW- Underweight (<span id="count_uw"><span>) </label>

            <div class="progress">
                <div class="progress-bar bg-success" id="progress_uw"  role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <br>
            <label>SU- Severely Underweight (<span id="count_su"><span>) </label>

            <div class="progress">
                <div class="progress-bar bg-primary" id="progress_su" role="progressbar" style="width: 75%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <br>
            <label>OW- Overweight (<span id="count_ow"><span>) </label>

            <div class="progress">
                <div class="progress-bar bg-warning" id="progress_ow" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
        </div>
        <div class="pt-5">
            <p class="text-center font-weight-normal font-size-lg pb-7">
                Notes: This is a real time data<br/>
            </p>
            <a href="home.php?view=feedingReport&id=<?php echo $_GET['id'];?>" class="btn btn-success btn-shadow-hover font-weight-bolder w-100 py-3" >Generate Report</a>
        </div>
    </div>
    <!--end::Body-->
</div>
<!--end::Mixed Widget 14-->
    </div>
</div>
<!--end::Row-->


<script>
$(document).ready(function(){
    $('#kt_select2_1').select2({
    });
    getData();
})

     function progress(){
       var volunteer_id =  $("#volunteer_id").val();
         $.ajax({
            url:"ajax/getChildrenPerVolunteer.php",
            type:"POST",
            data:{
                volunteer_id:volunteer_id,
                cycle_id:'<?php echo $_GET['id'];?>',
                status :'View'

            },success:function(data){
                var o = JSON.parse(data);
                $("#count_n").html(o.progress_n+"%)");
                $("#count_uw").html(o.progress_uw+"%)");
                $("#count_su").html(o.progress_su+"%)");
                $("#count_ow").html(o.progress_ow+"%)");

                $("#progress_value").html(o.progress_all+"%");


                $("#progress_n").css('width',o.progress_n+"%");
                $("#progress_uw").css('width',o.progress_uw+"%");
                $("#progress_su").css('width',o.progress_su+"%");
                $("#progress_ow").css('width',o.progress_ow+"%");

                $("#progress_main").css('width',o.progress_all+"%");

            }
        });
    }

    function getData(){
      var table = $('#kt_datatable').DataTable();
      table.destroy();
      var cycle_id = '<?php echo $_GET['id'];?>';
      var volunteer_id =  $("#volunteer_id").val();
      progress();


      $("#kt_datatable").dataTable({
        "processing":true,
        "ajax":{
          "type":"POST",
          "url":"ajax/getChildrenPerVolunteer.php",
          "dataSrc":"data",
          "data":{
            status :'datatable',
            volunteer_id:volunteer_id,
            cycle_id:cycle_id
          }
        },
        "columns":[
          {
            "data":"count"
          },
          {
            "data":"child"
          },
          {
            "data":"bmi"
          },
          {
            "data":"feed_type"
          }, 
          {
            "data":"status"
          },     
          {
             "data":"volunteer"
          }
        ]
      });
    }
</script>