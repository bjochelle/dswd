<?php 

function getValue($status,$field){

	$fetch_standard = mysql_fetch_array(mysql_query("SELECT $field from tbl_standard where status ='$status' "));
	return $fetch_standard[0];
}
?>
<div class="subheader py-2 py-lg-6  subheader-solid " id="kt_subheader">
    <div class=" container-fluid  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
        <div class="d-flex align-items-center flex-wrap mr-1">
			<div class="d-flex align-items-baseline flex-wrap mr-5">
	            <h5 class="text-dark font-weight-bold my-1 mr-5">
					Nutritional Standard
				</h5>
	        </div>
        </div>
    </div>
</div>
<!--end::Subheader-->

<!--begin::Card-->
<div class="card card-custom">
	<div class="card-header">
		<div class="card-title">
			<span class="card-icon"><i class="fas fa-balance-scale text-primary"></i></span>
			<h3 class="card-label">Nutritional Standard</h3>
		</div>
		<div class="card-toolbar">

		</div>
	</div>
    <?php require('modal/modal_addPettyCash.php');
        require('modal/modal_editPettyCash.php');
    // require('modal/modal_viewStaff.php');?>

	<div class="card-body">
		<form id="update_standard">
		<!--begin: Datatable-->
		<table class="table table-bordered table-hover table-checkable" id="kt_datatable" style="margin-top: 13px !important">
			<thead>
				<tr>
					<th>Status</th>
					<th>Start Range (BMI)</th>
					<th>End Range (BMI)</th>
				</tr>
			</thead>
			<tbody id="standard_body">
				<tr>
					<td><input type="hidden" class="status" value='n'>Normal</td>
					<td><input type="number" name="sr" class="sr" required="" step="0.01" value="<?php echo getValue('n','start_range');?>"></td>
					<td><input type="number" name="er" class="er" required="" step="0.01" value="<?php echo getValue('n','end_range');?>"></td>
				</tr>
				<tr>
					<td><input type="hidden" class="status" value='uw'>Underweight</td>
					<td><input type="number" name="sr" class="sr" required="" step="0.01" value="<?php echo getValue('uw','start_range');?>"></td>
					<td><input type="number" name="er" class="er" required="" step="0.01" value="<?php echo getValue('uw','end_range');?>"></td>
				</tr>
				<tr>
					<td><input type="hidden" class="status" value='su'>Severely Underweight</td>
					<td><input type="number" name="sr" class="sr" required="" step="0.01" value="<?php echo getValue('su','start_range');?>"></td>
					<td><input type="number" name="er" class="er" required="" step="0.01" value="<?php echo getValue('su','end_range');?>"></td>
				</tr>
				<tr>
					<td><input type="hidden" class="status" value='ow'>Over weight</td>
					<td><input type="number" name="sr" class="sr" required="" step="0.01" value="<?php echo getValue('ow','start_range');?>"></td>
					<td><input type="number" name="er" class="er" required=""step="0.01"  value="<?php echo getValue('ow','end_range');?>"></td>
				</tr>
			</tbody>
        </table>
		<!--end: Datatable-->

		<!--begin::Button-->
		<button type="submit" class="btn btn-primary pull-right" ><span class="fas fa-check-circle"></span>
			Update Record
		</button>
		<!--end::Button-->
	</form>
	</div>
</div>
<!--end::Card-->

<script type="text/javascript">
	
	$("#update_standard").submit(function(e){
		e.preventDefault();
		var counter = 0;
		var value = '';

		$("#standard_body tr").each(function(){
		$(this).find(".sr").each(function(){
			sr = $(this).val();
		});
		
		$(this).find(".er").each(function(){
			er = $(this).val();
		});

		$(this).find(".status").each(function(){
			status = $(this).val();
		});
	
		var data_per_row = sr + "," + er + "," + status;
		
		var stat = status;

		$.post("ajax/update_standard.php",
		{
			status:status,
			sr:sr,
			er:er
		}, function(data){

			if(stat == 'n'){
			    value = 'Normal';
			}else if(stat == 'uw'){
			    value = 'Underweight';
			}else if(stat == 'su'){
			    value = 'Severely Underweight';
			}else{
			    value = 'Over Weight';
			}


			if(data == 1){
				// success in update
					// var width = (counter/4) * 100;
					// var setWidth = parseFloat(width).toFixed(2);
					// $("#farrow_progress").html(setWidth+'%');
					// $("#farrow_progress").css({ width: ""+setWidth+"%" });
					counter = counter + 1;
					if (counter >= 4) {
						alertMe("All is cool! Successfully updated standard","success");
						location.reload();
					}

			}else{
				

				alertMe("Sorry! Unable to update, Range is exsiting to status "+value+" ","error");
			}

			


		}
		);
		
	});
	})
</script>