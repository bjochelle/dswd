<div class="subheader py-2 py-lg-6  subheader-solid " id="kt_subheader">
    <div class=" container-fluid  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
        <div class="d-flex align-items-center flex-wrap mr-1">
			<div class="d-flex align-items-baseline flex-wrap mr-5">
	            <h5 class="text-dark font-weight-bold my-1 mr-5">
					Skip Feeding History
				</h5>

	             <!-- <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
	                <li class="breadcrumb-item">
	                    <a href=""class="text-muted">General</a>
					</li>
	                 <li class="breadcrumb-item">
	                    <a href="#" class="text-muted"> dasdsa Page</a>
					</li>
	            </ul> -->
	        </div>
        </div>
    </div>
</div>
<!--end::Subheader-->

<!--begin::Card-->
<div class="card card-custom">
	<div class="card-header">
		<div class="card-title">
			<span class="card-icon"><i class="flaticon-notepad text-primary"></i></span>
			<h3 class="card-label">Skip Feeding History </h3>
		</div>
		<div class="card-toolbar">

		</div>
	</div>

	<div class="card-body">
		<!--begin: Datatable-->
		<table class="table table-bordered table-hover table-checkable" id="kt_datatable" style="margin-top: 13px !important">
			<thead>
				<tr>
					<th></th>
					<th>Name</th>
					<th>Feed Type</th>
					<th>Cycle</th>
					<th>Volunteer Name</th>

				</tr>
			</thead>


        </table>
		<!--end: Datatable-->
	</div>
</div>
<!--end::Card-->

<script>
function getData(){
      var table = $('#kt_datatable').DataTable();
      table.destroy();
      var value = 'V';
      $("#kt_datatable").dataTable({
        "processing":true,
        "ajax":{
          "type":"POST",
          "url":"ajax/datatables/dt_skipped.php",
          "dataSrc":"data",
          "data":{
            value:value
          }
        },
        "columns":[
          {
            "data":"count"
          },
          {
            "data":"name"
          },
          {
            "data":"feed_type"
          },
          {
            "data":"cycle"
          },
          {
            "data":"volunteer_name"
          },
        ]
      });
    }




jQuery(document).ready(function() {
	getData();
});
</script>