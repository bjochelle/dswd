    <style>
        .wrap { max-width: 980px; margin: 10px auto 0; }
        #steps { margin: 80px 0 0 0 }
        .commands { overflow: hidden; margin-top: 30px; }
        .prev {float:left}
        .next, .submit {float:right}
        .error { color: #b33; }
        #progress { position: relative; height: 5px; background-color: #eee; margin-bottom: 20px; }
        #progress-complete { border: 0; position: absolute; height: 5px; min-width: 10px; background-color: #337ab7; transition: width .2s ease-in-out; }

    </style>
    
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/jquery.validate.min.js"></script>
    <script src="assets/js/jquery.formtowizard.js"></script>
    
    <script>
        $( function() {
            var $signupForm = $( '#SignupForm' );
            
            $signupForm.validate({errorElement: 'em'});
            
            $signupForm.formToWizard({
                submitButton: 'SaveAccount',
                nextBtnClass: 'btn btn-primary next',
                prevBtnClass: 'btn btn-default prev',
                buttonTag:    'button',
                validateBeforeNext: function(form, step) {
                    var stepIsValid = true;
                    var validator = form.validate();
                    $(':input', step).each( function(index) {
                        var xy = validator.element(this);
                        stepIsValid = stepIsValid && (typeof xy == 'undefined' || xy);
                    });
                    return stepIsValid;
                },
                progress: function (i, count) {
                    $('#progress-complete').width(''+(i/count*100)+'%');
                }
            });
        });
    </script>

<div class="subheader py-2 py-lg-6  subheader-solid " id="kt_subheader">
    <div class=" container-fluid  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
        <div class="d-flex align-items-center flex-wrap mr-1">
			<div class="d-flex align-items-baseline flex-wrap mr-5">
	            <h5 class="text-dark font-weight-bold my-1 mr-5">
					Add  Child Profile
				</h5>
	        </div>
        </div>
    </div>
</div>
<!--end::Subheader-->

<!--begin::Card-->
<div class="card card-custom">
	<div class="card-header">
		<div class="card-title">
			<span class="card-icon"><i class="flaticon-notepad text-primary"></i></span>
			<h3 class="card-label">Child Form</h3>
		</div>
	</div>


	<div class="card-body">

    <div class="row wrap"><div class="col-lg-12">

    <div id='progress'><div id='progress-complete'></div></div>

    <form id="SignupForm" action="">
        <fieldset>
            <legend>Account information</legend>
               <!--begin::Input-->
               <div class="form-group">
                                    <label>First Name</label>
                                    <input type="text" class="form-control form-control-solid form-control-lg" name="fname" placeholder="First Name" value="" />
                                    <span class="form-text text-muted">Please enter your first name.</span>
                                </div>
                                <!--end::Input-->

                                <!--begin::Input-->
                                <div class="form-group">
                                    <label>Middle Name</label>
                                    <input type="text" class="form-control form-control-solid form-control-lg" name="mname" placeholder="Middle Name" value="" />
                                    <span class="form-text text-muted">Please enter your middle name.</span>
                                </div>
                                <!--end::Input-->

                                <!--begin::Input-->
                                <div class="form-group">
                                    <label>Last Name</label>
                                    <input type="text" class="form-control form-control-solid form-control-lg" name="lname" placeholder="Last Name" value="" />
                                    <span class="form-text text-muted">Please enter your last name.</span>
                                </div>
                                <!--end::Input-->

                                 <!--begin::Input-->
                                 <div class="form-group">
                                    <label>Address</label>
                                    <input type="text" class="form-control form-control-solid form-control-lg" name="address" placeholder="Address Line " />
                                    <span class="form-text text-muted">Please enter your Address.</span>
                                </div>
                                <!--end::Input-->

                                  <!--begin::Input-->
                                  <div class="form-group">
                                    <label>Religion</label>
                                    <input type="text" class="form-control form-control-solid form-control-lg" name="religion" placeholder="Religion" value="" />
                                    <span class="form-text text-muted">Please enter your religion.</span>
                                </div>
                                <!--end::Input-->


                                <div class="row">
                                    <div class="col-xl-6">
                                        <!--begin::Input-->
                                        <div class="form-group">
                                            <label>Date of birth</label>
                                            <input type="date" class="form-control form-control-solid form-control-lg" name="birthdate" placeholder="birthdate" />
                                            <span class="form-text text-muted">Please enter your birthdate.</span>
                                        </div>
                                        <!--end::Input-->
                                    </div>
                                    <div class="col-xl-6">
                                    <!--begin::Input-->
                                    <div class="form-group">
                                        <label>Gender</label>
                                        <select name="gender" class="form-control form-control-solid form-control-lg">
                                            <option value="">Select</option>
                                            <option value="Male">Male</option>
                                            <option value="Female">Female</option>
                                        </select>
                                    </div>
                                        <!--end::Input-->
        </fieldset>

        <fieldset>
            <legend>II .Family Compostion :</legend>
                <div class="row">
                <div class="col-xl-6">
                    <!--begin::Input-->
                    <div class="form-group">
                        <label>Number of Family Member living in the same house</label>
                        <select id="num_fam" name="num_fam" class="form-control form-control-solid form-control-lg" onchange="gen()">
                        <?php 
                        $counter =0;
                        while($counter <=12){
                            echo '<option value='.$counter.'>'.$counter.'</option>';
                            $counter++;
                        }
                        ?>
                    </select>
                    </div>
                    <!--end::Input-->
                </div>
                <div class="table-responsive">
                  <table class="table table-bordered table-hover table-checkable"  style="width: 100%">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Relationship</th>
                            <th>Age</th>
                            <th>Civil Status</th>
                            <th>Occupation</th>
                            <th>Income</th>
                        </tr>
                        <tbody id="fam_body">
                            
                            </tbody>
                        </thead>
                    </table>
                </div>
                  
            </div>
            <br>
            <br>
                    <!--end::Input-->

            <legend>Problems/Nees Commonly Encounterd</legend>
            <div class="row">
            
        
            <div class="row">
            <div class="col-xl-12">
            <!--begin::Input-->
                <div class="form-group">
                    <label><strong>Living/Residing With:</strong></label>
                    <div class="form-group">
                    <div class="checkbox-list">
                        <label class="checkbox">
                            <input type="radio" name="livingwt" value="Alone"/>
                            <span></span>
                            Alone
                        </label>
                        <label class="checkbox">
                            <input type="radio" value="Parents" name="livingwt" />
                            <span></span>
                            Parents
                        </label>
                        <label class="checkbox">
                            <input type="radio" value="Care" name="livingwt" />
                            <span></span>
                            <div>Care Institution</div>
                            
                        </label>
                        <label class="checkbox">
                            <input type="radio" name="livingwt" value="Children"/>
                            <span></span>
                            Children
                        </label>
                        <label class="checkbox">
                            <input type="radio" name="livingwt" value="Friends"/>
                            <span></span>
                            Friends
                        </label>
                        <label class="checkbox">
                            <input type="radio" name="livingwt" value="Relatives"/>
                            <span></span>
                            Relatives
                        </label>
                        <label class="checkbox">
                            <input type="radio" name="livingwt" id="checkboc_lr" value="others" onclick="getValue('checkboc_lr','text_lr')" />
                            <span></span>
                            Others: 
                        </label>
                        <input type="text" name="livingwt" id="text_lr"  id="others" disabled/>
                    </div>
                </div>
                </div>
            <!--end::Input-->

             <!--begin::Input-->
             <div class="form-group">
                    <label><strong>Areas of interest:</strong></label>
                    <div class="form-group">
                    <div class="checkbox-list">
                        <label class="checkbox">
                            <input type="checkbox" name="a_int[]" value="Medical"/>
                            <span></span>
                            Medical
                        </label>
                        <label class="checkbox">
                            <input type="checkbox" name="a_int[]" value="Teaching"/>
                            <span></span>
                            Teaching
                        </label>
                        <label class="checkbox">
                            <input type="checkbox" name="a_int[]" value="Legal"/>
                            <span></span>
                            <div>Legal Services</div>
                            
                        </label>
                        <label class="checkbox">
                            <input type="checkbox" name="a_int[]" value="Dental"/>
                            <span></span>
                            Dental
                        </label>
                        <label class="checkbox">
                            <input type="checkbox" name="a_int[]" value="Counseling"/>
                            <span></span>
                            Counseling
                        </label>
                        <label class="checkbox">
                            <input type="checkbox" name="a_int[]" value="Evangelization" />
                            <span></span>
                            Evangelization
                        </label>
                        <label class="checkbox">
                            <input type="checkbox" id="checkbox_area" name="a_int[]" value="others" onclick="getValue('checkbox_area','text_area')"/>
                            <span></span>
                            Others: 
                        </label>
                        <input type="text" name="a_int[]" id="text_area" disabled/>
                    </div>
                </div>
                </div>
            <!--end::Input-->
                
            </div> 
        </fieldset>

        <fieldset class="form-horizontal" role="form">
            <legend>III .Problem/needs commonly encounter(Parent):</legend>

              <!--begin::Input-->
              <div class="form-group">
                    <label><strong>A. Economic:</strong></label>
                    <div class="form-group">
                    <div class="checkbox-list">
                        <label class="checkbox">
                            <input type="checkbox" name="prob[]" value="Lack of income"  />
                            <span></span>
                            Lack of income/Resources
                        </label>
                        <label class="checkbox">
                            <input type="checkbox" name="prob[]" value="Skills"/>
                            <span></span>
                            Skills/Capacity Training
                        </label>
                        <label class="checkbox">
                            <input type="checkbox" name="prob[]" value="Livelihood"/>
                            <span></span>
                            Livelihood opportunities
                        </label>
                        <label class="checkbox">
                            <input type="checkbox"  id="checkbox_economic" name="prob[]" value="others" onclick="getValue('checkbox_economic','textbox_economic')"/>
                            <span></span>
                            Others
                        </label>
                        <input type="text" name="prob[]" id="textbox_economic" />
                    </div>
                </div>
                </div>
            <!--end::Input-->
              <!--begin::Input-->
              <div class="form-group">
                    <label><strong>B. Socio/Emiotional(Child)</strong></label>
                    <div class="form-group">
                     <div class="checkbox-list">
                        <label class="checkbox">
                            <input type="checkbox" name="socio[]" value="Feeling of neglect"/>
                            <span></span>
                            Feeling of neglect & rejection
                        </label>
                        <label class="checkbox">
                            <input type="checkbox" name="socio[]" value="Feeling of helplessness"/>
                            <span></span>
                            Feeling of helplessness & worthlessness
                        </label>
                        <label class="checkbox">
                            <input type="checkbox" name="socio[]" value="Feeling of loneliness"/>
                            <span></span>
                            Feeling of loneliness & isolation
                            
                        </label>
                        <label class="checkbox">
                            <input type="checkbox" name="socio[]" value="Inadequate leisure"/>
                            <span></span>
                            Inadequate leisure/ recreational activites
                        </label>
                        <label class="checkbox">
                            <input type="checkbox" name="socio[]" value="Senior"/>
                            <span></span>
                            Senior Citizen Friendly Environment
                        </label>
                        <label class="checkbox">
                            <input type="checkbox"  id="checkbox_socio" name="socio[]" value="others" onclick="getValue('checkbox_socio','textbox_socio')"/> 
                            
                            <span></span>
                            Others
                        </label>
                       <input type="text" name="socio[]" id="textbox_socio" placeholder="Specify:"/>
                    </div>
                </div>
                </div>
            <!--end::Input-->
            
              <!--begin::Input-->
              <div class="form-group">
                    <label><strong>C. Health(Child)</strong></label>
                    <div class="form-group">
                    <div class="checkbox-list">
                        <label class="checkbox">
                            <input type="checkbox" placeholder="Please specify" name="health[]" value="Condition"/>
                            <span></span>
                            Condition / Illness
                        </label>
                        <label class="checkbox">
                            <input type="checkbox" placeholder="Please specify" name="health[]" value="With maintenance"/>
                            <span></span>
                            With maintenance
                        </label>
                        <label class="checkbox">
                            <input type="checkbox" name="health[]" value="High cost medicine"/>
                            <span></span>
                            High cost medicine
                        </label>
                        <label class="checkbox">
                            <input type="checkbox" name="health[]" value="Lack of medical professionals"/>
                            <span></span>
                            Lack of medical professionals
                        </label>
                        <label class="checkbox">
                            <input type="checkbox" name="health[]" value="Lack/No access to sanitation"/>
                            <span></span>
                            Lack/No access to sanitation
                        </label>
                        <label class="checkbox">
                            <input type="checkbox" id="checkbox_ailments" name="health[]" value="Health"/>
                            <span></span>
                            Health problems/Ailments
                        </label>

                        <label class="checkbox">
                            <input type="checkbox" name="health[]" value="No health insurance"/>
                            <span></span>
                            Lack/No health insurance/s inadequate health services
                        </label>

                        <label class="checkbox">
                            <input type="checkbox" name="health[]" value="medical facilities"/>
                            <span></span>
                            Lack of hospitals/medical facilities
                        </label>
                        <label class="checkbox">
                            <input type="checkbox"  id="checkbox_health" name="health[]" value="others" onclick="getValue('checkbox_health','textbox_health')"/>
                            <span></span>
                            Others
                        </label>
                        <input type="text" name="health[]" id="textbox_health"  placeholder="Specify:"/>
                    </div>
                <br>
                    <label><strong>Deworming<strong></label>
                    <div class="row">
                        <div class="col-md-6">
                            <label> First  Dose</label>
                            <input type="date" name="deworming_fir" />
                        </div>
                        <div class="col-md-6">
                            <label> Second  Dose</label>
                            <input type="date" name="deworming_sec" />
                        </div>
                    </div>
                    <table class="table table-bordered table-hover table-checkable"  style="margin-top: 13px !important">
                    <thead>
                        <tr>
                            <th rowspan="2">Nutritional Status</th>
                            <th colspan="2">Baseline</th>
                            <th colspan="2">After 3 months </th>
                            <th colspan="2">After Feeding</th>
                        </tr>
                        <tr>
                            <th>Wt/ht</th>
                            <th>NS</th>
                            <th>Wt/ht</th>
                            <th>NS</th>
                            <th>Wt/ht</th>
                            <th>NS</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Weight/Length for age:</td>
                            <td><input type="number" name="wt_lgth_age" min="1"></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Weight for age:</td>
                            <td><input type="number" name="wt_age" min="1"></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Weight for Height/Length:</td>
                            <td><input type="number" name="weigh_hght_lg" min="1"></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>

                </div>
                </div>
            <!--end::Input-->
              <!--begin::Input-->
              <div class="form-group">
                    <label><strong>E. Identify Others specific needs</strong></label>
                    <div class="form-group">
                    <textarea class="form-control" name="specific_needs"></textarea>
           
                </div>
                </div>
            <!--end::Input-->
           <input type="hidden" name="status" value="Create">
        </fieldset>
        <p>
            <button id="SaveAccount" class="btn btn-primary submit">Submit form</button>
        </p>
    </form>

</div></div>
	
	</div>
</div>
<!--end::Card-->
<script>
    $("#SignupForm").submit(function(e){
        e.preventDefault();

         var livingwt_value =  $("input[name='livingwt']:checked").map(function(){
            return this.value;
        }).get();

        if(livingwt_value == 'others'){
            var livingwt = "others," + $("#text_lr").val();
        }else{
            var livingwt = livingwt_value;
        }
        $.ajax({
        url:"ajax/childCrud.php",
        method:"POST",
        data:$("#SignupForm").serialize() + '&livingwt=' + livingwt,
        success: function(data){
            if(data == 1){
                alertMe("All is cool! Successfully added child information","success");
                window.setTimeout(function(){ 
                    window.location.replace("home.php?view=children");
                } ,1000);
           }else{
                alertMe("Sorry, looks like there are some errors detected, please try again.","error");
           }
          
        }
     });
    })
function gen(){
    var input = $("#num_fam").val();
    var start =1;
    var tbody_data="";
    while(start <= input){
        tbody_data+="<tr>"+
                "<td><input type='text' name='fam_name[]'></td>"+
                "<td><input type='text' name='fam_rel[]'></td>"+
                "<td><input type='text' name='fam_age[]'></td>"+
                "<td><input type='text' name='fam_civil_status[]'></td>"+
                "<td><input type='text' name='fam_occupation[]'></td>"+
                "<td><input type='text' name='fam_income[]'></td>"+
                "</tr>";
         start=start+1;
    }
     $("#fam_body").html(tbody_data);
}

function getValue(checkbox,textbox){
    if($("#"+checkbox).prop("checked") == true){
        $("#"+textbox).prop('disabled',false);
    } else{
        $("#"+textbox).prop('disabled',true);
    }
}


</script>
