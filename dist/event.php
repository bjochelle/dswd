<div class="subheader py-2 py-lg-6  subheader-solid " id="kt_subheader">
    <div class=" container-fluid  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
        <div class="d-flex align-items-center flex-wrap mr-1">
			<div class="d-flex align-items-baseline flex-wrap mr-5">
	            <h5 class="text-dark font-weight-bold my-1 mr-5">
					Event
				</h5>
	        
	        </div>
        </div>
    </div>
</div>
<!--end::Subheader-->

<!--begin::Card-->
<div class="card card-custom">
	<div class="card-header">
		<div class="card-title">
			<span class="card-icon"><i class="flaticon-notepad text-primary"></i></span>
			<h3 class="card-label">List of Event</h3>
		</div>
		<div class="card-toolbar">
		<!--begin::Button-->
		<?php if($status == 'A'){?>
		<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#staticBackdrop"><span class="fas fa-plus-circle"></span>
			New Record  
		</button>
		<?php }?>
		<!--end::Button-->
		</div>
	</div>
    <?php require('modal/modal_addEvent.php');
    require('modal/modal_editEvent.php');?>
	<div class="card-body">
		<!--begin: Datatable-->
		<table class="table table-bordered table-hover table-checkable" id="kt_datatable" style="margin-top: 13px !important">
			<thead>
				<tr>
					<th></th>
					<th>Name</th>
					<th>When</th>
					<th>Where</th>
					<th>Description</th>
					<th>Status</th>
					<th>Actions</th>
				</tr>
			</thead>


        </table>
		<!--end: Datatable-->
	</div>
</div>
<!--end::Card-->

<script>
function getData(){
      var table = $('#kt_datatable').DataTable();
      table.destroy();
      var status = 'datatable';

      $("#kt_datatable").dataTable({
        "processing":true,
        "ajax":{
          "type":"POST",
          "url":"ajax/eventCrud.php",
          "dataSrc":"data",
          "data":{
            status:status
          }
        },
        "columns":[
          {
            "data":"count"
          },
          {
            "data":"eventName"
          },
          {
            "data":"eventWhen"
          }, 
          {
            "data":"eventWhere"
          }, 
          {
            "data":"eventDesc"
          },   
          {
            "data":"status_info"
          },       
          {
            "mRender": function(data,type,row){

            	if('<?php echo $profile['status'];?>' == 'A'){
        
            	
            		if(row.status == '1'){
            			var edit = '';
            			var feed_status = '</div>';
            		}else{
            			var edit = '<a href="javascript:;" class="btn btn-sm btn-clean btn-icon mr-1" title="View/Edit details" onclick="edit('+row.id+')"><i class="la la-edit"></i></a>';
            			var feed_status = '<a href="javascript:;" class="btn btn-sm btn-clean btn-icon" title="Close Event"><i class="fas fa-window-close" onclick="closeEvent('+row.id+')"></i></a></div>';
            		}
            		
            	}else{
            		var edit = '';
            		var feed_status ='';
            	}
				return '<div class="d-flex align-items-center">'
				+'<div class="dropdown dropdown-inline mr-1"><div class="dropdown-menu dropdown-menu-sm 	dropdown-menu-right"></div></div>'
				+edit+feed_status;
            }
          }
        ]
      });
    }


	$("#kt_form_1").submit(function(e){
	  e.preventDefault();
      $.ajax({
        url:"ajax/eventCrud.php",
        method:"POST",
        data:$("#kt_form_1").serialize(),
        success: function(data){

           if(data == 1){
				alertMe("All is cool! Successfully added event","success");
				$("#staticBackdrop").modal("hide");
				getData();
			}else if(data == 2){
				alertMe("Sorry, Event is already existing, please check the form .","error");
		   }else{
				alertMe("Sorry, looks like there are some errors detected, please try again.","error");
		   }
          
        }
      });
	});

	$("#kt_form_edit").submit(function(e){
	  e.preventDefault();
      $.ajax({
        url:"ajax/eventCrud.php",
        method:"POST",
        data:$("#kt_form_edit").serialize(),
        success: function(data){
           if(data == 1){
				alertMe("All is cool! Successfully updated event","success");
				$("#editModal").modal("hide");
				getData();
			}else if(data == 2){
				alertMe("Sorry, Event is already existing, please check the form .","error");
		   }else{
				alertMe("Sorry, looks like there are some errors detected, please try again.","error");
		   }
          
        }
      });
	});

	function closeEvent(id){
		$("#id").val(id);

		alert(id)
		$.ajax({
			url:"ajax/eventCrud.php",
			type:"POST",
			data:{
				id:id,
				status:'Close'
			},success:function(data){
				   if(data == 1){
						alertMe("All is cool! Successfully closed Event","success");
						getData();
				   }else{
						alertMe("Sorry, looks like there are some errors detected, please try again.","error");
				   }
          
			}
		});
	}

	function edit(id){
		$("#editModal").modal("show");
		$("#id").val(id);
		$.ajax({
			url:"ajax/eventCrud.php",
			type:"POST",
			data:{
				id:id,
				status:'View'
			},success:function(data){
				var o = JSON.parse(data);
				$("#event_name").val(o.event_name);
				$("#event_when").val(o.event_when);
				$("#event_where").val(o.event_where);
				$("#event_description").val(o.event_description);
			}
		});
	}
	
	function viewEvent(id){
        window.location.replace("home.php?view=viewEvent&id="+id);
	}

jQuery(document).ready(function() {
	getData();
});
</script>